/*
 * Copyright (c) 2023 Tyler Crandall
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include <stdint.h>
#include <stdbool.h>

#ifndef QUEUE_TYPE
#error There must be a provided type for QUEUE_TYPE and it must be defined prior to using this code!
#define QUEUE_TYPE int32_t // To make VSCode happy
#endif

#ifndef INDEX_TYPE
#define INDEX_TYPE uint64_t
#endif

#ifndef PREFIX_NAME
#define PREFIX_NAME 
#endif

#define MAKE_PREFIX_QUEUE(x) x##queue
#define GEN_PREFIX_QUEUE(x) MAKE_PREFIX_QUEUE(x)
#define PREFIXED_QUEUE GEN_PREFIX_QUEUE(PREFIX_NAME)
#define MAKE_QUEUE_NAME(x, y) x##_##y
#define QUEUE_NAME(x, y) MAKE_QUEUE_NAME(x, y)
#define QUEUE QUEUE_NAME(PREFIXED_QUEUE, QUEUE_TYPE)

#ifndef DEFAULT_SIZE
#define DEFAULT_SIZE 32
#endif

typedef struct QUEUE *QUEUE;

#define MAKE_QUEUE_NEW_NAME(x) x##_new()
#define GEN_NEW_NAME(x) MAKE_QUEUE_NEW_NAME(x)

QUEUE GEN_NEW_NAME(QUEUE);

#undef GEN_NEW_NAME
#undef MAKE_QUEUE_NEW_NAME

#define MAKE_QUEUE_NEW_CAPACITY_NAME(x, y) x##_new_withcapacity(y initialCapacity)
#define GEN_NEW_CAPACITY_NAME(x, y) MAKE_QUEUE_NEW_CAPACITY_NAME(x, y)

QUEUE GEN_NEW_CAPACITY_NAME(QUEUE, INDEX_TYPE);

#undef GEN_NEW_CAPACITY_NAME
#undef MAKE_QUEUE_NEW_CAPACITY_NAME

#define MAKE_QUEUE_DESTROY_NAME(x) x##_destroy(QUEUE this)
#define GEN_DESTROY_NAME(x) MAKE_QUEUE_DESTROY_NAME(x)

bool GEN_DESTROY_NAME(QUEUE);

#undef GEN_DESTROY_NAME
#undef MAKE_QUEUE_DESTROY_NAME

#define MAKE_QUEUE_CLEAR_NAME(x) x##_clear(QUEUE this)
#define GEN_CLEAR_NAME(x) MAKE_QUEUE_CLEAR_NAME(x)

bool GEN_CLEAR_NAME(QUEUE);

#undef GEN_CLEAR_NAME
#undef MAKE_QUEUE_CLEAR_NAME

#define MAKE_QUEUE_GETCOUNT_NAME(x) x##_getcount(QUEUE this, INDEX_TYPE *outCount)
#define GEN_GETCOUNT_NAME(x) MAKE_QUEUE_GETCOUNT_NAME(x)

bool GEN_GETCOUNT_NAME(QUEUE);

#undef GEN_GETCOUNT_NAME
#undef MAKE_QUEUE_GETCOUNT_NAME

#define MAKE_QUEUE_REALLOCATE_NAME(x) x##_reallocate(QUEUE this, INDEX_TYPE newSize)
#define GEN_REALLOCATE_NAME(x) MAKE_QUEUE_REALLOCATE_NAME(x)

bool GEN_REALLOCATE_NAME(QUEUE);

#undef GEN_REALLOCATE_NAME
#undef MAKE_QUEUE_REALLOCATE_NAME

#define MAKE_QUEUE_ENQUEUE_NAME(x, y, z) x##_enqueue(x this, y item, z *outEnqueuedCount)
#define GEN_ENQUEUE_NAME(x, y, z) MAKE_QUEUE_ENQUEUE_NAME(x, y, z)

bool GEN_ENQUEUE_NAME(QUEUE, QUEUE_TYPE, INDEX_TYPE);

#undef GEN_ENQUEUE_NAME
#undef MAKE_QUEUE_ENQUEUE_NAME

#define MAKE_QUEUE_ENQUEUE_ITEMS_NAME(x, y, z) x##_enqueue_items(x this, y *items, z items_index, z items_count, z *outEnqueuedCount)
#define GEN_ENQUEUE_ITEMS_NAME(x, y, z) MAKE_QUEUE_ENQUEUE_ITEMS_NAME(x, y, z)

bool GEN_ENQUEUE_ITEMS_NAME(QUEUE, QUEUE_TYPE, INDEX_TYPE);

#undef GEN_ENQUEUE_ITEMS_NAME
#undef MAKE_QUEUE_ENQUEUE_ITEMS_NAME

#define MAKE_QUEUE_DEQUEUE_NAME(x, y, z) x##_dequeue(x this, y *outItem, z* outDequeueCount)
#define GEN_DEQUEUE_NAME(x, y, z) MAKE_QUEUE_DEQUEUE_NAME(x, y, z)

bool GEN_DEQUEUE_NAME(QUEUE, QUEUE_TYPE, INDEX_TYPE);

#undef GEN_DEQUEUE_NAME
#undef MAKE_QUEUE_DEQUEUE_NAME

#define MAKE_QUEUE_DEQUEUE_ITEMS_NAME(x, y, z) x##_dequeue_items(x this, z count, y *outItems, z *outDequeueCount)
#define GEN_DEQUEUE_ITEMS_NAME(x, y, z) MAKE_QUEUE_DEQUEUE_ITEMS_NAME(x, y, z)

bool GEN_DEQUEUE_ITEMS_NAME(QUEUE, QUEUE_TYPE, INDEX_TYPE);

#undef GEN_DEQUEUE_ITEMS_NAME
#undef MAKE_QUEUE_DEQUEUE_ITEMS_NAME

#define MAKE_QUEUE_PEEK_NAME(x, y, z) x##_peek(x this, y *outItem, z* outPeekCount)
#define GEN_PEEK_NAME(x, y, z) MAKE_QUEUE_PEEK_NAME(x, y, z)

bool GEN_PEEK_NAME(QUEUE, QUEUE_TYPE, INDEX_TYPE);

#undef GEN_PEEK_NAME
#undef MAKE_QUEUE_PEEK_NAME

#define MAKE_QUEUE_PEEK_ITEMS_NAME(x, y, z) x##_peek_items(x *this, z count, y *outItems, z *outPeekCount)
#define GEN_PEEK_ITEMS_NAME(x, y, z) MAKE_QUEUE_PEEK_ITEMS_NAME(x, y, z)

bool GEN_PEEK_ITEMS_NAME(QUEUE, QUEUE_TYPE, INDEX_TYPE);

#undef GEN_PEEK_ITEMS_NAME
#undef MAKE_QUEUE_PEEK_ITEMS_NAME

#undef INDEX_TYPE
#undef QUEUE_TYPE
#undef QUEUE
#undef QUEUE_NAME
#undef MAKE_QUEUE_NAME
#undef DEFAULT_SEGMENT_SIZE
#undef PREFIX_NAME
