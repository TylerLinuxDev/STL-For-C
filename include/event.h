/*
 * Copyright (c) 2023 Tyler Crandall
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include <stdint.h>
#ifndef EVENT_ARG_TYPE
    #error EVENT_ARG_TYPE need to be defined with a type specified (Ex. int, long, double, float, struct TestStruct, ...)
    #define EVENT_ARG_TYPE int32_t
#endif

#ifndef INDEX_TYPE
    #define INDEX_TYPE uint64_t
#endif
#include <stdbool.h>

#define MAKE_EVENT_ARG_NAME(x) event_arg_ ## x
#define EVENT_ARG_NAME(x) MAKE_EVENT_ARG_NAME(x)
#define EVENT_ARG EVENT_ARG_NAME(EVENT_ARG_TYPE)

#define MAKE_EVENT_CALLBACK_NAME(x) callback_## x
#define EVENT_CALLBACK_NAME(x) MAKE_EVENT_CALLBACK_NAME(x)
typedef void (*EVENT_CALLBACK_NAME(EVENT_ARG_TYPE))(EVENT_ARG_TYPE arg);

#define MAKE_EVENT_NAME(x) event_ ## x
#define EVENT_NAME(x) MAKE_EVENT_NAME(x)
#define EVENT EVENT_NAME(EVENT_ARG_TYPE)

typedef struct EVENT *EVENT;

#define MAKE_EVENT_NEW_NAME(x) event_new_ ## x
#define EVENT_NEW_NAME(x) MAKE_EVENT_NEW_NAME(x)
EVENT* EVENT_NEW_NAME(EVENT_ARG_TYPE)();
#undef EVENT_NEW_NAME
#undef MAKE_EVENT_NEW_NAME

#define MAKE_EVENT_DESTROY_NAME(x) event_destroy_ ## x
#define EVENT_DESTROY_NAME(x) MAKE_EVENT_DESTROY_NAME(x)
bool EVENT_DESTROY_NAME(EVENT_ARG_TYPE)(EVENT* event);
#undef EVENT_DESTROY_NAME
#undef MAKE_EVENT_DESTROY_NAME

#define MAKE_EVENT_INVOKE_NAME(x) event_invoke_ ## x
#define EVENT_INVOKE_NAME(x) MAKE_EVENT_INVOKE_NAME(x)
bool EVENT_INVOKE_NAME(EVENT_ARG_TYPE)(EVENT* event, EVENT_ARG_TYPE arg);
#undef EVENT_INVOKE_NAME
#undef MAKE_EVENT_INVOKE_NAME

#define MAKE_EVENT_ADD_NAME(x) event_add_ ## x
#define EVENT_ADD_NAME(x) MAKE_EVENT_ADD_NAME(x)
bool EVENT_ADD_NAME(EVENT_ARG_TYPE)(EVENT* event, EVENT_CALLBACK_NAME(EVENT_ARG_TYPE) callback);
#undef EVENT_ADD_NAME
#undef MAKE_EVENT_ADD_NAME

#define MAKE_EVENT_REMOVE_NAME(x) event_remove_ ## x
#define EVENT_REMOVE_NAME(x) MAKE_EVENT_REMOVE_NAME(x)
bool EVENT_REMOVE_NAME(EVENT_ARG_TYPE)(EVENT* event, EVENT_CALLBACK_NAME(EVENT_ARG_TYPE) callback);
#undef EVENT_REMOVE_NAME
#undef MAKE_EVENT_REMOVE_NAME

#define MAKE_EVENT_CLEAR_NAME(x) event_clear_ ## x
#define EVENT_CLEAR_NAME(x) MAKE_EVENT_CLEAR_NAME(x)
bool EVENT_CLEAR_NAME(EVENT_ARG_TYPE)(EVENT* event);
#undef EVENT_CLEAR_NAME
#undef MAKE_EVENT_CLEAR_NAME

#define MAKE_EVENT_COUNT_NAME(x) event_count_ ## x
#define EVENT_COUNT_NAME(x) MAKE_EVENT_COUNT_NAME(x)
bool EVENT_COUNT_NAME(EVENT_ARG_TYPE)(EVENT* event, INDEX_TYPE* outCount);
#undef EVENT_CLEAR_NAME
#undef MAKE_EVENT_CLEAR_NAME
#undef EVENT_ARG_TYPE
