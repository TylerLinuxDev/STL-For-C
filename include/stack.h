/*
 * Copyright (c) 2023 Tyler Crandall
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef STACK_TYPE
#error There must be a provided type for STACK_TYPE and it must be defined prior to using this code!
#define STACK_TYPE int32_t // To make VSCode happy
#endif

#ifndef INDEX_TYPE
#define INDEX_TYPE uint64_t
#endif

#ifndef STL_FOR_C_IMPLEMENTATION_ONLY
#define STL_FOR_C_IMPLEMENTATION_ONLY
#endif

#include <stdint.h>
#include <stdbool.h>

#define MAKE_STACK_NAME(x) stack_##x
#define STACK_NAME(x) MAKE_STACK_NAME(x)
#define STACK STACK_NAME(STACK_TYPE)

#ifndef DEFAULT_SIZE
#define DEFAULT_SIZE 32
#endif

typedef struct STACK* STACK;

#define MAKE_STACK_NEW_NAME(x) x##_new()
#define GEN_NEW_NAME(x) MAKE_STACK_NEW_NAME(x)

STACK *GEN_NEW_NAME(STACK);

#undef GEN_NEW_NAME
#undef MAKE_STACK_NEW_NAME

#define MAKE_STACK_DESTROY_NAME(x) x##_destroy(STACK this)
#define GEN_DESTROY_NAME(x) MAKE_STACK_DESTROY_NAME(x)

bool GEN_DESTROY_NAME(STACK);

#undef GEN_DESTROY_NAME
#undef MAKE_STACK_DESTROY_NAME

#define MAKE_STACK_CLEAR_NAME(x) x##_clear(STACK this)
#define GEN_CLEAR_NAME(x) MAKE_STACK_CLEAR_NAME(x)

bool GEN_CLEAR_NAME(STACK);

#undef GEN_CLEAR_NAME
#undef MAKE_STACK_CLEAR_NAME

#define MAKE_STACK_GETCOUNT_NAME(x) x##_getcount(STACK this, INDEX_TYPE *outCount)
#define GEN_GETCOUNT_NAME(x) MAKE_STACK_GETCOUNT_NAME(x)

bool GEN_GETCOUNT_NAME(STACK);

#undef GEN_GETCOUNT_NAME
#undef MAKE_STACK_GETCOUNT_NAME

#define MAKE_STACK_REALLOCATE_NAME(x) x##_reallocate(STACK this, INDEX_TYPE newSize)
#define GEN_REALLOCATE_NAME(x) MAKE_STACK_REALLOCATE_NAME(x)

bool GEN_REALLOCATE_NAME(STACK);

#undef GEN_REALLOCATE_NAME
#undef MAKE_STACK_REALLOCATE_NAME

#define MAKE_STACK_PUSH_NAME(x, y, z) x##_push(x this, y item, z *outEnstackdCount)
#define GEN_PUSH_NAME(x, y, z) MAKE_STACK_PUSH_NAME(x, y, z)

bool GEN_PUSH_NAME(STACK, STACK_TYPE, INDEX_TYPE);

#undef GEN_PUSH_NAME
#undef MAKE_STACK_PUSH_NAME

#define MAKE_STACK_PUSH_ITEMS_NAME(x, y, z) x##_push_items(x this, y *items, z items_index, z items_count, z *outEnstackdCount)
#define GEN_PUSH_ITEMS_NAME(x, y, z) MAKE_STACK_PUSH_ITEMS_NAME(x, y, z)

bool GEN_PUSH_ITEMS_NAME(STACK, STACK_TYPE, INDEX_TYPE);

#undef GEN_PUSH_ITEMS_NAME
#undef MAKE_STACK_PUSH_ITEMS_NAME

#define MAKE_STACK_POP_NAME(x, y, z) x##_pop(x this, y *outItem, z* outPopCount)
#define GEN_POP_NAME(x, y, z) MAKE_STACK_POP_NAME(x, y, z)

bool GEN_POP_NAME(STACK, STACK_TYPE, INDEX_TYPE);

#undef GEN_POP_NAME
#undef MAKE_STACK_POP_NAME

#define MAKE_STACK_POP_ITEMS_NAME(x, y, z) x##_pop_items(x this, z count, y *outItems, z *outPopCount)
#define GEN_POP_ITEMS_NAME(x, y, z) MAKE_STACK_POP_ITEMS_NAME(x, y, z)

bool GEN_POP_ITEMS_NAME(STACK, STACK_TYPE, INDEX_TYPE);
    
#undef GEN_POP_ITEMS_NAME
#undef MAKE_STACK_POP_ITEMS_NAME

#define MAKE_STACK_PEEK_NAME(x, y) x##_peek(x this, y *outItem)
#define GEN_PEEK_NAME(x, y) MAKE_STACK_PEEK_NAME(x, y)

bool GEN_PEEK_NAME(STACK, STACK_TYPE);

#undef GEN_PEEK_NAME
#undef MAKE_STACK_PEEK_NAME

#define MAKE_STACK_PEEK_ITEMS_NAME(x, y, z) x##_peek_items(x this, z count, y *outItems, z *outPopCount)
#define GEN_PEEK_ITEMS_NAME(x, y, z) MAKE_STACK_PEEK_ITEMS_NAME(x, y, z)

bool GEN_PEEK_ITEMS_NAME(STACK, STACK_TYPE, INDEX_TYPE);

#undef GEN_PEEK_ITEMS_NAME
#undef MAKE_STACK_PEEK_ITEMS_NAME

#undef STL_FOR_C_IMPLEMENTATION_ONLY
#undef INDEX_TYPE
#undef STACK_TYPE
#undef STACK
#undef STACK_NAME
#undef MAKE_STACK_NAME
#undef DEFAULT_SIZE
