/*
 * Copyright (c) 2023 Tyler Crandall
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include <stdbool.h>
#ifndef HASH_TABLE_VALUE_TYPE
    #error HASH_TABLE_VALUE_TYPE need to be defined with a type specified (Ex. int, long, double, float, struct TestStruct, ...)
    #define HASH_TABLE_VALUE_TYPE int32_t
#endif

#include <stdint.h>
#ifndef INDEX_TYPE
    #define INDEX_TYPE uint64_t
#endif

#define MAKE_HASH_TABLE_NAME(x) hash_table_ ## x
#define HASH_TABLE_NAME(x) MAKE_HASH_TABLE_NAME(x)
#define HASH_TABLE HASH_TABLE_NAME(HASH_TABLE_VALUE_TYPE)

typedef struct HASH_TABLE *HASH_TABLE;

#define MAKE_HASH_TABLE_NEW_NAME(x) x ## _new()
#define GEN_HASH_TABLE_NEW_NAME(x) MAKE_HASH_TABLE_NEW_NAME(x)
HASH_TABLE GEN_HASH_TABLE_NEW_NAME(HASH_TABLE);

#define MAKE_HASH_TABLE_DESTROY_NAME(x) x ## _destroy(x this)
#define GEN_HASH_TABLE_DESTROY_NAME(x) MAKE_HASH_TABLE_DESTROY_NAME(x)
void GEN_HASH_TABLE_DESTROY_NAME(HASH_TABLE);

#define MAKE_HASH_TABLE_GET_NAME(x) x ## _get(x this, const char* key, HASH_TABLE_VALUE_TYPE* outValue)
#define GEN_HASH_TABLE_GET_NAME(x) MAKE_HASH_TABLE_GET_NAME(x)
bool GEN_HASH_TABLE_GET_NAME(HASH_TABLE);

#define MAKE_HASH_TABLE_REMOVE_NAME(x) x ## _remove(x this, const char* key)
#define GEN_HASH_TABLE_REMOVE_NAME(x) MAKE_HASH_TABLE_REMOVE_NAME(x)
bool GEN_HASH_TABLE_REMOVE_NAME(HASH_TABLE);

#define MAKE_HASH_TABLE_SET_NAME(x) x ## _set(x this, const char* key, HASH_TABLE_VALUE_TYPE value, const char** outRealKey)
#define GEN_HASH_TABLE_SET_NAME(x) MAKE_HASH_TABLE_SET_NAME(x)
bool GEN_HASH_TABLE_SET_NAME(HASH_TABLE);

#define MAKE_HASH_TABLE_COUNT_NAME(x) x ## _count(x this, INDEX_TYPE* outCount)
#define GEN_HASH_TABLE_COUNT_NAME(x) MAKE_HASH_TABLE_COUNT_NAME(x)
bool GEN_HASH_TABLE_COUNT_NAME(HASH_TABLE);

#define MAKE_HASH_TABLE_ITERATOR_NAME(x) hash_table_iterator_ ## x
#define HASH_TABLE_ITERATOR_NAME(x) MAKE_HASH_TABLE_ITERATOR_NAME(x)
#define HASH_TABLE_ITERATOR HASH_TABLE_ITERATOR_NAME(HASH_TABLE_VALUE_TYPE)

typedef struct {
    const char* key;
    HASH_TABLE_VALUE_TYPE value;
    HASH_TABLE _table;
    INDEX_TYPE _index;
} HASH_TABLE_ITERATOR;

#define MAKE_HASH_TABLE_GETITERATOR_NAME(x) x ## _getiterator(x this, HASH_TABLE_ITERATOR* outIterator)
#define GEN_HASH_TABLE_GETITERATOR_NAME(x) MAKE_HASH_TABLE_GETITERATOR_NAME(x)
bool GEN_HASH_TABLE_GETITERATOR_NAME(HASH_TABLE);

#define MAKE_HASH_TABLE_ITERATOR_NEXT_NAME(x, y) x ## _iterator_next(y iterator)
#define GEN_HASH_TABLE_ITERATOR_NEXT_NAME(x, y) MAKE_HASH_TABLE_ITERATOR_NEXT_NAME(x, y)
bool GEN_HASH_TABLE_ITERATOR_NEXT_NAME(HASH_TABLE, HASH_TABLE_ITERATOR);

#undef GEN_HASH_TABLE_ITERATOR_NEXT_NAME
#undef MAKE_HASH_TABLE_ITERATOR_NEXT_NAME
#undef GEN_HASH_TABLE_GETITERATOR_NAME
#undef MAKE_HASH_TABLE_GETITERATOR_NAME
#undef HASH_TABLE_ITERATOR
#undef HASH_TABLE_ITERATOR_NAME
#undef MAKE_HASH_TABLE_ITERATOR_NAME
#undef GEN_HASH_TABLE_GETCOUNT_NAME
#undef MAKE_HASH_TABLE_GETCOUNT_NAME
#undef GEN_HASH_TABLE_SET_NAME
#undef MAKE_HASH_TABLE_SET_NAME
#undef GEN_HASH_TABLE_GET_NAME
#undef MAKE_HASH_TABLE_GET_NAME
#undef GEN_HASH_TABLE_DESTROY_NAME
#undef MAKE_HASH_TABLE_DESTROY_NAME
#undef GEN_HASH_TABLE_NEW_NAME
#undef MAKE_HASH_TABLE_NEW_NAME
#undef HASH_TABLE
#undef HASH_TABLE_NAME
#undef MAKE_HASH_TABLE_NAME
#undef HASH_TABLE_VALUE_TYPE
#undef MAKE_HASH_TABLE_REMOVE_NAME
#undef GEN_HASH_TABLE_REMOVE_NAME
