#ifndef STL_FOR_C_STRING
#define STL_FOR_C_STRING
#include <stdint.h>
#include <stdbool.h>

typedef struct cstring cstring;

bool cstring_validate(cstring string);
cstring cstring_new();
cstring cstring_new2(uint64_t initialCapacity);
bool cstring_destroy(cstring str);
bool cstring_indexof(cstring this, char* stringToFind, uint64_t offset, uint64_t length, uint64_t* outIndex);
bool cstring_append(cstring* thisRef, const char* string);
bool cstring_appendnewline(cstring* thisRef);
bool cstring_appendline(cstring* thisRef, const char* line);
void cstring_print(cstring this);
uint64_t cstring_getlength(cstring this);
uint64_t cstring_getcapacity(cstring this);
char* cstring_getcstr(cstring this);
int32_t cstring_compare(cstring l, cstring r);
#endif
