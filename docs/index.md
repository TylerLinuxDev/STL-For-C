# STL For C
###### Written by [Tyler Crandall](https://codeberg.org/TylerLinuxDev) and STL For C contributors

***
## Documentation

* [Project Structure Guideline](Main/Project_Structure_Guideline.md)
* [Performance Benchmark]()
* [Note on INDEX_TYPE](Main/Note_on_Index_Type.md)

##### Non-Concurrent (Thread Unsafe)
* [List/Vector](NonConcurrent/List_Vector.md)
* [Dictionary/HashTable](NonConcurrent/Hashtable.md)
* [Queue](NonConcurrent/Queue.md)
* [Stack](NonConcurrent/Stack.md)
* [Event](NonConcurrent/Event.md)

##### Concurrent (Thread Safe)
* [List/Vector]()
* [Dictionary/HashTable]()
* [Queue](Concurrent/Concurrent_Queue.md)
* [Stack](Concurrent/Concurrent_Stack.md)
* [Event]()
* [Hashset]()
* [Single Linked List]()
* [Doubly Linked List]()

##### Future Data Container Types

* deque (Double Ended Queue)
* multiset
* multimap
* priority queue

##### Developer Information

* [How To Contribute to STL For C?](DeveloperInformation/HowToContributeToSTLForC.md)

Got an idea? Bring it up for [discussion](https://codeberg.org/TylerLinuxDev/STL-For-C/issues)! 
