# Contributing to the STL for C Project

We greatly appreciate your interest in contributing to the STL for C project! To ensure a smooth contribution process, we have outlined the necessary steps and guidelines in this document.

## Project Branch Structure

In the STL for C project, we primarily operate with two branches:

 - \`**main**\`: This branch contains stable, fully reviewed, and tested code that is ready for release.
 - \`**staging**\`: This is the active development branch where new features, fixes, and improvements are introduced and tested. When submitting new pull requests (PRs), they should be targeted to this branch.

## Pull Request Process

To contribute, please follow these steps when submitting a pull request (PR):

 1. **Create a Pull Request**: Initiate a PR from a new, separate branch on your own fork of the repository, targeting the \`**staging**\` branch.

 2. **Review**: A project contributor will review your PR, provide feedback if necessary, and potentially approve it.

 3. **Continuous Integration/Continuous Deployment (CI/CD) Trigger**: Our CI/CD pipeline is designed to automatically trigger whenever a pull request is merged into the \`**staging**\` branch. This ensures that your changes are thoroughly tested and deployed in a controlled manner.

 4. **Testing**: The CI/CD pipeline retrieves the latest code from the repository, builds the project, and executes the tests using \`**meson test -C build**\` and \`**cppcheck**\`. These commands respectively run unit tests and static code analysis.

 5. **Issue Handling**:
       - If the CI/CD pipeline identifies any issues, it will automatically create a new issue and reference it in a comment on your PR.
       - You're welcome to submit additional PRs to resolve these issues. Project contributors may also choose to fix them. We're committed to collaborating with you to resolve any problems.
       - In case of a significant issue, contributors might revert the merge in the \`**staging**\` branch and provide guidance to help you address the issue for future PRs.

 6. **Finalization**: If your contribution passes all CI/CD checks and receives approval from the project contributors, it will be merged into the \`**main**\` branch.

## Licensing and Attribution

Our project is licensed under the MIT license and we value acknowledging all contributions. As part of the contribution process, we ask you to add your name to the list of contributors in the LICENSE text file. This step is **not optional** as it helps ensure proper attribution of your work and maintains transparency about the project's authorship. Don't forget to also include the date of your contribution. Thank you for your cooperation and contribution!

## Engagement

We appreciate your active participation in the review process. It's also important to update the project's documentation alongside code changes where necessary to keep the information up-to-date for all users.

Please note, the \`**main**\` branch represents the stable state of the project. Any code with lingering issues in the \`**staging**\` branch will not be merged into \`**main**\` until they are resolved.

By adhering to these guidelines, you can help ensure that your contribution to the STL for C project is processed efficiently. Thank you for your valuable contribution!
