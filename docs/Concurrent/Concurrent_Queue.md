# Concurrent Queue

## Brief

A queue is an ordered collection of elements that follows a first-in, first-out (FIFO) access policy. Elements are added to the end of the queue and removed from the front. Queues exhibit ordered, FIFO behavior and provide enqueue/dequeue operations with linear access to the front and back. Queues are commonly used for task scheduling, breadth-first search, buffering data streams, and other ordered processing.

## Tutorial

1. Create three files: `concurrent_queue_int32_t.h`, `concurrent_queue_int32_t_impl.c`, `main.c` and then `git clone https://codeberg.org/TylerLinuxDev/STL-For-C` for the demonstrative purpose
2. In `concurrent_queue_int32_t.h`, it should have a header guard, have CONCURRENT_QUEUE_TYPE variable defined, and include the header code for queue:
```c
#ifndef CONCURRENT_QUEUE_INT32_T_H
#define CONCURRENT_QUEUE_INT32_T_H

#include <stdint.h> // For int32_t definition

#define CONCURRENT_QUEUE_TYPE int32_t
#include "STL_For_C/include/concurrent_queue.h"

#endif
```

3. in `concurrent_queue_int32_t_impl.c`, it should define CONCURRENT_QUEUE_TYPE variable and include queue implementation:
```c
#include <stdint.h>
#define CONCURRENT_QUEUE_TYPE int32_t
#include "STL_For_C/src/concurrent_queue_impl.c"
```

4. Finally in `main.c`, you can use various functions provided by queue type as followed:

```c
#include "concurrent_queue_int32_t.h"
#include <stdlib.h>
#include <stdio.h>

int main()
{
    concurrent_queue_int32_t* queue = concurrent_queue_int32_t_new();
    if (queue == NULL) {
        printf("Failed to allocate a queue!\n");
        return 1;
    }
    if (concurrent_queue_int32_t_enqueue(queue, 1)) {
        printf("Failed to enqueue item to queue!\n");
        return 1;
    }
    int32_t item = 0;
    if (concurrent_queue_int32_t_dequeue(queue, &item)) {
        printf("Failed to dequeue item from queue!\n");
        return 1;
    }
    if (item != 1)
    {
        printf("Dequeued item does not match with enqueue item!\n");
        return 1;
    }
    printf("Success!\n");

    concurrent_queue_int32_t_destroy(queue);
    return 0;
}
```

5. Run: `CC -oMyProgram -I. concurrent_queue_int32_t_impl.c main.c`


## Files ##
* STL_For_C/include/queue.h
* STL_For_C/src/concurrent_queue_impl.c

## Variables
| Variable Name | Description |
| :-- | :-- |
| **CONCURRENT_QUEUE_TYPE** | Define a type of element in a queue to be preprocessed. <br/> **Example:** ` #define CONCURRENT_QUEUE_TYPE int32_t ` <br/> **Note:** The type name should not contains any space or asterisk otherwise a preprocessing error results, use typedef instead. <br /> **DO:** <br/> `typedef int32_t* ptrToInt32_t;` <br/> `#define CONCURRENT_QUEUE_TYPE ptrToInt32_t` <br /> **DON'T:** <br/> `#define CONCURRENT_QUEUE_TYPE int32_t*` |
| **INDEX_TYPE** | Define an integer or floating point type for List to utilize for keeping track of index and sizes. This defaults to uint64_t unless specify otherwise. Struct should not be used for this type and that it should either be a floating point or integer that can be computed with arithmetic. <br/> **DO:** <br/>`#define INDEX_TYPE int32_t` <br/> **DON'T**: <br/> `typedef struct { float a; } MyStruct;` <br/> `#define INDEX_TYPE MyStruct` <br/> **WARNING:** Integer underflow or overflow is still a risk if you chose to use smaller sized data types and if floating point is chosen for index it may results in data corruption, so evaluate whether INDEX_TYPE should be changed with this in mind, if this is to be changed, make sure you keep INDEX_TYPE consistent across all data containers since other data containers may use underlying implementation. Both unsigned and signed integer can be used. |
| **CONCURRENT_QUEUE_MINIMUM_SIZE** | Define the minimum size as an integer or floating point value. This defaults to 16 items capacity whenever queue is being allocated without capacity being specified or when clearing the queue that will results in reallocating the queue back to 32 items capacity. It's recommended to leave this unchanged. <br/> **DO:** <br/> `#define CONCURRENT_QUEUE_MINIMUM_SIZE 32` <br/> **DON'T:** <br/> `#define CONCURRENT_QUEUE_MINIMUM_SIZE -1` <br/> **or** <br/> `#define CONCURRENT_QUEUE_MINIMUM_SIZE int` <br/> **WARNING:** If specified to 0 or below items capacity then arthimetric and memory allocation errors will result. |
| **CONCURRENT_QUEUE_MAXIMUM_CAPACITY** | Define the maximum size as an integer or floating point value. This defaults to 128 items capacity when queue is being allocated without capacity being specified or when filling the queue that will result in reallocation of the queue up to this maximum items capacity. Adjusting this value depends on the anticipated workload and system memory availability. <br/> DO: <br/> `#define CONCURRENT_QUEUE_MAXIMUM_CAPACITY 2048` <br/> DON'T: <br/> `#define CONCURRENT_QUEUE_MAXIMUM_CAPACITY -1` <br/> or <br/> `#define CONCURRENT_QUEUE_MAXIMUM_CAPACITY float` <br/> WARNING: If specified to a negative value, arithmetic and memory allocation errors may occur. A too large value can lead to excessive memory consumption and potential performance degradation due to the increased pressure on the memory subsystem. |
| **ALLOW_SHRINKING_QUEUE** | Define whether to allow queue to be reallocated when dequeued sufficient amount of elements from queue. By default, shrinking queue will only trigger when size of items in queue is below or equals to the quarter of capacity, then resized, the capacity will be halved. This is set to 0 by default if not defined. This should be defined to a 1 or an 0. <br/> **DO:** <br/> `#define ALLOW_SHRINKING_QUEUE 1` <br/> **or** <br/> `#define ALLOW_SHRINKING_QUEUE 0` <br/> **DON'T:** <br/> `#define ALLOW_SHRINKING_QUEUE 2` |

## Functions

Replace X for CONCURRENT_QUEUE_TYPE for the followings:

| Functions | Description |
| :-- | :-- |
| <span style="color: red;">concurrent_queue_X*</span> concurrent_queue_X_new() | **Returns:** <span style="color:red;"> concurrent_queue_X* </span> - New allocated queue object <br/> **Description:** Allocates a new concurrent_queue_X object with the default size of 16 or specified CONCURRENT_QUEUE_MINIMUM_SIZE variable items capacity. Returns NULL if fail to allocate new concurrent_queue_X object. |
| <span style="color: red;">concurrent_queue_X*</span> concurrent_queue_X_new_withcapacity(<span style="color:green;">INDEX_TYPE initialCapacity</span>) | **Parameter 1:** <span style="color:green;">INDEX_TYPE initialCapacity</span> - Number of elements size capacity that queue should be allocated for, must exceed CONCURRENT_QUEUE_MINIMUM_SIZE variable otherwise CONCURRENT_QUEUE_MINIMUM_SIZE will be used instead of initialCapacity parameter. <br/> **Returns:** <span style="color: red;">concurrent_queue_X*</span> - New allocated queue object with specified capacity or CONCURRENT_QUEUE_MINIMUM_SIZE capacity. <br/> **Description:** Allocates a new concurrent_queue_X object with the specified initial capacity or CONCURRENT_QUEUE_MINIMUM_SIZE variable capacity. Returns NULL if fail to allocate new concurrent_queue_X object. |
| <span style="color: red;">bool</span> concurrent_queue_X_destroy(<span style="color:green;">concurrent_queue_X* this</span>) | **Parameter 1:** <span style="color:green;">concurrent_queue_X* this</span> - Allocated concurrent_queue_X object to be destroyed. <br/> **Returns:** <span style="color:red;">bool</span> - Returns true on error when disposing `this` parameter, otherwise false on successful disposal. <br/> **Description:** Dispose allocated concurrent_queue_X object and return false on successful disposal. Returns true if an error occurs during disposal. Elements stored in `this` queue will not be disposed, those memories must be managed separately. |
| <span style="color:red;">bool</span> concurrent_queue_X_clear(<span style="color:green;">concurrent_queue_X* this</span>) | **Parameter 1:** <span style="color:green;">concurrent_queue_X* this</span> - Allocated concurrent_queue_X object to be cleared. <br/> **Returns:** <span style="color:red;">bool</span> - Returns true on error when clearing `this` queue, otherwise returns false on success. <br/> **Description:** Clear `this` queue by reallocating the queue to CONCURRENT_QUEUE_MINIMUM_SIZE capacity and set the size of queue to zero. Memory for each element stored in this queue must be managed separately. Returns true on error, otherwise returns false on success. |
| <span style="color:red;">bool</span> concurrent_queue_X_getcount(<span style="color:green;">concurrent_queue_X* this</span>, <span style="color:purple;">INDEX_TYPE* outCount</span>) | **Parameter 1:** <span style="color:green;">concurrent_queue_X* this</span> - Allocated concurrent_queue_X object to get count of elements from. <br/> **Parameter 2:** <span style="color:purple;">INDEX_TYPE* outCount</span> - Pointer to INDEX_TYPE variable to write count of elements in queue to. <br/> **Returns:** Returns true if either `this` parameter or `outCount` parameter are NULL or invalid, otherwise returns false on success. <br/> **Description:** Obtain a count of elements stored in `this` queue object and write the count of elements in queue to `outCount` pointer. Returns true on error if either `this` or `outCount` are NULL or invalid, otherwise returns false on success. |
| <span style="color:red;">bool</span> concurrent_queue_X_reallocate(<span style="color:green;">concurrent_queue_X* this</span>, <span style="color:purple;">INDEX_TYPE newSize</span>) | **Parameter 1:** <span style="color:green;">concurrent_queue_X* this</span> - Allocated concurrent_queue_X object to be reallocated. <br/> **Parameter 2:** <span style="color:purple;">INDEX_TYPE newSize</span> - New capacity size to reallocate `this` queue to. <br/> **Returns:** <span style="color:red;">bool</span> - Returns true on error during reallocation of `this` queue, otherwise returns false on success. <br/> **Description:** Reallocate `this` queue by specified `newSize` parameter and the size specified must exceed the current stored number of elements in the queue. The elements stored in queue will be rearranged to allows for capacity reallocation, so a performance penalty during reallocation can occurs. Returns true on error, otherwise returns false on success. |
| <span style="color:red">bool</span> concurrent_queue_X_enqueue(<span style="color:green;">concurrent_queue_X* this</span>, <span style="color:purple;">X item</span>, <span style="color:teal;">INDEX_TYPE* outEnqueuedCount</span>) | **Parameter 1:** <span style="color:green;">concurrent_queue_X* this</span> - Allocated concurrent_queue_X object to be enqueued to. <br/> **Parameter 2:** <span style="color:purple;">X item</span> - Item to be enqueued into `this` queue. <br/> **Parameter 3:** <span style="color:teal;">INDEX_TYPE* outEnqueuedCount</span> - An optional out parameter to store count of enqueued item, can be NULL. <br/> **Returns:** <span style="color:red">bool</span> - Returns true on error, otherwise return false on success. <br/> **Description:** Enqueue `item` into `this` queue and return the count of enqueue item that was successfully added to queue in `outEnqueuedCount`. `outEnqueuedCount` can be NULL if enqueued item count is not desired. If item exceed capacity of `this` queue, the queue may automatically reallocate to accommodate a new entry. Returns true on error, otherwise returns false on success. |
| <span style="color:red">bool</span> concurrent_queue_X_enqueue_items(<span style="color:green;">concurrent_queue_X* this</span>, <span style="color:purple;">X* items</span>, <span style="color:teal;">INDEX_TYPE items_index</span>, <span style="color:orange;">INDEX_TYPE items_length</span>, <span style="color:brown;">INDEX_TYPE* outEnqueuedCount</span>) | **Parameter 1:** <span style="color:green;">concurrent_queue_X* this</span> - Allocated concurrent_queue_X object to be enqueued to. <br/> **Parameter 2:** <span style="color:purple;">X* items</span> - Pointer to memory containing a range of elements to be enqueued to queue. <br/> **Parameter 3:** <span style="color:teal;">INDEX_TYPE items_index</span> - The starting index of items to be enqueued, if items contains valid starting point, then this should be set to 0. <br/> **Parameter 4:** <span style="color:orange;">INDEX_TYPE items_length</span> - Number of elements to be enqueued on and after `items_index` index. <br/> **Parameter 5:** <span style="color:brown;">INDEX_TYPE* outEnqueuedCount</span> - An optional out parameter to store count of enqueued item, can be NULL. <br/> **Returns:** <span style="color:red">bool</span> - Returns true on error, otherwise return false on success. <br/> **Description:** Enqueue a range of `item` defined by `items_index` and `items_length` into `this` queue and returns the count of enqueued item that was successfully added to queue in `outEnqueuedCount`. `outEnqueuedCount` can be NULL if enqueued item count is not desired. If item exceed capacity of `this` queue, the queue may automatically reallocate to accommodate a new entry. Returns true on error, otherwise returns false on success. |
| <span style="color:red;">bool</span> concurrent_queue_X_dequeue(<span style="color:green;">concurrent_queue_X* this</span>, <span style="color:purple;">X* outItem</span>, <span style="color:teal;">INDEX_TYPE* outDequeueCount</span>) | **Parameter 1:** <span style="color:green;">concurrent_queue_X* this</span> - Allocated concurrent_queue_X object to dequeue from. <br/> **Parameter 2:** <span style="color:purple;">X* outItem</span> - Pointer to variable for dequeue item to be written into. <br/> **Parameter 3:** <span style="color:teal;">INDEX_TYPE* outDequeueCount</span> - An optional pointer to INDEX_TYPE to write number of successful pop counts to, can be set to NULL. <br/> **Returns:** <span style="color:red;">bool</span> - Returns true on error, otherwise return false on success. <br/> **Description:** Dequeue an item from `this` queue and write the dequeued item into `outItem` parameter and write the count of successful pop count into `outDequeueCount` parameter if not NULL. Returns true on error, otherwise return false for success. |
| <span style="color:red;">bool</span> concurrent_queue_X_dequeue_items(<span style="color:green;">concurrent_queue_X* this</span>, <span style="color:purple;">INDEX_TYPE count</span>, <span style="color:teal;">X* outItems</span>, <span style="color:orange;">INDEX_TYPE* outDequeueCount</span>) | **Parameter 1:** <span style="color:green;">concurrent_queue_X* this</span> - Allocated concurrent_queue_X object to dequeue from. <br/> **Parameter 2:** <span style="color:purple;">INDEX_TYPE count</span> - Number of items to be dequeued from `this` queue. <br/> **Parameter 3:** <span style="color:teal;">X* outItems</span> - Pointer to memory containing elements for dequeued items to be written into. <br/> **Parameter 4:** <span style="color:orange;">INDEX_TYPE* outDequeueCount</span> - An optional pointer to INDEX_TYPE to write number of successful pop counts to, can be set to NULL. <br/> **Returns:** <span style="color:red;">bool</span> - Returns true on error, otherwise return false on success. <br/> **Description:** Dequeue a `count` of items from `this` queue and write the dequeued items range into `outItems` parameter and write the count of successful dequeue count into `outDequeueCount` parameter if not NULL. Returns true on error, otherwise return false for success. |
| <span style="color:red;">bool</span> concurrent_queue_X_peek(<span style="color:green;">concurrent_queue_X* this</span>, <span style="color:purple;">X* outItem</span>, <span style="color:teal;">INDEX_TYPE* outPeekCount</span>) | **Parameter 1:** <span style="color:green;">concurrent_queue_X* this</span> - Allocated concurrent_queue_X object to peek into. <br/> **Parameter 2:** <span style="color:purple;">X* outItem</span> - Pointer to variable for peek item to be written into. <br/> **Parameter 3:** <span style="color:teal;">INDEX_TYPE* outPeekCount</span> - An optional pointer to INDEX_TYPE to write number of successful peek counts to, can be set to NULL. <br/> **Returns:** <span style="color:red;">bool</span> - Returns true on error, otherwise return false on success. <br/> **Description:** Peek into `this` queue and obtain an item without advancing the queue and write the peeked item into `outItem` parameter and write the count of successful peek count into `outPeekCount` parameter if not NULL. Returns true on error, otherwise return false for success. |
| <span style="color:red;">bool</span> concurrent_queue_X_peek_items(<span style="color:green;">concurrent_queue_X* this</span>, <span style="color:purple;">INDEX_TYPE count</span>, <span style="color:teal;">X* outItems</span>, <span style="color:orange;">INDEX_TYPE* outPeekCount</span>) | **Parameter 1:** <span style="color:green;">concurrent_queue_X* this</span> - Allocated concurrent_queue_X object to peek into. <br/> **Parameter 2:** <span style="color:purple;">INDEX_TYPE count</span> - Number of items to be peek into `this` queue. <br/> **Parameter 3:** <span style="color:teal;">X* outItems</span> - Pointer to memory containing elements for peek items to be written into. <br/> **Parameter 4:** <span style="color:orange;">INDEX_TYPE* outPeekCount</span> - An optional pointer to INDEX_TYPE to write a number of successful peek counts to, can be set to NULL. <br/> **Returns:** <span style="color:red;">bool</span> - Returns true on error, otherwise return false on success. <br/> **Description:** Peek `count` of items into `this` queue without advancing the queue and write the peeked items range into `outItems` parameter and write the count of successful peeked count into `outPeekCount` parameter if not NULL. Returns true on error, otherwise return false for success. |
## Implementation Specific Notes
:information_source: This implementation of Queue use circular buffer internally to minimize the use of reallocation. The implementation make extensive use of memcpy for most of it's SIMD optimization giving you the best performance result comparable to other implementation of queue.

:warning: It should be noted that anytime an element is added to the queue exceeding it's capacity, it will reallocates doubling the size of capacity. When the capacity exceed quarter of elements after removing items, it will also reallocate to shrink capacity halving it's size.

Thread Safety: ✅ - This implementation is thread safe, it should be noted that this implementation will incur some performance overhead.
