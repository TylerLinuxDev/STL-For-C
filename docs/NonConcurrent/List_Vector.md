# List Non-Concurrent

## Brief

The list or vector is a fundamental data structure that implements a dynamic array, providing fast constant time index-based access and modification to a sequential collection of elements. A list manages an underlying contiguous buffer that is reallocated and resized as needed when elements are inserted or removed. This enables efficiently appending elements and random access without inserting/removing overhead of linked lists. Iteration over elements is cache-friendly due to memory locality.

## Tutorial

1. Create three files: `list_int32_t.h`, `list_int32_t_impl.c`, `main.c` and then `git clone https://codeberg.org/TylerLinuxDev/STL-For-C` for the demonstrative purpose
2. In `list_int32_t.h`, it should have a header guard, have LIST_TYPE variable defined, and include the header code for list:
```c
#ifndef LIST_INT32_T_H
#define LIST_INT32_T_H

#include <stdint.h> // For int32_t definition

#define LIST_TYPE int32_t
#include "STL_For_C/include/list.h"

#endif
```

3. in `list_int32_t_impl.c`, it should define LIST_TYPE variable and include list implementation:
```c
#include <stdint.h>
#define LIST_TYPE int32_t
#include "STL_For_C/src/list_impl.c"
```

4. Finally in `main.c`, you can use various functions provided by list type as followed:

```c
#include "list_int32_t.h"
#include <stdlib.h>
#include <stdio.h>

int main()
{
    list_int32_t* list = list_int32_t_new();
    if (list == NULL) {
        printf("Failed to allocate a list!\n");
        return 1;
    }
    if (list_int32_t_add(list, 1)) {
        printf("Failed to add item to list!\n");
        return 1;
    }
    if (list_int32_t_remove_item(list, 1)) {
        printf("Failed to remove item from list!\n");
        return 1;
    }
    return 0;
}
```

5. Run: `CC -oMyProgram -I. list_int32_t_impl.c main.c`


## Files ##
* STL_For_C/include/list.h
* STL_For_C/src/list_impl.c

## Variables
<table>
<tr><td><font size="5"><b>Variable Name</b></font></td><td><font size="5"><b>Description</b></font></td>
<tr><td><b>LIST_TYPE</b></td><td>Define a type of element in a List to be preprocessed. <br/> <b>Example:</b> 

`#define LIST_TYPE int32_t `

<b>Note:</b> The type name should not contains any space or asterisk otherwise a preprocessing error results, use typedef instead. <br /> <b>DO:</b> <br/> 

```c
typedef int32_t* ptrToInt32_t;
#define LIST_TYPE ptrToInt32_t
```

<b>DON'T:</b> <br/> 

`#define LIST_TYPE int32_t*`

</td></tr>
<tr><td><b>INDEX_TYPE</b></td><td>Define an integer or floating point type for List to utilize for keeping track of index and sizes. This defaults to uint64_t unless specify otherwise. Struct should not be used for this type and that it should either be a floating point or integer that can be computed with arithmetic. <br/> <b>DO:</b> <br/>

`#define INDEX_TYPE int32_t`

<b>DON'T</b>:

```c
typedef struct { float a; } MyStruct;
#define INDEX_TYPE MyStruct
```

<b>WARNING:</b> Integer underflow or overflow is still a risk if you chose to use smaller sized data types and if floating point is chosen for index it may results in data corruption, so evaluate whether INDEX_TYPE should be changed with this in mind, if this is to be changed, make sure you keep INDEX_TYPE consistent across all data containers since other data containers may use underlying implementation. Both unsigned and signed integer can be used.</td></tr>
<tr><td><b>MINIMUM_LIST_SIZE</b></td><td>Define the minimum size as an integer or floating point value. This defaults to 32 items capacity whenever list is being allocated without capacity being specified or when clearing the list that will results in reallocating the list back to 32 items capacity. It's recommended to leave this unchanged. <br/> <b>DO:</b>

`#define MINIMUM_LIST_SIZE 32`

<b>DON'T:</b> 

`#define MINIMUM_LIST_SIZE -1`

<b>or</b> 

`#define MINIMUM_LIST_SIZE int`

<b>WARNING:</b> If specified to 0 or below items capacity then arthimetric and memory allocation errors will result.</td></tr>
<tr><td><b>LIST_TYPE_CMP_FUNCTION</b></td><td>Define a custom list item comparison function by function name reflecting the following function prototype: 

```c
bool Compare(LIST_TYPE itemA, LIST_TYPE itemB)
```

The function should returns true for matching LIST_TYPE items and otherwise false if those items doesn't match. If LIST_TYPE_CMP_FUNCTION is not defined, then memcmp function will be used instead. If for instance that LIST_TYPE is defined as an integer, the memcmp can be optimized out by compiler to a simple integer comparison. LIST_TYPE_CMP_FUNCTION may be necessary for a more complex data structures. <br/> <b>DO:</b> <br/>

```c
#include <math.h>
#include <float.h>

typedef struct CustomStruct {
  float a;
  int b;
} CustomStruct;

bool CustomStruct_Compare(CustomStruct itemA, CustomStruct itemB)
{
    return (fabs(itemA.a - itemB.a) < FLT_EPSILON) && (itemA.b == itemB.b);
}

#define LIST_TYPE_CMP_FUNCTION CustomStruct_Compare
#define LIST_TYPE CustomStruct
#include "list.h"
```
</td></tr>
</table>
## Functions

Replace X for LIST_TYPE for the followings:

| Functions | Description |
| :-- | :-- |
| <span style="color:red;">list_X*</span> list_X_new() | **Returns:** <span style="color:red;">list_X*</span> - New allocated list object <br/> **Description:** Allocates a new list_X with the default size of 32 or specified MINIMUM_LIST_SIZE variable items capacity. |
| <span style="color:red">list_X*</span> list_X_new_withcapacity(<span style="color:green;">INDEX_TYPE initialCapacity</span>) | **Parameter 1:** <span style="color:green;">INDEX_TYPE initialCapacity</span> - Initial allocation size of initialCapacity amount of items, if it is specified below MINIMUM_LIST_SIZE variable, it will be set to MINIMUM_LIST_SIZE value instead. <br/> **Returns:** <span style="color:red;">list_X*</span> - New allocated list object <br/> **Description:** Allocates a new list_X with the specified size from initialCapacity parameter for items capacity. |
| <span style="color:red">bool</span> list_X_destroy(<span style="color:green;">list_X* this</span>) | **Parameter 1:** <span style="color:green;">list_X* this</span> - The allocated list referenced for this function. <br/> **Returns:** <span style="color:red;">bool</span> - Returns true if list is NULL, otherwise false. <br/> **Description:** Dispose any memory allocated by list_X object, it should be noted that it does not mean any memory resides in each element contained in the list to be disposed as well, those elements should be managed separately. |
| <span style="color:red;">bool</span> list_X_indexof(<span style="color:green;">list_X* this</span>, <span style="color: purple">X searchItem</span>, <span style="color:teal;">INDEX_TYPE* outIndex</span>) | **Parameter 1:** <span style="color:green;">list_X* this</span> - The allocated list referenced for this function. <br/> **Parameter 2:** <span style="color:purple;">X searchItem</span> - A specific element item to be searched within List object provided by `this` parameter. <br/> **Parameter 3:** <span style="color:teal;">INDEX_TYPE* outIndex</span> - Pointer to index type to be overwritten with an 0-based Index of item found, otherwise this index will not be written upon failure. <br/> **Returns:** <span style="color:red;">bool</span> - Returns true on failure to find object, index being specified out of range or `this` parameter being invalid, otherwise returns false on success. <br/>  **Description:** Attempts a linear search through the List type to obtain an 0-based index of found element within the list, otherwise returns true if item is not found within the list. |
| <span style="color:red">bool</span> list_X_indexof2(<span style="color:green">list_X* this</span>, <span style="color:purple">X searchItem</span>, <span style="color:teal">INDEX_TYPE start</span>, <span style="color:orange">INDEX_TYPE* outIndex</span>) | **Parameter 1:** <span style="color:green">list_X* this</span> - The allocated list referenced for this function. <br/> **Parameter 2:** <span style="color:purple">X searchItem</span> - A specific element item to be searched within List object provided by `this` parameter. <br/> **Parameter 3:** <span style="color:teal">INDEX_TYPE start</span> - An index within the List to begin the search from, if this exceed the range of List, the function will returns true for exceeding the range. <br/> **Parameter 4:** <span style="color:orange">INDEX_TYPE* outIndex</span> - Pointer to index type to be overwritten with an 0-based Index of item found, otherwise this index will not be written upon failure. <br/> **Returns:** <span style="color:red">bool</span> - Returns true on failure to find object, index being specified out of range or `this` parameter being invalid, otherwise returns false for success. <br/> **Description:** Attempts a linear search through the List type starting from `start` parameter index to obtain an 0-based index of found element within the list and write into outIndex parameter, otherwise returns true if item is not found within the list. |
| <span style="color:red">bool</span> list_X_indexof3(<span style="color:green">list_X* this</span>, <span style="color:purple">X searchItem</span>, <span style="color:teal">INDEX_TYPE start</span>, <span style="color:orange">INDEX_TYPE length</span>, <span style="color:brown">INDEX_TYPE* outIndex</span>) | **Parameter 1:** <span style="color:green">list_X* this</span> - The allocated list referenced for this function. <br/> **Parameter 2:** <span style="color:purple">X searchItem</span> - A specific element item to be searched within List object provided by `this` parameter. <br/> **Parameter 3:** <span style="color:teal">INDEX_TYPE start</span> - An index within the List to begin the search from, if this exceed the range of List, the function will returns true for item not found. <br/> **Parameter 4:** <span style="color:orange">INDEX_TYPE length</span> - A count of elements ahead of `start` parameter creating a range of items to search through saving computational cost. <br/> **Parameter 5:** <span style="color:brown">INDEX_TYPE* outIndex</span> - Pointer to index type to be overwritten with an 0-based Index of item found, otherwise this index will not be written upon failure. <br/> **Returns:** <span style="color:red">bool</span> - Returns true on failure to find object, index being specified out of range or `this` parameter being invalid, otherwise returns false on success. <br/> **Description:** Attempts a linear search through the List type starting from `start` parameter index and the `length` parameter creating a range of items to search through to obtain an 0-based index of found element within the list and write into outIndex parameter, otherwise returns true if item is not found within the list. |
| <span style="color:red">bool</span> list_X_get(<span style="color:green">list_X* this</span>, <span style="color:purple">INDEX_TYPE index</span>, <span style="color:teal;">LIST_TYPE* result</span>) | **Parameter 1:** <span style="color:green">list_X* this</span> - The allocated list referenced for this function. <br/> **Parameter 2:** <span style="color:purple">INDEX_TYPE index</span> - Position within the list where specific element locates in. <br/> **Parameter 3:** <span style="color:teal">LIST_TYPE* result</span> - An out parameter where the content of result will be overwritten by the content of the element found from `index` parameter in the list. <br/> **Returns:** <span style="color:red">bool</span> - Returns true if an error occurs during it's attempt to obtain an element from the list, this may include Index being out of range, `result` parameter is NULL, or `this` parameter is NULL. Otherwise returns false on success. <br/> **Description:** Attempts an element lookup provided by `index` parameter and overwrite `result` out parameter. Returns true if an error occurs, otherwise false for successful operation. |
| <span style="color:red">bool</span> list_X_getrange(<span style="color:green">list_X* this</span>, <span style="color:purple">INDEX_TYPE index</span>, <span style="color:teal">INDEX_TYPE length</span>, <span style="color:orange">X** result</span>) |  **Parameter 1:** <span style="color:green">list_X* this</span> - The allocated list referenced for this function. <br/> **Parameter 2:** <span style="color:purple">INDEX_TYPE start</span> - An index within the List to begin the search from, if this exceed the range of List, the function will returns a -1 for item not found. <br/> **Parameter 3:** <span style="color:teal">INDEX_TYPE length</span> - Number of elements ahead of `start` index creating a range of elements to be retrieved from `this` list. <br/> **Parameter 4:** <span style="color:orange">X** result</span> - An output parameter to be written into. <br/> **Returns:** <span style="color:red">bool</span> - Returns true on failure to obtain the list of elements from list or failure on allocation of memory for `result` parameter, otherwise returns false on success. <br/> **Description:** Attempts to retrieve specific range of elements within the list and allocate a new memory for `result` out parameter to contains the range of elements obtained from the list and overwrite `result` address. Returns true on failure, otherwise false on success. |
| <span style="color:red">bool</span> list_X_add(<span style="color:green">list_X* this</span>, <span style="color:purple">X item</span>) | **Parameter 1:** <span style="color:green">list_X* this</span> - The allocated list referenced for this function. <br/> **Parameter 2:** <span style="color:purple">X item</span> - Item to be added to the list <br/> **Returns:** <span style="color:red">bool</span> - Return true if an error occurs either by memory reallocation or invalid list, otherwise return false on success. <br/> **Description:** Add an `item` to the list, when item being added to the list exceed list's capacity, the list will resize the capacity to be twice the size of capacity size to accommodate new entry. If reallocation fails, this function will returns true for error on reallocation, otherwise returns false for successful operation. |
| <span style="color:red">bool</span> list_X_addrange(<span style="color:green">list_X* this</span>, <span style="color:purple">X* items</span>, <span style="color:teal">INDEX_TYPE items_length</span>) | **Parameter 1:** <span style="color:green">list_X* this</span> - The allocated list referenced for this function. <br/> **Parameter 2:** <span style="color:purple">X* items</span> - Items to be added to the list <br/> **Parameter 3:** <span style="color:teal">INDEX_TYPE items_length</span> - Number of elements in `items` parameter. <br/> **Returns:** Return true if an error occurs either by memory reallocation or invalid list, otherwise return false on success. <br/> **Description:** Add a specified amount (`items_length`) `items` to the list, when items being added to the list exceed list's capacity, the list will resize the capacity to be twice the size of capacity size or more depending on the items_length supplied to accommodate new entries. If reallocation fails, this function will returns true for error on reallocation, otherwise returns false for successful operation. |
| <span style="color:red">bool</span> list_X_add_at(<span style="color:green">list_X* this</span>, <span style="color:purple">X item</span>, <span style="color:teal">INDEX_TYPE index</span>) | **Parameter 1:** <span style="color:green">list_X* this</span> - The allocated list referenced for this function. <br/> **Parameter 2:** <span style="color:purple">X item</span> - Item to be added to the list <br/> **Parameter 3:** <span style="color:teal">INDEX_TYPE index</span> - Index to specify where to add an entry to the list. <br/> **Returns:** <span style="color:red">bool</span> - Return true if an error occurs either by memory reallocation or invalid list, otherwise return false on success. <br/> **Description:** Add an `item` to the list at a specified index in the list, when item being added to the list exceed list's capacity, the list will resize the capacity to be twice the size of capacity size to accommodate new entry. If reallocation fails, this function will returns true for error on reallocation, otherwise returns false for successful operation. |
| <span style="color:red">bool</span> list_X_addrange_at(<span style="color:green">list_X* this</span>, <span style="color:purple">X* items</span>, <span style="color:teal">INDEX_TYPE items_length</span>, <span style="color:orange">INDEX_TYPE index</span>) | **Parameter 1:** <span style="color:green">list_X* this</span> - The allocated list referenced for this function. <br/> **Parameter 2:** <span style="color:purple">X* items</span> - Items to be added to the list <br/> **Parameter 3:** <span style="color:teal">INDEX_TYPE items_length</span> - Number of elements in `items` parameter. <br/> **Parameter 4:** <span style="color:orange">INDEX_TYPE index</span> - Index to specify where to put a set of items into the list. <br/> **Returns:** <span style="color:red">bool</span> - Return true if an error occurs either by memory reallocation, invalid index or invalid list, otherwise return false on success. <br/> **Description:** Add a specified amount (`items_length`) `items` to the list at a specified index, when items being added to the list exceed list's capacity, the list will resize the capacity to be twice the size of capacity size or more depending on the items_length supplied to accommodate new entries. If reallocation fails, this function will returns true for error on reallocation, invalid index or invalid list, otherwise returns false for successful operation. |
| <span style="color:red">bool</span> list_X_remove(<span style="color:green">list_X* this</span>, <span style="color:purple">INDEX_TYPE index</span>) | **Parameter 1:** <span style="color:green">list_X* this</span> - The allocated list referenced for this function. <br/> **Parameter 2:** <span style="color:purple">INDEX_TYPE index</span> - Index within the list for an item to be removed <br/> **Returns:** <span style="color:red">bool</span> - Return true if an error occurs either by memory reallocation, invalid index or invalid list, otherwise return false on success. <br/> **Description:** Remove an item from the list, when item being removed from the list caused the capacity to exceed 4 times the size of currently stored items in the list, the list will resize the capacity to be half the size of capacity size to reduce excessive memory usage. If reallocation fails, this function will returns true for error on reallocation, otherwise returns false for successful operation. |
| <span style="color:red">bool</span> list_X_removerange(<span style="color:green">list_X* this</span>, <span style="color:purple">INDEX_TYPE index</span>, <span style="color:teal">INDEX_TYPE length</span>) | **Parameter 1:** <span style="color:green">list_X* this</span> - The allocated list referenced for this function. <br/> **Parameter 2:** <span style="color:purple">INDEX_TYPE index</span> - Starting position of the range to be removed from list <br/> **Parameter 3:** <span style="color:teal">INDEX_TYPE length</span> - Number of elements to be removed ahead of `index` parameter. <br/> **Returns:** <span style="color:red">bool</span> - Return true if an error occurs either by memory reallocation, invalid index, invalid length or invalid list, otherwise return false on success. <br/> **Description:** Remove a specified amount (`length`) `items` from the list starting at `index`, when items being removed to the list caused capacity to exceed 4 times of the remaining number of elements stored in list, the list will resize the capacity to be half the size of capacity size or more depending on the `length`. If reallocation fails, this function will returns true for error on reallocation, otherwise returns false for successful operation. |
| <span style="color:red">bool</span> list_X_remove_item(<span style="color:green">list_X* this</span>, <span style="color:purple">X item</span>) | **Parameter 1:** <span style="color:green">list_X* this</span> - The allocated list referenced for this function. <br/> **Parameter 2:** <span style="color:purple">X item</span> - Item to be searched in the list to be removed <br/> **Returns:** <span style="color:red">bool</span> - Return true if an error occurs either by memory reallocation or if item could not be found in the list, otherwise return false on success. <br/> **Description:** Remove an item from the list, when item being removed from the list caused the capacity to exceed 4 times the size of currently stored items in the list, the list will resize the capacity to be half the size of capacity size to reduce excessive memory usage. If reallocation fails, this function will returns true for error on reallocation, otherwise returns false for successful operation. |
| <span style="color:red">bool</span> list_X_clear(<span style="color:green">list_X* this</span>) | **Parameter 1:** <span style="color:green">list_X* this</span> - The allocated list referenced for this function. <br/> **Returns:** Return true if an error occurs if `this` parameter is invalid, otherwise return false on success. <br/> **Description:** Remove all items from the list and reallocate with 32 or MINIMUM_LIST_SIZE variable items capacity, any items removed must be freed separately. Returns true on invalid `this` parameter, otherwise return false on success. |
| <span style="color:red">bool</span> list_X_count(<span style="color:green">list_X* this</span>, <span style="color:purple">INDEX_TYPE* outCount</span>) | **Parameter 1:** <span style="color:green">list_X* this</span> - The allocated list referenced for this function. <br/> **Parameter 2:** <span style="color:purple">INDEX_TYPE* outCount</span> - Pointer to INDEX_TYPE to write into the count of items stored in `this` list object. <br/> **Returns:** <span style="color:red">bool</span> - Returns true for invalid list or an error resulting from attempting to retrieve list count, otherwise returns false on success.. <br/> **Description:** Returns true on invalid `this` parameter, otherwise returns false on success and write a number of items stored in `this` list into `outCount` parameter. |
| <span style="color:red">bool</span> list_X_swap(<span style="color:green">list_X* this</span>, <span style="color:purple">INDEX_TYPE itemIndex1</span>, <span style="color:teal">INDEX_TYPE itemIndex2</span>) | **Parameter 1:** <span style="color:green">list_X* this</span> - The allocated list referenced for this function. <br/> **Parameter 2:** <span style="color:purple">INDEX_TYPE itemIndex1</span> - Index to the first item in the list to be swapped with another item placement in the list.  <br/> **Parameter 3:** <span style="color:teal">INDEX_TYPE itemIndex2</span> - Index to the second item in the list to be swapped with first item placement in the list. <br/> **Description:** Returns true on invalid `this` parameter, invalid index parameters that fall outside of the range held in list, The two items specified by index in the list will be swapped and return false on success. |

## Implementation Specific Notes
:warning: It should be noted that anytime an element is added to the List exceeding it's capacity, it will reallocates doubling the size of capacity. When the capacity exceed quarter of elements after removing items, it will also reallocate to shrink capacity halving it's size. 

Thread Safety: ❌  - This implementation is not meant to be thread safe, you will need to refer to [concurrent_list](TODO) for thread safety.
