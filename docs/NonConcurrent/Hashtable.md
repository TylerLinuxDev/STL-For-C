# Hash-Table/Dictionary Non-Concurrent

## Brief

A hash table is a data structure that maps keys to values for highly efficient lookup and insertion operations. It accomplishes this by using a hash function to compute an index into an array of buckets from which the desired value can be found.

## Tutorial

1. Create three files: `hashtable_int32_t.h`, `hashtable_int32_t_impl.c`, `main.c` and then `git clone https://codeberg.org/TylerLinuxDev/STL-For-C` for the demonstrative purpose
2. In `hashtable_int32_t.h`, it should have a header guard, have EVENT_ARG_TYPE variable defined, and include the header code for hashtable:
```c
#ifndef HASHTABLE_INT32_T_H
#define HASHTABLE_INT32_T_H

#include <stdint.h> // For int32_t definition

#define HASH_TABLE_VALUE_TYPE int32_t
#include "STL_For_C/include/hash_table.h"

#endif
```

3. in `hashtable_int32_t_impl.c`, it should define HASH_TABLE_VALUE_TYPE variable and include hashtable implementation:
```c
#include <stdint.h>
#include "hashtable_int32_t.h"

#define HASH_TABLE_VALUE_TYPE int32_t
#include "STL_For_C/src/event_impl.c"
```

4. Finally in `main.c`, you can use various functions provided by event type as followed:

```c
#include "hashtable_int32_t.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#pragma pack(1)
typedef struct {
    int32_t val;
    int32_t pad0;
} paddedIntegerStack;

int main()
{
    hash_table_int32_t* table = hash_table_int32_t_new();
    paddedIntegerStack originalKey = (paddedIntegerStack){.val = 16, .pad0 = 0};
    hash_table_int32_t_set(table, (const char*) &originalKey.val, 321, NULL);
    paddedIntegerStack realKey = (paddedIntegerStack){.val = 0, .pad0 = 0};
    for (int i = 0; i < 64; ++i)
    {
        realKey.val = i;
        if (!hash_table_int32_t_get(table, (const char*) &realKey.val, &val))
            break;
    }
    printf("%i == 16 = %s\n", val, (val == 16 ? "True" : "False"));
    hash_table_int32_t_destroy(table);
    
    return 0;
}
```

5. Run: `CC -oMyProgram -I. hashtable_int32_t_impl.c main.c`


## Files ##
* STL_For_C/include/hashtable.h
* STL_For_C/src/hashtable_impl.c

## Variables
<table>
<tr><td><font size="5"><b>Variable Name</b></font></td><td><font size="5"><b>Description</b></font></td>
<tr><td><b>HASH_TABLE_VALUE_TYPE</b></td><td>Define a type of value for hash_table look up. <br/> <b>Example:</b> 

```c
#define HASH_TABLE_VALUE_TYPE int32_t
```

<b>Note:</b> The type name should not contains any space or asterisk otherwise a preprocessing error results, use typedef instead. <br /> <b>DO:</b> <br/> 

```c
typedef int32_t* ptrToInt32_t;
#define HASH_TABLE_VALUE_TYPE ptrToInt32_t
```

<b>DON'T:</b> <br/> 

```c
#define HASH_TABLE_VALUE_TYPE int32_t*
```

</td></tr>
<tr><td><b>INDEX_TYPE</b></td><td>Define an integer or floating point type for List to utilize for keeping track of index and sizes. This defaults to uint64_t unless specify otherwise. Struct should not be used for this type and that it should either be a floating point or integer that can be computed with arithmetic. <br/> <b>DO:</b> <br/>

`#define INDEX_TYPE int32_t`

<b>DON'T</b>:

```c
typedef struct { float a; } MyStruct;
#define INDEX_TYPE MyStruct
```

<b>WARNING:</b> Integer underflow or overflow is still a risk if you chose to use smaller sized data types and if floating point is chosen for index it may results in data corruption, so evaluate whether INDEX_TYPE should be changed with this in mind, if this is to be changed, make sure you keep INDEX_TYPE consistent across all data containers since other data containers may use underlying implementation. Both unsigned and signed integer can be used.</td></tr>
</table>
## Functions

Replace X for HASH_TABLE_VALUE_TYPE for the followings:

| Functions | Description |
| :-- | :-- |
| <span style="color:red;">hash_table_X\*</span> hash_table_X_new() | **Returns:** <span style="color:red;">hash_table_X\*</span> - New allocated event object <br/> **Description:** Allocates a new hash_table_X with the default size capacity. |
| <span style="color:red">bool</span> hash_table_X_destroy(<span style="color:green;">hash_table_X\* this</span>) | **Parameter 1:** <span style="color:green;">hash_table_X* this</span> - The allocated hash_table_X referenced for this function. <br/> **Returns:** <span style="color:red;">bool</span> - Returns true if hash_table_X is NULL, otherwise false. <br/> **Description:** Dispose any memory allocated by hash_table_X object, it should be noted that it does not mean any memory resides in each entry contained in the hash table to be disposed as well, those entries should be managed separately. Key and value entries can be freed by iterating through hash table prior to calling hash_table_X_destory function if those keys and values aren't being used by any other part of the running program. |
| <span style="color:red">bool</span> hash_table_X_get(<span style="color:green">hash_table_X* this</span>, <span style="color:purple">const char* key</span>, <span style="color:teal">X* outValue</span>) | **Parameter 1:** <span style="color:green">hash_table_X* this</span> - The allocated hash_table_X referenced for this function. <br/> **Parameter 2:** <span style="color:purple">const char* key</span> - Key to look up for in hash table. <br/> **Parameter 3:** <span style="color:teal;">X* outValue</span> - Pointer to value to write into when matching key entry is found. <br/> **Returns:** <span style="color:red">bool</span> - Return true if an error occurs by either invalid hash_table_X or no entry is found, otherwise return false on success. <br/> **Description:** Attempts to find a matching key entry within `this` hash table and write the value of found entry into `outValue` pointer, return true on failure to find matching key entry or invalid pointer, otherwise return false on success. |
| <span style="color:red">bool</span> hash_table_X_remove(<span style="color:green">event_X* this</span>, <span style="color:purple">const char* key</span>) | **Parameter 1:** <span style="color:green">event_X* this</span> - The allocated event referenced for this function. <br/> **Parameter 2:** <span style="color:purple">const char* key</span> - Key for matching entry to be deleted. <br/> **Returns:** <span style="color:red">bool</span> - Return true if an error occurs either by memory reallocation, invalid key or invalid hash_table_X, otherwise return false on success. <br/> **Description:** Remove the matching key entry from `this` hash table, return true on failure to find matching key entry or invalid `this` pointer, otherwise returns false on success. |
| <span style="color:red">bool</span> hash_table_X_set(<span style="color:green">event_X* this</span>, <span style="color:purple">const char* key</span>, <span style="color:teal;">X value</span>, <span style="color:orange">const char\*\* outRealKey</span>) | **Parameter 1:** <span style="color:green">event_X* this</span> - The allocated event referenced for this function. <br/> **Parameter 2:** <span style="color:purple">const char* key</span> - Key for given entry in hash table <br/> **Parameter 3:** <span style="color:teal;">X value</span> - Value of entry associated with key. <br/> **Parameter 4:** <span style="color:orange">const char\*\* outRealKey</span> - An alternative key in case a colliding key is found that matches with given `key`. <br/> **Returns:** Return true if an error occurs if `this` parameter is invalid, otherwise return false on success. <br/> **Description:** Add a new entry to `this` table with the pair of key and value, if a colliding key is found, the `outRealKey` would be written to provide an alternative non-colliding key. Returns true on failure to set entry in `this` hash table or `this` is an invalid pointer, otherwise returns false on success. |
| <span style="color:red">bool</span> hash_table_X_count(<span style="color:green">event_X* this</span>, <span style="color:purple">INDEX_TYPE* outCount</span>) | **Parameter 1:** <span style="color:green">event_X* this</span> - The allocated hash_table_X referenced for this function. <br/> **Parameter 2:** <span style="color:purple">INDEX_TYPE* outCount</span> - Pointer to INDEX_TYPE to write into the count of entries stored in `this` event object. <br/> **Returns:** <span style="color:red">bool</span> - Returns true for invalid hash_table_X or an error resulting from attempting to retrieve entries count, otherwise returns false on success. <br/> **Description:** Returns true on invalid `this` parameter, otherwise returns false on success and write a number of entries stored in `this` hash table into `outCount` parameter. |
| <span style="color:red">bool</span> hash_table_X_getiterator(<span style="color:green">hash_table_X* this</span>, <span style="color:purple">hash_table_iterator_X* outIterator</span>) | **Parameter 1:** <span style="color:green">hash_table_X* this</span> - The allocated hash_table_X referenced for this function. <br/> **Parameter 2:** <span style="color:purple">hash_table_iterator_X* outIterator</span> - Pointer to iterator object to write into to iterate through `this` hash table. <br/> **Description:** Returns true on invalid `this` parameter, otherwise returns false on success and write `outIterator` parameter for iterating through `this` hash table. |
| <span style="color:red">bool</span> hash_table_X_iterator_next(<span style="color:green">hash_table_iterator_X* this</span>) | **Parameter 1:** <span style="color:green">hash_table_iterator_X* this</span> - Pointer to iterator to advance to next entry in the hash table. <br/> **Description:** Returns true on invalid `this` parameter, otherwise returns false on success and advances `this` iterator to the next entry in the hash table. |
## Implementation Specific Notes
:warning: It should be noted that anytime an entry is added to the hash table exceeding it's capacity, it will reallocates doubling the size of capacity. When the capacity exceed quarter of entries after removing items, it may also reallocate to shrink capacity halving it's size. 

Thread Safety: ❌  - This implementation is not meant to be thread safe, you will need to refer to [concurrent_hashtable](TODO) for thread safety.
