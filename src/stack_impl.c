/*
 * Copyright (c) 2023 Tyler Crandall
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef STACK_TYPE
#error There must be a provided type for STACK_TYPE and it must be defined prior to using this code!
#define STACK_TYPE int32_t // To make VSCode happy
#endif

#ifndef INDEX_TYPE
#define INDEX_TYPE uint64_t
#endif

#ifndef STL_FOR_C_IMPLEMENTATION_ONLY
#define STL_FOR_C_IMPLEMENTATION_ONLY
#endif

#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#define MAKE_STACK_NAME(x) stack_##x
#define STACK_NAME(x) MAKE_STACK_NAME(x)
#define STACK STACK_NAME(STACK_TYPE)

#ifndef DEFAULT_SIZE
#define DEFAULT_SIZE 32
#endif

typedef struct STACK
{
    STACK_TYPE *items;
    INDEX_TYPE tail;
    INDEX_TYPE size;
    INDEX_TYPE capacity;
} *STACK;

#define MAKE_STACK_NEW_NAME(x) x##_new()
#define GEN_NEW_NAME(x) MAKE_STACK_NEW_NAME(x)

STACK GEN_NEW_NAME(STACK)
{
    STACK newStack = (STACK)malloc(sizeof(struct STACK));
    if (newStack == NULL)
        return NULL;
    newStack->items = (STACK_TYPE *)malloc(sizeof(STACK_TYPE) * DEFAULT_SIZE);
    if (newStack->items == NULL)
    {
        free(newStack);
        return NULL;
    }
    newStack->tail = 0;
    newStack->size = 0;
    newStack->capacity = DEFAULT_SIZE;
    return newStack;
}

#undef GEN_NEW_NAME
#undef MAKE_STACK_NEW_NAME

#define MAKE_STACK_DESTROY_NAME(x) x##_destroy(STACK this)
#define GEN_DESTROY_NAME(x) MAKE_STACK_DESTROY_NAME(x)

bool GEN_DESTROY_NAME(STACK)
{
    if (this == NULL)
        return false;
    if (this->items != NULL)
        free(this->items);
    free(this);
    return false;
}

#undef GEN_DESTROY_NAME
#undef MAKE_STACK_DESTROY_NAME

#define MAKE_STACK_CLEAR_NAME(x) x##_clear(STACK this)
#define GEN_CLEAR_NAME(x) MAKE_STACK_CLEAR_NAME(x)

bool GEN_CLEAR_NAME(STACK)
{
    if (this == NULL)
        return true;

    if (this->size > DEFAULT_SIZE)
    {
        STACK_TYPE* ptr = (STACK_TYPE*) realloc(this->items, DEFAULT_SIZE);
        if (ptr == NULL)
            return true;
        this->items = ptr;
        this->capacity = DEFAULT_SIZE;
    }
    this->tail = 0;
    this->size = 0;
    return false;
}

#undef GEN_CLEAR_NAME
#undef MAKE_STACK_CLEAR_NAME

#define MAKE_STACK_GETCOUNT_NAME(x) x##_getcount(STACK this, INDEX_TYPE *outCount)
#define GEN_GETCOUNT_NAME(x) MAKE_STACK_GETCOUNT_NAME(x)

bool GEN_GETCOUNT_NAME(STACK)
{
    if (this == NULL || outCount == NULL)
        return true;
    if (outCount != NULL)
        *outCount = this->size;
    return false;
}

#undef GEN_GETCOUNT_NAME
#undef MAKE_STACK_GETCOUNT_NAME

#define MAKE_STACK_REALLOCATE_NAME(x) x##_reallocate(STACK this, INDEX_TYPE newSize)
#define GEN_REALLOCATE_NAME(x) MAKE_STACK_REALLOCATE_NAME(x)

bool GEN_REALLOCATE_NAME(STACK)
{
    if (this == NULL)
        return true;
    if (newSize <= this->size)
        return true;
    STACK_TYPE* ptr = (STACK_TYPE*)malloc(sizeof(STACK_TYPE) * newSize);
    if (ptr == NULL)
        return true;
    INDEX_TYPE head = (this->tail + this->size) % this->capacity;
    if (this->tail > head)
    {
        INDEX_TYPE tailToTopSize = this->size - this->tail + 1;
        memcpy(&ptr[tailToTopSize], this->items, head * sizeof(STACK_TYPE));
        memcpy(ptr, &this->items[this->tail], tailToTopSize * sizeof(STACK_TYPE));
    }
    else
    {
        memcpy(ptr, &this->items[this->tail], (head - this->tail) * sizeof(STACK_TYPE));
    }
    free(this->items);
    this->items = ptr;
    this->tail = 0;
    this->capacity = newSize;
    return false;
}

#undef GEN_REALLOCATE_NAME
#undef MAKE_STACK_REALLOCATE_NAME

#define MAKE_STACK_PUSH_NAME(x, y, z) x##_push(x this, y item, z *outEnstackdCount)
#define GEN_PUSH_NAME(x, y, z) MAKE_STACK_PUSH_NAME(x, y, z)

bool GEN_PUSH_NAME(STACK, STACK_TYPE, INDEX_TYPE)
{
    #define MAKE_STACK_REALLOCATE_NAME(x) x##_reallocate
    #define GEN_REALLOCATE_NAME(x) MAKE_STACK_REALLOCATE_NAME(x)

    if (this == NULL)
        return true;
    if (this->capacity <= this->size + 1)
        if (GEN_REALLOCATE_NAME(STACK)(this, this->capacity * 2))
            return true;
    this->tail = (this->tail - 1) % this->capacity;
    this->items[this->tail] = item;
    this->size++;
    if (outEnstackdCount != NULL)
        *outEnstackdCount = 1;
    
    return false;

    #undef GEN_REALLOCATE_NAME
    #undef MAKE_STACK_REALLOCATE_NAME
}

#undef GEN_PUSH_NAME
#undef MAKE_STACK_PUSH_NAME

#define MAKE_STACK_PUSH_ITEMS_NAME(x, y, z) x##_push_items(x this, y *items, z items_index, z items_count, z *outEnstackdCount)
#define GEN_PUSH_ITEMS_NAME(x, y, z) MAKE_STACK_PUSH_ITEMS_NAME(x, y, z)

bool GEN_PUSH_ITEMS_NAME(STACK, STACK_TYPE, INDEX_TYPE)
{
    #define MAKE_STACK_REALLOCATE_NAME(x) x##_reallocate
    #define GEN_REALLOCATE_NAME(x) MAKE_STACK_REALLOCATE_NAME(x)
    
    if (this == NULL || this->items == NULL)
        return true;
    if (items_count <= 0)
        return false;
    // Reallocate items if reaching capacity
    INDEX_TYPE newSize = this->size + items_count;
    if (newSize > this->capacity)
    {
        INDEX_TYPE newCapacity = this->capacity * 2;
        while (newCapacity < newSize)
        {
            newCapacity *= 2;
        }
        if (GEN_REALLOCATE_NAME(STACK)(this, newCapacity))
            return true;
    }
    
    // Enstack the data!
    INDEX_TYPE index = items_index;
    for (INDEX_TYPE remainingCount = items_count; remainingCount > 0; remainingCount--)
    {
        this->tail = (this->tail - 1) % this->capacity;
        this->items[this->tail] = items[index++];
    }
    this->size = newSize;
    if (outEnstackdCount != NULL)
        *outEnstackdCount = items_count;
    
    return false;
    #undef GEN_REALLOCATE_NAME
    #undef MAKE_STACK_REALLOCATE_NAME
}

#undef GEN_PUSH_ITEMS_NAME
#undef MAKE_STACK_PUSH_ITEMS_NAME

#define MAKE_STACK_POP_NAME(x, y, z) x##_pop(x this, y *outItem, z* outPopCount)
#define GEN_POP_NAME(x, y, z) MAKE_STACK_POP_NAME(x, y, z)
bool GEN_POP_NAME(STACK, STACK_TYPE, INDEX_TYPE)
{
    #define MAKE_STACK_REALLOCATE_NAME(x) x##_reallocate
    #define GEN_REALLOCATE_NAME(x) MAKE_STACK_REALLOCATE_NAME(x)

    if (this == NULL || this->size <= 0)
        return true;
#ifdef ALLOW_SHRINKING_STACK
    if (this->capacity > (this->size - 1) * 4 && this->capacity / 2 >= DEFAULT_SIZE)
        if (GEN_REALLOCATE_NAME(STACK)(this, this->capacity / 2))
            return true;
#endif
    if (outItem != NULL)
        *outItem = this->items[this->tail];
    this->tail = (this->tail + 1) % this->capacity;
    this->size--;
    if (outPopCount != NULL)
        *outPopCount = 1;
    
    return false;

    #undef GEN_REALLOCATE_NAME
    #undef MAKE_STACK_REALLOCATE_NAME
}
#undef GEN_POP_NAME
#undef MAKE_STACK_POP_NAME

#define MAKE_STACK_POP_ITEMS_NAME(x, y, z) x##_pop_items(x this, z count, y *outItems, z *outPopCount)
#define GEN_POP_ITEMS_NAME(x, y, z) MAKE_STACK_POP_ITEMS_NAME(x, y, z)

bool GEN_POP_ITEMS_NAME(STACK, STACK_TYPE, INDEX_TYPE)
{
    if (this == NULL || this->items == NULL)
        return true;
    if (count <= 0)
    {
        if (outPopCount != NULL)
            *outPopCount = 0;
        return false;
    }
    INDEX_TYPE maxCount = count > this->size ? this->size : count;
    for (INDEX_TYPE i = 0; i < maxCount; ++i)
    {
        if (outItems != NULL)
            outItems[i] = this->items[this->tail];
        this->tail = (this->tail + 1) % this->capacity;
    }
    this->size -= maxCount;
    if (outPopCount != NULL)
        *outPopCount = maxCount;
    return false;
}

#undef GEN_POP_ITEMS_NAME
#undef MAKE_STACK_POP_ITEMS_NAME

#define MAKE_STACK_PEEK_NAME(x, y) x##_peek(x this, y *outItem)
#define GEN_PEEK_NAME(x, y) MAKE_STACK_PEEK_NAME(x, y)
bool GEN_PEEK_NAME(STACK, STACK_TYPE) {
    if (this == NULL || this->size <= 0)
        return true;
    
    if (outItem != NULL)
        *outItem = this->items[this->tail];
        
    return false;

    #undef GEN_REALLOCATE_NAME
    #undef MAKE_STACK_REALLOCATE_NAME
}
#undef GEN_PEEK_NAME
#undef MAKE_STACK_PEEK_NAME

#define MAKE_STACK_PEEK_ITEMS_NAME(x, y, z) x##_peek_items(x this, z count, y *outItems, z *outPopCount)
#define GEN_PEEK_ITEMS_NAME(x, y, z) MAKE_STACK_PEEK_ITEMS_NAME(x, y, z)

bool GEN_PEEK_ITEMS_NAME(STACK, STACK_TYPE, INDEX_TYPE)
{
    if (this == NULL || this->items == NULL)
        return true;
    if (count <= 0)
    {
        if (outPopCount != NULL)
            *outPopCount = 0;
        return false;
    }
    INDEX_TYPE maxCount = count > this->size ? this->size : count;
    INDEX_TYPE tail = this->tail;
    
    for (INDEX_TYPE i = 0; i < maxCount; ++i)
    {
        if (outItems != NULL)
            outItems[i] = this->items[tail];
        tail = (tail + 1) % this->capacity;
    }
    
    if (outPopCount != NULL)
        *outPopCount = maxCount;
    return false;
}

#ifndef NO_UNDEF
#undef GEN_PEEK_ITEMS_NAME
#undef MAKE_STACK_PEEK_ITEMS_NAME

#undef STL_FOR_C_IMPLEMENTATION_ONLY
#undef INDEX_TYPE
#undef STACK_TYPE
#undef STACK
#undef STACK_NAME
#undef MAKE_STACK_NAME
#undef DEFAULT_SIZE
#endif
