/*
 * Copyright (c) 2023 Tyler Crandall
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
    Bitmap Data Container for C
    - Used for representing sparse distribute of buffer allocation in concurrent_list
*/

#include <stdbool.h>
#include <stdint.h>
#include <stdatomic.h>
#ifndef BITMAP_TYPE
    #define BITMAP_TYPE int32_t
#endif

#ifndef INDEX_TYPE
#define INDEX_TYPE uint64_t
#endif

#ifndef BITMAP_MINIMUM_SIZE
#define BITMAP_MINIMUM_SIZE 8192
#endif

#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#define MAKE_BITMAP_NAME(x) stlc_bitmap_ ## x
#define BITMAP_NAME(x) MAKE_BITMAP_NAME(x)
#define BITMAP BITMAP_NAME(BITMAP_TYPE)

typedef struct {
    INDEX_TYPE size;
    BITMAP_TYPE *items;
    atomic_int_fast32_t write_mutex;
} BITMAP;

#define MAKE_BITMAP_NEW_NAME(x) x ## _new()
#define GEN_BITMAP_NEW_NAME(x) MAKE_BITMAP_NEW_NAME(x)
// stl_bitmap_x* stlc_bitmap_x_new()
BITMAP* GEN_BITMAP_NEW_NAME(BITMAP)
{
    BITMAP* map = (BITMAP*)malloc(sizeof(BITMAP));
    if (map == NULL)
    {
        return NULL;
    }
    map->items = (BITMAP_TYPE*)calloc(BITMAP_MINIMUM_SIZE, sizeof(int32_t));
    if (map->items == NULL)
    {
        free(map);
        return NULL;
    }
    map->size = BITMAP_MINIMUM_SIZE;
    map->write_mutex = 0;
    return map;
}
#undef GEN_BITMAP_NEW_NAME
#undef MAKE_BITMAP_NEW_NAME

#define MAKE_BITMAP_NEW_WITHCAPACITY_NAME(x,y) x ## _new_withcapacity(y initialCapacity)
#define GEN_BITMAP_NEW_WITHCAPACITY_NAME(x,y) MAKE_BITMAP_NEW_WITHCAPACITY_NAME(x,y)
// stl_bitmap_x* stlc_bitmap_x_new_withcapacity(INDEX_TYPE initialCapacity)
BITMAP* GEN_BITMAP_NEW_WITHCAPACITY_NAME(BITMAP, INDEX_TYPE)
{
    if (initialCapacity <= 0)
        return NULL;
    BITMAP* map = (BITMAP*)malloc(sizeof(BITMAP));
    if (map == NULL)
    {
        return NULL;
    }
    map->items = (BITMAP_TYPE*)calloc(initialCapacity, sizeof(int32_t));
    if (map->items == NULL)
    {
        free(map);
        return NULL;
    }
    map->size = BITMAP_MINIMUM_SIZE;
    map->write_mutex = 0;
    return map;
}
#undef GEN_BITMAP_NEW_WITHCAPACITY_NAME
#undef MAKE_BITMAP_NEW_WITHCAPACITY_NAME

#undef MAKE_BITMAP_NAME
#undef BITMAP_NAME
#undef BITMAP
#undef BITMAP_TYPE
