/*
 * Copyright (c) 2023 Tyler Crandall
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef HASH_TABLE_VALUE_TYPE
    #error HASH_TABLE_VALUE_TYPE need to be defined with a type specified (Ex. int, long, double, float, struct TestStruct, ...)
    #define HASH_TABLE_VALUE_TYPE int32_t
#endif

#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <stdint.h>
#ifndef INDEX_TYPE
    #define INDEX_TYPE uint64_t
#endif

#ifndef DEFAULT_TABLE_SIZE
    #define DEFAULT_TABLE_SIZE 32
#endif

#define FNV_OFFSET 14695981039346656037UL
#define FNV_PRIME 1099511628211UL

#define MAKE_HASH_TABLE_NAME(x) hash_table_ ## x
#define HASH_TABLE_NAME(x) MAKE_HASH_TABLE_NAME(x)
#define HASH_TABLE HASH_TABLE_NAME(HASH_TABLE_VALUE_TYPE)

#define MAKE_HASH_TABLE_ENTRY_NAME(x) hash_table_entry_ ## x
#define HASH_TABLE_ENTRY_NAME(x) MAKE_HASH_TABLE_ENTRY_NAME(x)
#define HASH_TABLE_ENTRY HASH_TABLE_ENTRY_NAME(HASH_TABLE_VALUE_TYPE)

typedef struct HASH_TABLE_ENTRY {
    char* key;
    HASH_TABLE_VALUE_TYPE value;
} *HASH_TABLE_ENTRY;

typedef struct HASH_TABLE {
    HASH_TABLE_ENTRY entries;
    INDEX_TYPE capacity;
    INDEX_TYPE length;
} *HASH_TABLE;

#define MAKE_HASH_TABLE_NEW_NAME(x) x ## _new()
#define GEN_HASH_TABLE_NEW_NAME(x) MAKE_HASH_TABLE_NEW_NAME(x)
HASH_TABLE GEN_HASH_TABLE_NEW_NAME(HASH_TABLE)
{
    // Allocate space for hash table struct.
    HASH_TABLE table = malloc(sizeof(struct HASH_TABLE));
    if (table == NULL) {
        return NULL;
    }
    table->length = 0;
    table->capacity = DEFAULT_TABLE_SIZE;

    // Allocate (zero'd) space for entry buckets.
    table->entries = calloc(table->capacity, sizeof(struct HASH_TABLE_ENTRY));
    if (table->entries == NULL) {
        free(table); // error, free table before we return!
        return NULL;
    }
    return table;
}

#define MAKE_HASH_TABLE_DESTROY_NAME(x) x ## _destroy(x this)
#define GEN_HASH_TABLE_DESTROY_NAME(x) MAKE_HASH_TABLE_DESTROY_NAME(x)
bool GEN_HASH_TABLE_DESTROY_NAME(HASH_TABLE)
{
    if (this == NULL) return true;
    // First free allocated keys.
    for (size_t i = 0; i < this->length; i++) {
        if (this->entries[i].key != NULL) {
            free(this->entries[i].key);
        }
    }

    // Then free entries array and table itself.
    free(this->entries);
    free(this);
    return false;
}

#define MAKE_HASH_TABLE_HASH_KEY_NAME(x) x ## _hash_key
#define GEN_HASH_TABLE_HASH_KEY_NAME(x) MAKE_HASH_TABLE_HASH_KEY_NAME(x)
static INDEX_TYPE GEN_HASH_TABLE_HASH_KEY_NAME(HASH_TABLE)(const char* key)
{
    INDEX_TYPE hash = FNV_OFFSET;
    for (const char* p = key; *p; p++) {
        hash ^= (INDEX_TYPE)(unsigned char)(*p);
        hash *= FNV_PRIME;
    }
    return hash;
}

#define MAKE_HASH_TABLE_GET_NAME(x) x ## _get(x this, const char* key, HASH_TABLE_VALUE_TYPE* outValue)
#define GEN_HASH_TABLE_GET_NAME(x) MAKE_HASH_TABLE_GET_NAME(x)
bool GEN_HASH_TABLE_GET_NAME(HASH_TABLE)
{
    if (this == NULL)
        return true;
    // AND hash with capacity-1 to ensure it's within entries array.
    INDEX_TYPE hash = GEN_HASH_TABLE_HASH_KEY_NAME(HASH_TABLE)(key);
    INDEX_TYPE index = (INDEX_TYPE)(hash & (INDEX_TYPE)(this->capacity - 1));

    // Loop till we find an empty entry.
    while (this->entries[index].key != NULL) {
        if (strcmp(key, this->entries[index].key) == 0) {
            // Found key, return value.
            if (outValue != NULL)
                *outValue = this->entries[index].value;
            return false;
        }
        // Key wasn't in this slot, move to next (linear probing).
        index++;
        if (index >= this->capacity) {
            // At end of entries array, wrap around.
            index = 0;
        }
    }
    return true;
}

#define MAKE_HASH_TABLE_REMOVE_NAME(x) x ## _remove(x this, const char* key)
#define GEN_HASH_TABLE_REMOVE_NAME(x) MAKE_HASH_TABLE_REMOVE_NAME(x)
bool GEN_HASH_TABLE_REMOVE_NAME(HASH_TABLE)
{
    if (this == NULL)
        return true;
    // AND hash with capacity-1 to ensure it's within entries array.
    INDEX_TYPE hash = GEN_HASH_TABLE_HASH_KEY_NAME(HASH_TABLE)(key);
    INDEX_TYPE index = (INDEX_TYPE)(hash & (INDEX_TYPE)(this->capacity - 1));

    // Loop till we find an empty entry.
    while (this->entries[index].key != NULL) {
        if (strcmp(key, this->entries[index].key) == 0) {
            free(this->entries[index].key);
            INDEX_TYPE remainder = this->length - index;
            if (remainder > 0)
                memmove(&this->entries[index + 1], &this->entries[index], remainder);
            this->length--;
            return false;
        }
        // Key wasn't in this slot, move to next (linear probing).
        index++;
        if (index >= this->capacity) {
            // At end of entries array, wrap around.
            index = 0;
        }
    }
    return true;
}

#define MAKE_HASH_TABLE_SET_ENTRY_NAME(x) x ## _set_entry
#define GEN_HASH_TABLE_SET_ENTRY_NAME(x) MAKE_HASH_TABLE_SET_ENTRY_NAME(x)
// Internal function to set an entry (without expanding table).
static const char* GEN_HASH_TABLE_SET_ENTRY_NAME(HASH_TABLE)(HASH_TABLE_ENTRY entries, INDEX_TYPE capacity,
        const char* key, HASH_TABLE_VALUE_TYPE value, INDEX_TYPE* plength) {
    // AND hash with capacity-1 to ensure it's within entries array.
    INDEX_TYPE hash = GEN_HASH_TABLE_HASH_KEY_NAME(HASH_TABLE)(key);
    INDEX_TYPE index = (INDEX_TYPE)(hash & (INDEX_TYPE)(capacity - 1));

    // Loop till we find an empty entry.
    while (entries[index].key != NULL) {
        if (strcmp(key, entries[index].key) == 0) {
            // Found key (it already exists), update value.
            entries[index].value = value;
            return entries[index].key;
        }
        // Key wasn't in this slot, move to next (linear probing).
        index++;
        if (index >= capacity) {
            // At end of entries array, wrap around.
            index = 0;
        }
    }

    // Didn't find key, allocate+copy if needed, then insert it.
    char* realKey = (char*)key;
    if (plength != NULL) {
        size_t len = strlen(key);
        if (len <= 0) {
            return NULL;
        }
        realKey = calloc(len + 1, 1);
        memcpy(realKey, key, len);
        (*plength)++;
    }
    entries[index].key = (char*)realKey;
    entries[index].value = value;
    return realKey;
}

#define MAKE_HASH_TABLE_EXPAND_NAME(x) x ## _expand
#define GEN_HASH_TABLE_EXPAND_NAME(x) MAKE_HASH_TABLE_EXPAND_NAME(x)
// Expand hash table to twice its current size. Return true on success,
// false if out of memory.
static bool GEN_HASH_TABLE_EXPAND_NAME(HASH_TABLE)(HASH_TABLE this) {
    if (this == NULL)
        return false;
    // Allocate new entries array.
    INDEX_TYPE new_capacity = this->capacity * 2;
    if (new_capacity < this->capacity) {
        return false;  // overflow (capacity would be too big)
    }
    HASH_TABLE_ENTRY new_entries = calloc(new_capacity, sizeof(struct HASH_TABLE_ENTRY));
    if (new_entries == NULL) {
        return false;
    }

    // Iterate entries, move all non-empty ones to new table's entries.
    for (INDEX_TYPE i = 0; i < this->capacity; i++) {
        struct HASH_TABLE_ENTRY entry = this->entries[i];
        if (entry.key != NULL) {
            GEN_HASH_TABLE_SET_ENTRY_NAME(HASH_TABLE)(new_entries, new_capacity, entry.key,
                         entry.value, NULL);
        }
    }

    // Free old entries array and update this table's details.
    free(this->entries);
    this->entries = new_entries;
    this->capacity = new_capacity;
    return true;
}

#define MAKE_HASH_TABLE_SET_NAME(x) x ## _set(x this, const char* key, HASH_TABLE_VALUE_TYPE value, const char** outRealKey)
#define GEN_HASH_TABLE_SET_NAME(x) MAKE_HASH_TABLE_SET_NAME(x)
bool GEN_HASH_TABLE_SET_NAME(HASH_TABLE)
{
    if (this == NULL)
        return true;
    // If length will exceed half of current capacity, expand it.
    if (this->length >= this->capacity / 2) {
        if (!GEN_HASH_TABLE_EXPAND_NAME(HASH_TABLE)(this)) {
            return true;
        }
    }

    // Set entry and update length.
    const char* realKey = GEN_HASH_TABLE_SET_ENTRY_NAME(HASH_TABLE)(this->entries, this->capacity, key, value, &this->length);
    if (outRealKey != NULL)
        *outRealKey = realKey;
    return realKey == NULL;
}

#define MAKE_HASH_TABLE_COUNT_NAME(x) x ## _count(x this, INDEX_TYPE* outCount)
#define GEN_HASH_TABLE_COUNT_NAME(x) MAKE_HASH_TABLE_COUNT_NAME(x)
bool GEN_HASH_TABLE_COUNT_NAME(HASH_TABLE)
{
    if (this == NULL)
        return true;
    if (outCount != NULL)
        *outCount = this->length;
    return false;
}

#define MAKE_HASH_TABLE_ITERATOR_NAME(x) hash_table_iterator_ ## x
#define HASH_TABLE_ITERATOR_NAME(x) MAKE_HASH_TABLE_ITERATOR_NAME(x)
#define HASH_TABLE_ITERATOR HASH_TABLE_ITERATOR_NAME(HASH_TABLE_VALUE_TYPE)

typedef struct HASH_TABLE_ITERATOR {
    const char* key;
    HASH_TABLE_VALUE_TYPE value;
    HASH_TABLE _table;
    INDEX_TYPE _index;
} *HASH_TABLE_ITERATOR;

#define MAKE_HASH_TABLE_GETITERATOR_NAME(x) x ## _getiterator(x this, HASH_TABLE_ITERATOR* outIterator)
#define GEN_HASH_TABLE_GETITERATOR_NAME(x) MAKE_HASH_TABLE_GETITERATOR_NAME(x)
bool GEN_HASH_TABLE_GETITERATOR_NAME(HASH_TABLE)
{
    if (this == NULL)
        return true;
    HASH_TABLE_ITERATOR iterator;
    iterator->_table = this;
    iterator->_index = 0;
    if (outIterator != NULL)
        *outIterator = iterator;
    return false;
}

#define MAKE_HASH_TABLE_ITERATOR_NEXT_NAME(x, y) x ## _iterator_next(y iterator)
#define GEN_HASH_TABLE_ITERATOR_NEXT_NAME(x, y) MAKE_HASH_TABLE_ITERATOR_NEXT_NAME(x, y)
bool GEN_HASH_TABLE_ITERATOR_NEXT_NAME(HASH_TABLE, HASH_TABLE_ITERATOR)
{
    if (iterator == NULL)
        return true;
    // Loop till we've hit end of entries array.
    HASH_TABLE table = iterator->_table;
    while (iterator->_index < table->capacity) {
        size_t i = iterator->_index;
        iterator->_index++;
        if (table->entries[i].key != NULL) {
            // Found next non-empty item, update iterator key and value.
            struct HASH_TABLE_ENTRY entry = table->entries[i];
            iterator->key = entry.key;
            iterator->value = entry.value;
            return false;
        }
    }
    return true;
}

#undef GEN_HASH_TABLE_ITERATOR_NEXT_NAME
#undef MAKE_HASH_TABLE_ITERATOR_NEXT_NAME
#undef GEN_HASH_TABLE_GETITERATOR_NAME
#undef MAKE_HASH_TABLE_GETITERATOR_NAME
#undef HASH_TABLE_ITERATOR
#undef HASH_TABLE_ITERATOR_NAME
#undef MAKE_HASH_TABLE_ITERATOR_NAME
#undef GEN_HASH_TABLE_GETCOUNT_NAME
#undef MAKE_HASH_TABLE_GETCOUNT_NAME
#undef GEN_HASH_TABLE_SET_NAME
#undef MAKE_HASH_TABLE_SET_NAME
#undef GEN_HASH_TABLE_GET_NAME
#undef MAKE_HASH_TABLE_GET_NAME
#undef GEN_HASH_TABLE_DESTROY_NAME
#undef MAKE_HASH_TABLE_DESTROY_NAME
#undef GEN_HASH_TABLE_NEW_NAME
#undef MAKE_HASH_TABLE_NEW_NAME
#undef HASH_TABLE
#undef HASH_TABLE_NAME
#undef MAKE_HASH_TABLE_NAME
#undef HASH_TABLE_VALUE_TYPE
#undef MAKE_HASH_TABLE_SET_ENTRY_NAME
#undef GEN_HASH_TABLE_SET_ENTRY_NAME
#undef MAKE_HASH_TABLE_EXPAND_NAME
#undef GEN_HASH_TABLE_EXPAND_NAME
#undef MAKE_HASH_TABLE_HASH_KEY_NAME
#undef GEN_HASH_TABLE_HASH_KEY_NAME
#undef MAKE_HASH_TABLE_REMOVE_NAME
#undef GEN_HASH_TABLE_REMOVE_NAME
