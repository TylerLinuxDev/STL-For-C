/*
 * Copyright (c) 2023 Tyler Crandall
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdatomic.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#ifndef CONCURRENT_QUEUE_TYPE
#error There must be a provided type for CONCURRENT_QUEUE_TYPE and it must be defined prior to using this code!
#define CONCURRENT_QUEUE_TYPE int32_t // To make VSCode happy
#endif

#ifndef ALLOW_SHRINKING_QUEUE
#define ALLOW_SHRINKING_QUEUE 1
#endif

#ifndef CONCURRENT_QUEUE_MINIMUM_SIZE
#define CONCURRENT_QUEUE_MINIMUM_SIZE 16
#endif

#ifndef CONCURRENT_QUEUE_MAXIMUM_CAPACITY
#define CONCURRENT_QUEUE_MAXIMUM_CAPACITY 128 // This should be set to the highest available CPU threads on the market.
                                              // By default, this is set to 128 threads limit for future proofing.
                                              // The trade off for this is that it'll require allocation of 128 pointers which
                                              // can adds up to 1 Kilobyte size allocation for each Concurrent Queue by default.
#endif

#if CONCURRENT_QUEUE_MAXIMUM_CAPACITY < CONCURRENT_QUEUE_MINIMUM_SIZE
#define CONCURRENT_QUEUE_MAXIMUM_CAPACITY CONCURRENT_QUEUE_MINIMUM_SIZE
#endif

#ifndef INDEX_TYPE
#define INDEX_TYPE uint64_t
#endif

#ifndef STL_FOR_C_IMPLEMENTATION_ONLY
#define STL_FOR_C_IMPLEMENTATION_ONLY
#endif

#define DEFAULT_EXCHANGE_MEMORY_ORDER memory_order_relaxed
#define DEFAULT_LOAD_MEMORY_ORDER memory_order_relaxed
#define DEFAULT_FETCH_ADD_MEMORY_ORDER memory_order_relaxed
#define DEFAULT_FETCH_SUB_MEMORY_ORDER memory_order_relaxed

#define SYMBOLIZE(x) x
#define GEN_SYMBOLIZE(x) SYMBOLIZE(x)
#define QUEUE_TYPE GEN_SYMBOLIZE(CONCURRENT_QUEUE_TYPE)
#define PREFIX_NAME internal_for_concurrent_queue_
#define NO_UNDEF 1
#include "src/queue_impl.c"

// CONCURRENT QUEUE SECTION

#define MAKE_CONCURRENT_QUEUE_NAME(x) concurrent_queue_##x
#define CONCURRENT_QUEUE_NAME(x) MAKE_CONCURRENT_QUEUE_NAME(x)
#define CONCURRENT_QUEUE CONCURRENT_QUEUE_NAME(CONCURRENT_QUEUE_TYPE)

#define MAKE_QUEUE_NODE_NAME(x) concurrent_queue_node_##x
#define QUEUE_NODE_NAME(x) MAKE_QUEUE_NODE_NAME(x)
#define QUEUE_NODE QUEUE_NODE_NAME(CONCURRENT_QUEUE_TYPE)

struct CONCURRENT_QUEUE {
    _Atomic QUEUE* queues;
    _Atomic INDEX_TYPE queues_count;
    _Atomic INDEX_TYPE sortIndex;
    _Atomic INDEX_TYPE items_count;
};
typedef  struct CONCURRENT_QUEUE* CONCURRENT_QUEUE;

#define MAKE_CONCURRENT_QUEUE_NEW_NAME(x) x##_new()
#define GEN_CONCURRENT_QUEUE_NEW_NAME(x) MAKE_CONCURRENT_QUEUE_NEW_NAME(x)

CONCURRENT_QUEUE
GEN_CONCURRENT_QUEUE_NEW_NAME(CONCURRENT_QUEUE)
{
#define MAKE_QUEUE_NEW_USE(x) x##_new
#define GEN_QUEUE_NEW_USE(x) MAKE_QUEUE_NEW_USE(x)

    CONCURRENT_QUEUE newQueue = (CONCURRENT_QUEUE)malloc(sizeof(struct CONCURRENT_QUEUE));
    newQueue->sortIndex = 0;
    newQueue->queues_count = CONCURRENT_QUEUE_MINIMUM_SIZE;
    newQueue->queues = (_Atomic QUEUE*)malloc(sizeof(QUEUE) * CONCURRENT_QUEUE_MINIMUM_SIZE);
    newQueue->items_count = 0;
    for (INDEX_TYPE i = 0; i < CONCURRENT_QUEUE_MINIMUM_SIZE; ++i) {
        newQueue->queues[i] = GEN_QUEUE_NEW_USE(QUEUE)();
    }
    return newQueue;

#undef GEN_QUEUE_NEW_USE
#undef MAKE_QUEUE_NEW_USE
}

#undef GEN_CONCURRENT_QUEUE_NEW_NAME
#undef MAKE_CONCURRENT_QUEUE_NEW_NAME

#define MAKE_CONCURRENT_QUEUE_DESTROY_NAME(x) x##_destroy(x this)
#define GEN_CONCURRENT_QUEUE_DESTROY_NAME(x) \
    MAKE_CONCURRENT_QUEUE_DESTROY_NAME(x)

bool GEN_CONCURRENT_QUEUE_DESTROY_NAME(CONCURRENT_QUEUE)
{
#define MAKE_QUEUE_DESTROY_USE(x) x##_destroy
#define GEN_QUEUE_DESTROY_USE(x) MAKE_QUEUE_DESTROY_USE(x)

    if (this == NULL)
        return true;

    if (this->queues_count > 0)
        for (size_t i = 0; i < this->queues_count; ++i)
        {
            GEN_QUEUE_DESTROY_USE(QUEUE)(this->queues[i]);
        }
    free((void*)this->queues);
    free((void*)this);
    return false;

#undef GEN_QUEUE_DESTROY_USE
#undef MAKE_QUEUE_DESTROY_USE
}

#undef GEN_CONCURRENT_QUEUE_DESTROY_NAME
#undef MAKE_CONCURRENT_QUEUE_DESTROY_NAME

#define MAKE_CONCURRENT_QUEUE_ENQUEUE_LEASE_NAME(x) x##_enqueue_lease(x this, INDEX_TYPE* outIndex)
#define GEN_CONCURRENT_QUEUE_ENQUEUE_LEASE_NAME(x) MAKE_CONCURRENT_QUEUE_ENQUEUE_LEASE_NAME(x)

QUEUE GEN_CONCURRENT_QUEUE_ENQUEUE_LEASE_NAME(CONCURRENT_QUEUE)
{
    if (this == NULL)
        return NULL;

    INDEX_TYPE count = atomic_load(&this->queues_count);
    for (int retry = 0; retry <3; ++retry)
    {
        for (INDEX_TYPE i = 0; i < count; ++i)
        {
            _Atomic QUEUE activeQueue = atomic_exchange(&this->queues[i], NULL);
            if (activeQueue != NULL)
            {
                *outIndex = i;
                return activeQueue;
            }
        }
    }
    return NULL;
}

#undef GEN_CONCURRENT_QUEUE_ENQUEUE_LEASE_NAME
#undef MAKE_CONCURRENT_QUEUE_ENQUEUE_LEASE_NAME

#define MAKE_CONCURRENT_QUEUE_DEQUEUE_LEASE_NAME(x) x##_dequeue_lease(x this, INDEX_TYPE* outIndex)
#define GEN_CONCURRENT_QUEUE_DEQUEUE_LEASE_NAME(x) MAKE_CONCURRENT_QUEUE_DEQUEUE_LEASE_NAME(x)

QUEUE GEN_CONCURRENT_QUEUE_DEQUEUE_LEASE_NAME(CONCURRENT_QUEUE)
{
    if (this == NULL)
        return NULL;

    INDEX_TYPE count = atomic_load(&this->queues_count);
    for (int retry = 0; retry <3; ++retry)
    {
        for (size_t i = count - 1; i < count; --i)
        {
            _Atomic QUEUE activeQueue = atomic_exchange(&this->queues[i], NULL);
            if (activeQueue != NULL)
            {
                *outIndex = i;
                return activeQueue;
            }
        }
    }
    return NULL;
}

#undef GEN_CONCURRENT_QUEUE_DEQUEUE_LEASE_NAME
#undef MAKE_CONCURRENT_QUEUE_DEQUEUE_LEASE_NAME

#define MAKE_CONCURRENT_QUEUE_RETURN_NAME(x, y) x##_return(x this, y queue, INDEX_TYPE queueSlot)
#define GEN_CONCURRENT_QUEUE_RETURN_NAME(x, y) MAKE_CONCURRENT_QUEUE_RETURN_NAME(x, y)

bool GEN_CONCURRENT_QUEUE_RETURN_NAME(CONCURRENT_QUEUE, QUEUE)
{
    if (this == NULL)
        return true;

    int retry;
    for (retry = 0; retry < 3; ++retry)
    {
        INDEX_TYPE count = atomic_load(&this->queues_count);
        INDEX_TYPE slot = atomic_fetch_add(&this->sortIndex, 1) % count;
        QUEUE swap = NULL;
        swap = atomic_exchange(&this->queues[slot], swap);
        if (swap == NULL)
            continue;
        if (slot > queueSlot) // slot = dequeue slot, queueSlot = enqueue slot
        {
            if (queue->size > swap->size)
            {
                INDEX_TYPE temp = queueSlot;
                queueSlot = slot;
                slot = temp;
            }
        }
        else // slot = enqueue slot, queueSlot = dequeue slot
        {
            if (swap->size >queue->size)
            {
                INDEX_TYPE temp = queueSlot;
                queueSlot = slot;
                slot = temp;
            }
        }
        atomic_store(&this->queues[slot], swap);
        atomic_store(&this->queues[queueSlot], queue);
        break;
    }
    if (retry >= 3)
        atomic_store(&this->queues[queueSlot], queue);
    return false;
}

#undef GEN_CONCURRENT_QUEUE_RETURN_NAME
#undef MAKE_CONCURRENT_QUEUE_RETURN_NAME

#define MAKE_CONCURRENT_QUEUE_ENQUEUE_NAME(x, y, z) x##_enqueue(x this, y value, z* outEnqueuedCount)
#define GEN_CONCURRENT_QUEUE_ENQUEUE_NAME(x, y, z) MAKE_CONCURRENT_QUEUE_ENQUEUE_NAME(x, y, z)

bool GEN_CONCURRENT_QUEUE_ENQUEUE_NAME(CONCURRENT_QUEUE, CONCURRENT_QUEUE_TYPE, INDEX_TYPE)
{
#define MAKE_QUEUE_ENQUEUE_USE(x) x##_enqueue
#define GEN_QUEUE_ENQUEUE_USE(x) MAKE_QUEUE_ENQUEUE_USE(x)
#define MAKE_CONCURRENT_QUEUE_ENQUEUE_LEASE_USE(x) x##_enqueue_lease
#define GEN_CONCURRENT_QUEUE_ENQUEUE_LEASE_USE(x) MAKE_CONCURRENT_QUEUE_ENQUEUE_LEASE_USE(x)
#define MAKE_CONCURRENT_QUEUE_RETURN_USE(x) x##_return
#define GEN_CONCURRENT_QUEUE_RETURN_USE(x) MAKE_CONCURRENT_QUEUE_RETURN_USE(x)
    if (this == NULL)
        return true;
    INDEX_TYPE index;
    QUEUE activeQueue =GEN_CONCURRENT_QUEUE_ENQUEUE_LEASE_USE(CONCURRENT_QUEUE)(this, &index);
    if (activeQueue == NULL) return true;

    bool res = GEN_QUEUE_ENQUEUE_USE(QUEUE)(activeQueue, value, outEnqueuedCount);
    if (!res)
        atomic_fetch_add(&this->items_count, *outEnqueuedCount);

    GEN_CONCURRENT_QUEUE_RETURN_USE(CONCURRENT_QUEUE)(this, activeQueue, index);
    return res;

#undef GEN_CONCURRENT_QUEUE_RETURN_USE
#undef MAKE_CONCURRENT_QUEUE_RETURN_USE
#undef GEN_CONCURRENT_QUEUE_ENQUEUE_LEASE_USE
#undef MAKE_CONCURRENT_QUEUE_ENQUEUE_LEASE_USE
#undef GEN_QUEUE_ENQUEUE_USE
#undef MAKE_QUEUE_ENQUEUE_USE
}

#undef GEN_CONCURRENT_QUEUE_ENQUEUE_NAME
#undef MAKE_CONCURRENT_QUEUE_ENQUEUE_NAME

#define MAKE_CONCURRENT_QUEUE_ENQUEUE_ITEMS_NAME(x, y, z) x##_enqueue_items(x this, y *items, z items_index, z items_count, z *outEnqueuedCount)
#define GEN_CONCURRENT_QUEUE_ENQUEUE_ITEMS_NAME(x, y, z) MAKE_CONCURRENT_QUEUE_ENQUEUE_ITEMS_NAME(x, y, z)

bool GEN_CONCURRENT_QUEUE_ENQUEUE_ITEMS_NAME(CONCURRENT_QUEUE, CONCURRENT_QUEUE_TYPE, INDEX_TYPE)
{
#define MAKE_QUEUE_ENQUEUE_ITEMS_USE(x) x##_enqueue_items
#define GEN_QUEUE_ENQUEUE_ITEMS_USE(x) MAKE_QUEUE_ENQUEUE_ITEMS_USE(x)
#define MAKE_CONCURRENT_QUEUE_ENQUEUE_LEASE_USE(x) x##_enqueue_lease
#define GEN_CONCURRENT_QUEUE_ENQUEUE_LEASE_USE(x) MAKE_CONCURRENT_QUEUE_ENQUEUE_LEASE_USE(x)
#define MAKE_CONCURRENT_QUEUE_RETURN_USE(x) x##_return
#define GEN_CONCURRENT_QUEUE_RETURN_USE(x) MAKE_CONCURRENT_QUEUE_RETURN_USE(x)

    if (this == NULL)
        return true;

    INDEX_TYPE index;
    QUEUE activeQueue =GEN_CONCURRENT_QUEUE_ENQUEUE_LEASE_USE(CONCURRENT_QUEUE)(this, &index);
    if (activeQueue == NULL) return true;

    bool res = GEN_QUEUE_ENQUEUE_ITEMS_USE(QUEUE)(activeQueue, items, items_index, items_count, outEnqueuedCount);
    if (!res)
        atomic_fetch_add(&this->items_count, *outEnqueuedCount);

    GEN_CONCURRENT_QUEUE_RETURN_USE(CONCURRENT_QUEUE)(this, activeQueue, index);
    return res;

#undef GEN_CONCURRENT_QUEUE_RETURN_USE
#undef MAKE_CONCURRENT_QUEUE_RETURN_USE
#undef GEN_CONCURRENT_QUEUE_ENQUEUE_LEASE_USE
#undef MAKE_CONCURRENT_QUEUE_ENQUEUE_LEASE_USE
#undef GEN_QUEUE_ENQUEUE_USE
#undef MAKE_QUEUE_ENQUEUE_USE
}

#undef GEN_CONCURRENT_QUEUE_ENQUEUE_NAME
#undef MAKE_CONCURRENT_QUEUE_ENQUEUE_NAME

#define MAKE_CONCURRENT_QUEUE_DEQUEUE_NAME(x, y, z) x##_dequeue(x this, y* outItem, z* outDequeueCount)
#define GEN_CONCURRENT_QUEUE_DEQUEUE_NAME(x, y, z) MAKE_CONCURRENT_QUEUE_DEQUEUE_NAME(x, y, z)

bool GEN_CONCURRENT_QUEUE_DEQUEUE_NAME(CONCURRENT_QUEUE, CONCURRENT_QUEUE_TYPE, INDEX_TYPE)
{
#define MAKE_QUEUE_DEQUEUE_USE(x) x##_dequeue
#define GEN_QUEUE_DEQUEUE_USE(x) MAKE_QUEUE_DEQUEUE_USE(x)
#define MAKE_CONCURRENT_QUEUE_DEQUEUE_LEASE_USE(x) x##_dequeue_lease
#define GEN_CONCURRENT_QUEUE_DEQUEUE_LEASE_USE(x) MAKE_CONCURRENT_QUEUE_DEQUEUE_LEASE_USE(x)
#define MAKE_CONCURRENT_QUEUE_RETURN_USE(x) x##_return
#define GEN_CONCURRENT_QUEUE_RETURN_USE(x) MAKE_CONCURRENT_QUEUE_RETURN_USE(x)

    if (this == NULL)
        return true;

    INDEX_TYPE index;
    QUEUE activeQueue =GEN_CONCURRENT_QUEUE_DEQUEUE_LEASE_USE(CONCURRENT_QUEUE)(this, &index);
    if (activeQueue == NULL) return true;

    bool res = GEN_QUEUE_DEQUEUE_USE(QUEUE)(activeQueue, outItem, outDequeueCount);
    if (!res)
        atomic_fetch_sub(&this->items_count, *outDequeueCount);

    GEN_CONCURRENT_QUEUE_RETURN_USE(CONCURRENT_QUEUE)(this, activeQueue, index);
    return res;

#undef GEN_CONCURRENT_QUEUE_RETURN_USE
#undef MAKE_CONCURRENT_QUEUE_RETURN_USE
#undef GEN_CONCURRENT_QUEUE_DEQUEUE_LEASE_USE
#undef MAKE_CONCURRENT_QUEUE_DEQUEUE_LEASE_USE
#undef GEN_QUEUE_DEQUEUE_USE
#undef MAKE_QUEUE_DEQUEUE_USE
}

#undef GEN_CONCURRENT_QUEUE_DEQUEUE_NAME
#undef MAKE_CONCURRENT_QUEUE_DEQUEUE_NAME

#define MAKE_CONCURRENT_QUEUE_DEQUEUE_ITEMS_NAME(x, y, z) x##_dequeue_items(x this, z count, y *outItems, z *outDequeueCount)
#define GEN_CONCURRENT_QUEUE_DEQUEUE_ITEMS_NAME(x, y, z) MAKE_CONCURRENT_QUEUE_DEQUEUE_ITEMS_NAME(x, y, z)

bool GEN_CONCURRENT_QUEUE_DEQUEUE_ITEMS_NAME(CONCURRENT_QUEUE, CONCURRENT_QUEUE_TYPE, INDEX_TYPE)
{
#define MAKE_QUEUE_DEQUEUE_ITEMS_USE(x) x##_dequeue_items
#define GEN_QUEUE_DEQUEUE_ITEMS_USE(x) MAKE_QUEUE_DEQUEUE_ITEMS_USE(x)
#define MAKE_CONCURRENT_QUEUE_DEQUEUE_LEASE_USE(x) x##_dequeue_lease
#define GEN_CONCURRENT_QUEUE_DEQUEUE_LEASE_USE(x) MAKE_CONCURRENT_QUEUE_DEQUEUE_LEASE_USE(x)
#define MAKE_CONCURRENT_QUEUE_RETURN_USE(x) x##_return
#define GEN_CONCURRENT_QUEUE_RETURN_USE(x) MAKE_CONCURRENT_QUEUE_RETURN_USE(x)

    if (this == NULL)
        return true;

    INDEX_TYPE index;
    QUEUE activeQueue =GEN_CONCURRENT_QUEUE_DEQUEUE_LEASE_USE(CONCURRENT_QUEUE)(this, &index);
    if (activeQueue == NULL) return true;

    bool res = GEN_QUEUE_DEQUEUE_ITEMS_USE(QUEUE)(activeQueue, count, outItems, outDequeueCount);
    if (!res)
        atomic_fetch_sub(&this->items_count, *outDequeueCount);

    GEN_CONCURRENT_QUEUE_RETURN_USE(CONCURRENT_QUEUE)(this, activeQueue, index);
    return res;

#undef GEN_CONCURRENT_QUEUE_RETURN_USE
#undef MAKE_CONCURRENT_QUEUE_RETURN_USE
#undef GEN_CONCURRENT_QUEUE_DEQUEUE_LEASE_USE
#undef MAKE_CONCURRENT_QUEUE_DEQUEUE_LEASE_USE
#undef GEN_QUEUE_DEQUEUE_ITEMS_USE
#undef MAKE_QUEUE_DEQUEUE_ITEMS_USE
}

#undef GEN_CONCURRENT_QUEUE_DEQUEUE_ITEMS_NAME
#undef MAKE_CONCURRENT_QUEUE_DEQUEUE_ITEMS_NAME

#define MAKE_CONCURRENT_QUEUE_GETCOUNT_NAME(x) x##_getcount(x this, INDEX_TYPE *outCount)
#define GEN_CONCURRENT_QUEUE_GETCOUNT_NAME(x) MAKE_CONCURRENT_QUEUE_GETCOUNT_NAME(x)

bool GEN_CONCURRENT_QUEUE_GETCOUNT_NAME(CONCURRENT_QUEUE)
{
    if (this == NULL || outCount == NULL)
        return true;

    *outCount = atomic_load(&this->items_count);
    return false;
}

#ifndef NO_UNDEF
#undef GEN_CONCURRENT_QUEUE_GETCOUNT_NAME
#undef MAKE_CONCURRENT_QUEUE_GETCOUNT_NAME

#undef CONCURRENT_QUEUE
#undef CONCURRENT_QUEUE_NAME
#undef MAKE_CONCURRENT_QUEUE_NAME
#undef STL_FOR_C_IMPLEMENTATION_ONLY
#undef INDEX_TYPE
#undef CONCURRENT_QUEUE_TYPE
#undef QUEUE
#undef QUEUE_NAME
#undef MAKE_QUEUE_NAME
#undef MINIMUM_QUEUE_SIZE
#endif
