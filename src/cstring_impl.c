#include "include/cstring.h"
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdint.h>
#include <immintrin.h>

// Short string optimization
#define DEFAULT_STACK_SIZE 16

struct cstring {
    uint64_t length;
    uint64_t capacity;
    union {
        char stackdata[DEFAULT_STACK_SIZE + 1];
        char* data;
    };
};

// If you're wondering why xmemmem instead of memmem,
// you can blame GLibC for excluding the memmem from C17 standard and making it a GNU Extension instead.
// While I could use _GNU_SOURCE define, but this project is not going to make an exception for GLibC.
// Reddit Post: https://old.reddit.com/r/C_Programming/comments/ud0n0o/confused_about_how_to_use_memmem/
// GNU Documentation for this: https://www.gnu.org/software/libc/manual/html_node/Search-Functions.html
// Source of xmemmem: https://github.com/skeeto/scratch/blob/master/misc/rabin-karp.c#L59
// License of xmemmem code: Rabin-Karp (rolling hash) string search - Public Domain
static void* xmemmem(const void *haystack, uint64_t hlen, const void *needle, uint64_t nlen)
{
    if (hlen < nlen) {
        return 0;
    }

    // Compute pattern hash
    uint64_t match = 0;
    const unsigned char *n = needle;
    for (uint64_t i = 0; i < nlen; i++) {
        match = match*257 + n[i];
    }

    // Compute first rolling hash
    uint64_t hash = 0;
    const unsigned char *h = haystack;
    const unsigned char *end = h + hlen;
    for (uint64_t i = 0; i < nlen-1; i++, h++) {
        hash = hash*257 + *h;
    }

    // High polynomial coefficient, exponentiation by squaring
    uint64_t f = 1;
    uint64_t x = 257;
    for (uint64_t i = nlen - 1; i; i >>= 1) {
        f *= i & 1 ? x : 1;
        x *= x;
    }

    // Run rolling hash over the haystack
    for (; h < end; h++) {
        hash = hash*257 + *h;
        if (hash == match && !memcmp(h-nlen+1, needle, nlen)) {
            return (char *)h + 1 - nlen;
        }
        hash -= f * (h[1-(int64_t)nlen] & 0xff);
    }
    return 0;
}

void cstring_default_errorhandler(const char* msg)
{
    perror(msg);
}

static void (*cstring_errorhandler_func)(const char* msg);

static void cstring_dispatch_errorhandler(const char* msg)
{
    if (cstring_errorhandler_func == NULL)
        cstring_default_errorhandler(msg);
    else
        cstring_errorhandler_func(msg);
}

bool cstring_validate(cstring string)
{
    if (string.capacity < DEFAULT_STACK_SIZE)
        return true;
    return false;
}
cstring cstring_new()
{
    return (cstring) {
        .capacity = DEFAULT_STACK_SIZE,
        .length = 0,
    };
}

cstring cstring_new2(uint64_t initialCapacity)
{
    if (initialCapacity <= DEFAULT_STACK_SIZE)
        return (cstring) {
            .capacity = DEFAULT_STACK_SIZE,
            .length = 0
        };
    char* heapstring = (char*)calloc(sizeof(char), initialCapacity);
    if (heapstring == NULL)
    {
        char errorMsg[512];
        sprintf(errorMsg, "Failed to allocate on heap for cstring, calloc returns null when attempting to allocate a capacity of: %lu", initialCapacity);
        cstring_dispatch_errorhandler(errorMsg);
        return (cstring){0};
    }
    return (cstring){
        .capacity = initialCapacity,
        .length = 0,
        .data = heapstring
    };
}

cstring cstring_new3(const char* string)
{
    uint64_t stringLen = strlen(string);
    if (stringLen <= DEFAULT_STACK_SIZE)
    {
        cstring val = (cstring){
            .capacity = DEFAULT_STACK_SIZE,
            .length = stringLen
        };
        memcpy(val.stackdata, string, stringLen);
        return val;
    }
    char* newString = (char*)malloc(sizeof(char) * (stringLen + 1));
    if (newString == NULL)
    {
        char errorMsg[512];
        sprintf(errorMsg, "Failed to allocate on heap for cstring, calloc returns null when attempting to allocate a capacity of: %lu", stringLen);
        cstring_dispatch_errorhandler(errorMsg);
        return (cstring){0};
    }
    memcpy(newString, string, stringLen);
    newString[stringLen] = '\0';
    return (cstring) {
        .data = newString,
        .length = stringLen,
        .capacity = stringLen + 1
    };
}

bool cstring_destroy(cstring str)
{
    if (cstring_validate(str))
        return false;
    if (str.capacity > DEFAULT_STACK_SIZE && str.data != NULL)
        free(str.data);
    return false;
}

bool cstring_reallocate(cstring* thisRef, uint64_t newCapacity)
{
    if (thisRef == NULL || cstring_validate(*thisRef))
        return true;
    if (newCapacity < thisRef->length)
        return true;
    if (newCapacity <= DEFAULT_STACK_SIZE)
        return false; // Do nothing

    if (thisRef->capacity <= DEFAULT_STACK_SIZE)
    {
        char* data = (char*)malloc(sizeof(char) *newCapacity);
        strncpy(data, thisRef->stackdata, thisRef->length);
        data[thisRef->length] = '\0';
        thisRef->data = data;
        thisRef->capacity = newCapacity;
        return false;
    }

    char* reallocPtr = realloc(thisRef->data, newCapacity);
    if (reallocPtr == NULL)
        return true;
    thisRef->data = reallocPtr;
    thisRef->capacity = newCapacity;
    return false;
}

bool cstring_append(cstring* thisRef, const char* string)
{
    if (thisRef == NULL || cstring_validate(*thisRef))
        return true;
    uint64_t stringLen = strlen(string);
    if (stringLen <= 0)
        return false;
    if (thisRef->capacity <= DEFAULT_STACK_SIZE && thisRef->capacity > stringLen + thisRef->length)
    {
        strcpy(&thisRef->stackdata[thisRef->length], string);
        thisRef->length += stringLen;
        return false;
    }

    if (stringLen + thisRef->length >thisRef->capacity)
    {
        if (cstring_reallocate(thisRef, (stringLen + thisRef->length) * 2))
            return true;
    }
    strcpy(&thisRef->data[thisRef->length], string);
    thisRef->length += stringLen;
    return false;
}

bool cstring_appendnewline(cstring* thisRef)
{
    if (thisRef == NULL || cstring_validate(*thisRef))
        return true;
    const char* newline = "\n";
    uint64_t stringLen = strlen(newline);
    if (stringLen <= 0)
        return false;
    if (thisRef->capacity <= DEFAULT_STACK_SIZE && thisRef->capacity > stringLen + thisRef->length)
    {
        strcpy(&thisRef->stackdata[thisRef->length], newline);
        thisRef->length += stringLen;
        return false;
    }

    if (stringLen + thisRef->length >thisRef->capacity)
    {
        if (cstring_reallocate(thisRef, (stringLen + thisRef->length) * 2))
            return true;
    }
    strcpy(&thisRef->data[thisRef->length], newline);
    thisRef->length += stringLen;
    return false;
}

bool cstring_appendline(cstring* thisRef, const char* line)
{
    if (thisRef == NULL || cstring_validate(*thisRef))
        return true;
    const char* newline = "\n";
    uint64_t lineLen = strlen(line);
    uint64_t stringLen = strlen(newline) + lineLen;
    if (stringLen <= 0)
        return false;
    if (thisRef->capacity <= DEFAULT_STACK_SIZE && thisRef->capacity > stringLen + thisRef->length)
    {
        strcpy(&thisRef->stackdata[thisRef->length], line);
        strcpy(&thisRef->stackdata[thisRef->length + lineLen], newline);
        thisRef->length += stringLen;
        return false;
    }

    if (stringLen + thisRef->length >thisRef->capacity)
    {
        if (cstring_reallocate(thisRef, (stringLen + thisRef->length) * 2))
            return true;
    }
    strcpy(&thisRef->data[thisRef->length], line);
    strcpy(&thisRef->data[thisRef->length + lineLen], newline);
    thisRef->length += stringLen;
    return false;
}

bool cstring_indexof(cstring this, char* stringToFind, uint64_t offset, uint64_t length, uint64_t* outIndex)
{
    if (cstring_validate(this))
    {
        fprintf(stdout, "Validate fail\n");
        return true;
    }
    if (offset >= this.length || offset < 0 || length < 0 || outIndex == NULL)
    {
        fprintf(stdout, "Conditional check fail\n");
        return true;
    }
    uint64_t realLen = length;
    if (this.length - offset < realLen)
        realLen = this.length - offset;
    uint64_t stringToFindLen = strlen(stringToFind);
    if (stringToFindLen <= 0)
    {
        fprintf(stdout, "String to find len is 0!");
        return true;
    }
    void* index;
    if (this.capacity > DEFAULT_STACK_SIZE)
    {
        index = xmemmem(&this.data[offset], realLen, stringToFind, stringToFindLen);
        *outIndex = (uint64_t) index - (uint64_t) this.data;
    }
    else
    {
        index = xmemmem(&this.stackdata[offset], realLen, stringToFind, stringToFindLen);
        *outIndex = (uint64_t) index - (uint64_t) this.stackdata;
    }

    return false;
}

bool cstring_getcharacter(cstring this, uint64_t offset, char* outChar)
{
    if (offset > this.length)
        return true;
    if (this.capacity <= DEFAULT_STACK_SIZE)
        *outChar = this.stackdata[offset];
    else
        *outChar = this.data[offset];
    return false;
}

void cstring_print(cstring this)
{
    if (this.capacity <= DEFAULT_STACK_SIZE)
        printf("%s\n", this.stackdata);
    else
        printf("%s\n", this.data);
}

uint64_t cstring_getlength(cstring this)
{
    return this.length;
}

uint64_t cstring_getcapacity(cstring this)
{
    return this.capacity;
}

char* cstring_getcstr(cstring this)
{
    char* data = (char*)malloc(this.length + 1);
    if (this.capacity <= DEFAULT_STACK_SIZE)
        memcpy(data, this.stackdata, this.length);
    else
        memcpy(data, this.data, this.length);
    data[this.length] = '\0';
    return data;
}

int32_t cstring_compare(cstring l, cstring r)
{
    if (l.length != r.length)
        return
    if (l.capacity <= DEFAULT_STACK_SIZE && r.capacity <= DEFAULT_STACK_SIZE)
        return (int32_t) strncmp(l.stackdata, r.stackdata);
}
