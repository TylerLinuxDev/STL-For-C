/*
 * Copyright (c) 2023 Tyler Crandall
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef CONCURRENT_STACK_TYPE
#error There must be a provided type for CONCURRENT_STACK_TYPE and it must be defined prior to using this code!
#define CONCURRENT_STACK_TYPE int32_t // To make VSCode happy
#endif

#ifndef ALLOW_SHRINKING_STACK
#define ALLOW_SHRINKING_STACK 1
#endif

#ifndef CONCURRENT_STACK_MINIMUM_SIZE
#define CONCURRENT_STACK_MINIMUM_SIZE 16
#endif

#ifndef CONCURRENT_STACK_MAXIMUM_CAPACITY
#define CONCURRENT_STACK_MAXIMUM_CAPACITY 128 // This should be set to the highest available CPU threads on the market.
                                              // By default, this is set to 128 threads limit for future proofing.
                                              // The trade off for this is that it'll require allocation of 128 pointers which
                                              // can adds up to 1 Kilobyte size allocation for each Concurrent Stack by default.
#endif

#if CONCURRENT_STACK_MAXIMUM_CAPACITY < CONCURRENT_STACK_MINIMUM_SIZE
#define CONCURRENT_STACK_MAXIMUM_CAPACITY CONCURRENT_STACK_MINIMUM_SIZE
#endif

#ifndef INDEX_TYPE
#define INDEX_TYPE uint64_t
#endif

#ifndef STL_FOR_C_IMPLEMENTATION_ONLY
#define STL_FOR_C_IMPLEMENTATION_ONLY
#endif

#define DEFAULT_EXCHANGE_MEMORY_ORDER memory_order_relaxed
#define DEFAULT_LOAD_MEMORY_ORDER memory_order_relaxed
#define DEFAULT_FETCH_ADD_MEMORY_ORDER memory_order_relaxed
#define DEFAULT_FETCH_SUB_MEMORY_ORDER memory_order_relaxed

#include <stdatomic.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#define SYMBOLIZE(x) x
#define STACK_TYPE SYMBOLIZE(CONCURRENT_STACK_TYPE)
#define PREFIX_NAME internal_for_concurrent_stack_
#define NO_UNDEF 1
#include "src/stack_impl.c"

// CONCURRENT STACK SECTION

#define MAKE_CONCURRENT_STACK_NAME(x) concurrent_stack_##x
#define CONCURRENT_STACK_NAME(x) MAKE_CONCURRENT_STACK_NAME(x)
#define CONCURRENT_STACK CONCURRENT_STACK_NAME(CONCURRENT_STACK_TYPE)

#define MAKE_STACK_NODE_NAME(x) concurrent_stack_node_##x
#define STACK_NODE_NAME(x) MAKE_STACK_NODE_NAME(x)
#define STACK_NODE STACK_NODE_NAME(CONCURRENT_STACK_TYPE)

typedef struct STACK_NODE {
    volatile STACK stack;
    volatile INDEX_TYPE lastStackSize;
} STACK_NODE;

typedef struct CONCURRENT_STACK {
    volatile STACK_NODE* stacks;
    volatile INDEX_TYPE size;
    volatile INDEX_TYPE failPopCount;
} *CONCURRENT_STACK;

#define MAKE_CONCURRENT_STACK_NEW_NAME(x) x##_new()
#define GEN_CONCURRENT_STACK_NEW_NAME(x) MAKE_CONCURRENT_STACK_NEW_NAME(x)

CONCURRENT_STACK
GEN_CONCURRENT_STACK_NEW_NAME(CONCURRENT_STACK)
{
#define MAKE_STACK_NEW_USE(x) x##_new
#define GEN_STACK_NEW_USE(x) MAKE_STACK_NEW_USE(x)

    CONCURRENT_STACK newStack = (CONCURRENT_STACK)malloc(sizeof(struct CONCURRENT_STACK));
    newStack->stacks = (STACK_NODE*)calloc(sizeof(STACK_NODE), CONCURRENT_STACK_MAXIMUM_CAPACITY);
    newStack->size = CONCURRENT_STACK_MINIMUM_SIZE;
    newStack->failPopCount = 0;
#if CONCURRENT_STACK_MINIMUM_SIZE > 0
    for (INDEX_TYPE i = 0; i < CONCURRENT_STACK_MINIMUM_SIZE; ++i) {
        STACK_NODE* node = (STACK_NODE*)&newStack->stacks[i];
        node->stack = GEN_STACK_NEW_USE(STACK)();
        node->lastStackSize = 0;
    }
#endif
    return newStack;

#undef GEN_STACK_NEW_USE
#undef MAKE_STACK_NEW_USE
}

#undef GEN_CONCURRENT_STACK_NEW_NAME
#undef MAKE_CONCURRENT_STACK_NEW_NAME

#define MAKE_CONCURRENT_STACK_DESTROY_NAME(x) x##_destroy(x this)
#define GEN_CONCURRENT_STACK_DESTROY_NAME(x) \
    MAKE_CONCURRENT_STACK_DESTROY_NAME(x)

bool GEN_CONCURRENT_STACK_DESTROY_NAME(CONCURRENT_STACK)
{
#define MAKE_STACK_DESTROY_USE(x) x##_destroy
#define GEN_STACK_DESTROY_USE(x) MAKE_STACK_DESTROY_USE(x)

    if (this == NULL)
        return true;
    
    for (INDEX_TYPE i = 0; i < this->size; ++i)
    {
        STACK_NODE* node = (STACK_NODE*)&this->stacks[i];
        GEN_STACK_DESTROY_USE(STACK)(node->stack);
    }
    free((void*)this->stacks);
    free(this);
    return false;

#undef GEN_STACK_DESTROY_USE
#undef MAKE_STACK_DESTROY_USE
}

#undef GEN_CONCURRENT_STACK_DESTROY_NAME
#undef MAKE_CONCURRENT_STACK_DESTROY_NAME

#define MAKE_CONCURRENT_STACK_PUSH_NAME(x, y, z) x##_push(x this, y value, z* outPushCount)
#define GEN_CONCURRENT_STACK_PUSH_NAME(x, y, z) MAKE_CONCURRENT_STACK_PUSH_NAME(x, y, z)

bool GEN_CONCURRENT_STACK_PUSH_NAME(CONCURRENT_STACK, CONCURRENT_STACK_TYPE, INDEX_TYPE)
{
#define MAKE_STACK_PUSH_USE(x) x##_push
#define GEN_STACK_PUSH_USE(x) MAKE_STACK_PUSH_USE(x)
#define MAKE_STACK_NEW_USE(x) x##_new
#define GEN_STACK_NEW_USE(x) MAKE_STACK_NEW_USE(x)
#define MAKE_STACK_DESTROY_USE(x) x##_destroy
#define GEN_STACK_DESTROY_USE(x) MAKE_STACK_DESTROY_USE(x)
    
    if (this == NULL)
        return true;
    
    INDEX_TYPE size = atomic_load_explicit((_Atomic(INDEX_TYPE)*)&this->size, DEFAULT_LOAD_MEMORY_ORDER);
    for (INDEX_TYPE i = 0; i < size; ++i)
    {
        STACK_NODE* node = (STACK_NODE*) &this->stacks[i];
        STACK* stack = atomic_exchange_explicit((_Atomic(STACK*)*)&node->stack, NULL, DEFAULT_EXCHANGE_MEMORY_ORDER);
        
        if (stack == NULL) {
            continue;
        }
        
        INDEX_TYPE actualPushCount = 0;
        if (GEN_STACK_PUSH_USE(STACK)((STACK)stack, value, &actualPushCount)) {
            atomic_store((_Atomic(STACK*)*)&node->stack, stack);
            continue;
        }
        atomic_fetch_add((_Atomic(INDEX_TYPE)*)&node->lastStackSize, actualPushCount);
        if (outPushCount != NULL)
            *outPushCount = actualPushCount;
        atomic_store((_Atomic(STACK*)*)&node->stack, stack);
        return false;
    }
    STACK newStack = GEN_STACK_NEW_USE(STACK)();
    if (newStack == NULL)
        return true;
    
    INDEX_TYPE currentIdx = atomic_fetch_add_explicit((_Atomic(INDEX_TYPE)*)&this->size, 1, DEFAULT_FETCH_ADD_MEMORY_ORDER);
    if (currentIdx >= CONCURRENT_STACK_MAXIMUM_CAPACITY)
    {
        atomic_store((_Atomic(INDEX_TYPE)*)&this->size, CONCURRENT_STACK_MAXIMUM_CAPACITY); // Let's not cause an integer overflow...
        return true;
    }
    STACK_NODE* newNode = (STACK_NODE*) &this->stacks[currentIdx];
    newNode->stack = newStack;
    INDEX_TYPE actualPushCount = 0;
    if (GEN_STACK_PUSH_USE(STACK)((STACK)newNode->stack, value, &actualPushCount))
    {
        return true; // We tried...
    }
    atomic_fetch_add((_Atomic(INDEX_TYPE)*)&newNode->lastStackSize, actualPushCount);
    if (outPushCount != NULL)
        *outPushCount = actualPushCount;
    return false;

#undef GEN_STACK_PUSH_USE
#undef MAKE_STACK_PUSH_USE
#undef GEN_STACK_NEW_USE
#undef MAKE_STACK_NEW_USE
#undef GEN_STACK_DESTROY_USE
#undef MAKE_STACK_DESTROY_USE
}

#undef GEN_CONCURRENT_STACK_PUSH_NAME
#undef MAKE_CONCURRENT_STACK_PUSH_NAME

#define MAKE_CONCURRENT_STACK_PUSH_ITEMS_NAME(x, y, z) x##_push_items(x this, y *items, z items_index, z items_count, z *outPushCount)
#define GEN_CONCURRENT_STACK_PUSH_ITEMS_NAME(x, y, z) MAKE_CONCURRENT_STACK_PUSH_ITEMS_NAME(x, y, z)

bool GEN_CONCURRENT_STACK_PUSH_ITEMS_NAME(CONCURRENT_STACK, CONCURRENT_STACK_TYPE, INDEX_TYPE)
{
#define MAKE_STACK_PUSH_ITEMS_USE(x) x##_push_items
#define GEN_STACK_PUSH_ITEMS_USE(x) MAKE_STACK_PUSH_ITEMS_USE(x)
#define MAKE_STACK_NEW_USE(x) x##_new
#define GEN_STACK_NEW_USE(x) MAKE_STACK_NEW_USE(x)
#define MAKE_STACK_DESTROY_USE(x) x##_destroy
#define GEN_STACK_DESTROY_USE(x) MAKE_STACK_DESTROY_USE(x)
    
    if (this == NULL)
        return true;
    INDEX_TYPE size = atomic_load_explicit((_Atomic(INDEX_TYPE)*)&this->size, DEFAULT_LOAD_MEMORY_ORDER);
    for (INDEX_TYPE i = 0; i < size; ++i)
    {
        STACK_NODE* node = (STACK_NODE*) &this->stacks[i];
        STACK* stack = atomic_exchange_explicit((_Atomic(STACK*)*)&node->stack, NULL, DEFAULT_EXCHANGE_MEMORY_ORDER);
        
        if (stack == NULL) {
            continue;
        }
        
        INDEX_TYPE actualPushCount = 0;
        if (GEN_STACK_PUSH_ITEMS_USE(STACK)((STACK)stack, items, items_index, items_count, &actualPushCount)) {
            atomic_store((_Atomic(STACK*)*)&node->stack, stack);
            continue;
        }
        atomic_fetch_add((_Atomic(INDEX_TYPE)*)&node->lastStackSize, actualPushCount);
        if (outPushCount != NULL)
            *outPushCount = actualPushCount;
        atomic_store((_Atomic(STACK*)*)&node->stack, stack);
        return false;
    }
    STACK newStack = GEN_STACK_NEW_USE(STACK)();
    if (newStack == NULL)
        return true;
    
    INDEX_TYPE currentIdx = atomic_fetch_add_explicit((_Atomic(INDEX_TYPE)*)&this->size, 1, DEFAULT_FETCH_ADD_MEMORY_ORDER);
    if (currentIdx >= CONCURRENT_STACK_MAXIMUM_CAPACITY)
    {
        atomic_store((_Atomic(INDEX_TYPE)*)&this->size, CONCURRENT_STACK_MAXIMUM_CAPACITY); // Let's not cause an integer overflow...
        return true;
    }
    STACK_NODE* newNode = (STACK_NODE*) &this->stacks[currentIdx];
    newNode->stack = newStack;
    INDEX_TYPE actualPushCount = 0;
    if (GEN_STACK_PUSH_ITEMS_USE(STACK)((STACK)newNode->stack, items, items_index, items_count, &actualPushCount))
    {
        return true; // We tried...
    }
    atomic_fetch_add((_Atomic(INDEX_TYPE)*)&newNode->lastStackSize, actualPushCount);
    if (outPushCount != NULL)
        *outPushCount = actualPushCount;
    return false;

#undef GEN_STACK_PUSH_USE
#undef MAKE_STACK_PUSH_USE
#undef GEN_STACK_NEW_USE
#undef MAKE_STACK_NEW_USE
#undef GEN_STACK_DESTROY_USE
#undef MAKE_STACK_DESTROY_USE
}

#undef GEN_CONCURRENT_STACK_PUSH_NAME
#undef MAKE_CONCURRENT_STACK_PUSH_NAME

#define MAKE_CONCURRENT_STACK_POP_NAME(x, y, z) x##_pop(x this, y* outItem, z* outPopCount)
#define GEN_CONCURRENT_STACK_POP_NAME(x, y, z) MAKE_CONCURRENT_STACK_POP_NAME(x, y, z)

bool GEN_CONCURRENT_STACK_POP_NAME(CONCURRENT_STACK, CONCURRENT_STACK_TYPE, INDEX_TYPE)
{
#define MAKE_STACK_POP_USE(x) x##_pop
#define GEN_STACK_POP_USE(x) MAKE_STACK_POP_USE(x)

    if (this == NULL)
        return true;
    INDEX_TYPE size = atomic_load_explicit((_Atomic(INDEX_TYPE)*)&this->size, DEFAULT_LOAD_MEMORY_ORDER);
    for (INDEX_TYPE i = 0; i < size; ++i)
    {
        INDEX_TYPE lastCount = atomic_load_explicit((_Atomic(INDEX_TYPE)*)&this->stacks[i].lastStackSize, DEFAULT_LOAD_MEMORY_ORDER);
        if (lastCount <= 0)
            continue;
        STACK_NODE* node = (STACK_NODE*) &this->stacks[i];
        STACK stack = atomic_exchange_explicit((_Atomic(STACK)*)&node->stack, NULL, DEFAULT_EXCHANGE_MEMORY_ORDER);
        if (stack == NULL) {
            continue;
        }
        INDEX_TYPE actualPopCount = 0;
        if (GEN_STACK_POP_USE(STACK)(stack, outItem, &actualPopCount)) {
            atomic_store((_Atomic(STACK)*)&node->stack, stack);
            continue;
        }
        atomic_fetch_sub((_Atomic(INDEX_TYPE)*)&node->lastStackSize, 1);
        if (outPopCount != NULL)
            *outPopCount = actualPopCount;
        atomic_store((_Atomic(STACK)*)&node->stack, stack);
        return false;
    }
    atomic_fetch_add_explicit((_Atomic(INDEX_TYPE)*)&this->failPopCount, 1, DEFAULT_FETCH_ADD_MEMORY_ORDER);
    return true;

#undef GEN_STACK_POP_USE
#undef MAKE_STACK_POP_USE
}

#undef GEN_CONCURRENT_STACK_POP_NAME
#undef MAKE_CONCURRENT_STACK_POP_NAME

#define MAKE_CONCURRENT_STACK_POP_ITEMS_NAME(x, y, z) x##_pop_items(x this, z count, y *outItems, z *outPopCount)
#define GEN_CONCURRENT_STACK_POP_ITEMS_NAME(x, y, z) MAKE_CONCURRENT_STACK_POP_ITEMS_NAME(x, y, z)

bool GEN_CONCURRENT_STACK_POP_ITEMS_NAME(CONCURRENT_STACK, CONCURRENT_STACK_TYPE, INDEX_TYPE)
{
#define MAKE_STACK_POP_ITEMS_USE(x) x##_pop_items
#define GEN_STACK_POP_ITEMS_USE(x) MAKE_STACK_POP_ITEMS_USE(x)

    if (this == NULL)
        return true;
    INDEX_TYPE size = atomic_load_explicit((_Atomic(INDEX_TYPE)*)&this->size, DEFAULT_LOAD_MEMORY_ORDER);
    for (INDEX_TYPE i = 0; i < size; ++i)
    {
        STACK_NODE* node = (STACK_NODE*) &this->stacks[i];
        STACK stack = atomic_exchange_explicit((_Atomic(STACK)*)&node->stack, NULL, DEFAULT_EXCHANGE_MEMORY_ORDER);
        if (stack == NULL) {
            continue;
        }
        INDEX_TYPE actualPopCount = 0;
        if (GEN_STACK_POP_ITEMS_USE(STACK)(stack, count, outItems, &actualPopCount)) {
            atomic_store((_Atomic(STACK)*)&node->stack, stack);
            continue;
        }
        atomic_fetch_sub((_Atomic(INDEX_TYPE)*)&node->lastStackSize, actualPopCount);
        if (outPopCount != NULL)
            *outPopCount = actualPopCount;
        atomic_store((_Atomic(STACK)*)&node->stack, stack);
        return false;
    }
    return true;

#undef GEN_STACK_POP_ITEMS_USE
#undef MAKE_STACK_POP_ITEMS_USE
}

#undef GEN_CONCURRENT_STACK_POP_ITEMS_NAME
#undef MAKE_CONCURRENT_STACK_POP_ITEMS_NAME

#define MAKE_CONCURRENT_STACK_GETCOUNT_NAME(x) x##_getcount(x this, INDEX_TYPE *outCount)
#define GEN_CONCURRENT_STACK_GETCOUNT_NAME(x) MAKE_CONCURRENT_STACK_GETCOUNT_NAME(x)

bool GEN_CONCURRENT_STACK_GETCOUNT_NAME(CONCURRENT_STACK)
{
    if (this == NULL || outCount == NULL)
        return true;
    INDEX_TYPE totalCount = 0;
    INDEX_TYPE size = atomic_load_explicit((_Atomic(INDEX_TYPE)*)&this->size, DEFAULT_LOAD_MEMORY_ORDER);
    for (INDEX_TYPE i = 0; i < size; ++i)
    {
        STACK_NODE* node = atomic_load_explicit((_Atomic(STACK_NODE*)*) &this->stacks[i], DEFAULT_EXCHANGE_MEMORY_ORDER);
        totalCount += atomic_load_explicit((_Atomic(INDEX_TYPE)*)&node->lastStackSize, DEFAULT_LOAD_MEMORY_ORDER);
    }
    *outCount = totalCount;
    return false;
}

#ifndef NO_UNDEF
#undef GEN_CONCURRENT_STACK_GETCOUNT_NAME
#undef MAKE_CONCURRENT_STACK_GETCOUNT_NAME

#undef CONCURRENT_STACK
#undef CONCURRENT_STACK_NAME
#undef MAKE_CONCURRENT_STACK_NAME
#undef STL_FOR_C_IMPLEMENTATION_ONLY
#undef INDEX_TYPE
#undef CONCURRENT_STACK_TYPE
#undef STACK
#undef STACK_NAME
#undef MAKE_STACK_NAME
#undef MINIMUM_STACK_SIZE
#endif
