/*
 * Copyright (c) 2023 Tyler Crandall
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
    List Type reimplemented in C
*/

#include <stdbool.h>
#include <stdint.h>
#include "float.h"
#ifndef SINGLE_LINKED_LIST_TYPE
    #error There must be a provided type for SINGLE_LINKED_LIST_TYPE and it must be defined prior to using this header!
    #define SINGLE_LINKED_LIST_TYPE int32_t
#endif

#ifndef INDEX_TYPE
#define INDEX_TYPE uint64_t
#endif

#include <stdlib.h>
#include <string.h>
#include <stdatomic.h>

#define MAKE_SINGLE_LINKED_LIST_ITEM_NAME(x) single_linked_list_item_ ## x
#define SINGLE_LINKED_LIST_ITEM_NAME(x) MAKE_SINGLE_LINKED_LIST_ITEM_NAME(x)
#define SINGLE_LINKED_LIST_ITEM SINGLE_LINKED_LIST_ITEM_NAME(SINGLE_LINKED_LIST_TYPE)

typedef struct SINGLE_LINKED_LIST_ITEM {
    SINGLE_LINKED_LIST_TYPE item;
    intptr_t next;
} *SINGLE_LINKED_LIST_ITEM;

#define MAKE_SINGLE_LINKED_LIST_NAME(x) single_linked_list_ ## x
#define SINGLE_LINKED_LIST_NAME(x) MAKE_SINGLE_LINKED_LIST_NAME(x)
#define SINGLE_LINKED_LIST SINGLE_LINKED_LIST_NAME(SINGLE_LINKED_LIST_TYPE)

typedef struct SINGLE_LINKED_LIST{
    INDEX_TYPE size;
    SINGLE_LINKED_LIST_ITEM head;
    _Atomic int32_t lock;
} *SINGLE_LINKED_LIST;

#define MAKE_SINGLE_LINKED_LIST_LOCK_USE(x) x ## _lock
#define GEN_SINGLE_LINKED_LIST_LOCK_USE(x) MAKE_SINGLE_LINKED_LIST_LOCK_USE(x)
#define MAKE_SINGLE_LINKED_LIST_UNLOCK_USE(x) x ## _unlock
#define GEN_SINGLE_LINKED_LIST_UNLOCK_USE(x) MAKE_SINGLE_LINKED_LIST_UNLOCK_USE(x)

#define MAKE_SINGLE_LINKED_LIST_NEW_NAME(x) x ## _new()
#define GEN_SINGLE_LINKED_LIST_NEW_NAME(x) MAKE_SINGLE_LINKED_LIST_NEW_NAME(x)
SINGLE_LINKED_LIST GEN_SINGLE_LINKED_LIST_NEW_NAME(SINGLE_LINKED_LIST)
{
    return (SINGLE_LINKED_LIST)calloc(1, sizeof(struct SINGLE_LINKED_LIST));
}
#undef GEN_SINGLE_LINKED_LIST_NEW_NAME
#undef MAKE_SINGLE_LINKED_LIST_NEW_NAME

#define MAKE_SINGLE_LINKED_LIST_DESTROY_NAME(x) x ## _destroy(x this)
#define GEN_SINGLE_LINKED_LIST_DESTROY_NAME(x) MAKE_SINGLE_LINKED_LIST_DESTROY_NAME(x)
bool GEN_SINGLE_LINKED_LIST_DESTROY_NAME(SINGLE_LINKED_LIST)
{
    for (SINGLE_LINKED_LIST_ITEM p = (void*) this->head; !p->next;)
    {
        void* next = (void*)p->next;
        free(p);
        p = (SINGLE_LINKED_LIST_ITEM) next;
    }
    free(this);
    return false;
}
#undef GEN_SINGLE_LINKED_LIST_DESTROY_NAME
#undef MAKE_SINGLE_LINKED_LIST_DESTROY_NAME

#define MAKE_SINGLE_LINKED_LIST_LOCK_NAME(x) x ## _lock(x this)
#define GEN_SINGLE_LINKED_LIST_LOCK_NAME(x) MAKE_SINGLE_LINKED_LIST_LOCK_NAME(x)
void GEN_SINGLE_LINKED_LIST_LOCK_NAME(SINGLE_LINKED_LIST)
{
    int32_t expected = 0;
    while (!atomic_compare_exchange_strong(&this->lock, &expected, 1))
    {
        expected = 0;
        atomic_signal_fence(memory_order_acq_rel);
    }
}
#undef GEN_SINGLE_LINKED_LIST_LOCK_NAME
#undef MAKE_SINGLE_LINKED_LIST_LOCK_NAME

#define MAKE_SINGLE_LINKED_LIST_UNLOCK_NAME(x) x ## _unlock(x this)
#define GEN_SINGLE_LINKED_LIST_UNLOCK_NAME(x) MAKE_SINGLE_LINKED_LIST_UNLOCK_NAME(x)
void GEN_SINGLE_LINKED_LIST_UNLOCK_NAME(SINGLE_LINKED_LIST)
{
    atomic_exchange(&this->lock, 0);
}
#undef GEN_SINGLE_LINKED_LIST_UNLOCK_NAME
#undef MAKE_SINGLE_LINKED_LIST_UNLOCK_NAME

#define MAKE_SINGLE_LINKED_LIST_GET_NAME(x) x ## _get(x this, INDEX_TYPE index, SINGLE_LINKED_LIST_TYPE* outResult)
#define GEN_SINGLE_LINKED_LIST_GET_NAME(x) MAKE_SINGLE_LINKED_LIST_GET_NAME(x)
bool GEN_SINGLE_LINKED_LIST_GET_NAME(SINGLE_LINKED_LIST)
{    
    if (this == NULL) return true;
    GEN_SINGLE_LINKED_LIST_LOCK_USE(SINGLE_LINKED_LIST)(this);
    SINGLE_LINKED_LIST_ITEM  current = this->head;
    for (INDEX_TYPE i = 0; i < index && current != NULL; ++i)
    {
        current = (SINGLE_LINKED_LIST_ITEM) current->next;
    }
    if (current != NULL)
    {
        *outResult = current->item;
        GEN_SINGLE_LINKED_LIST_UNLOCK_USE(SINGLE_LINKED_LIST)(this);
        return false;
    }
    GEN_SINGLE_LINKED_LIST_UNLOCK_USE(SINGLE_LINKED_LIST)(this);
    return true;
}
#undef GEN_SINGLE_LINKED_LIST_GET_NAME
#undef MAKE_SINGLE_LINKED_LIST_GET_NAME

#define MAKE_SINGLE_LINKED_LIST_GET_FIRST_NAME(x) x ## _get_first(x this, SINGLE_LINKED_LIST_TYPE* outResult)
#define GEN_SINGLE_LINKED_LIST_GET_FIRST_NAME(x) MAKE_SINGLE_LINKED_LIST_GET_FIRST_NAME(x)
bool GEN_SINGLE_LINKED_LIST_GET_FIRST_NAME(SINGLE_LINKED_LIST)
{
    if (this == NULL) return true;
    GEN_SINGLE_LINKED_LIST_LOCK_USE(SINGLE_LINKED_LIST)(this);
    if (this->head != NULL)
        *outResult = this->head->item;
    else
    {
        GEN_SINGLE_LINKED_LIST_UNLOCK_USE(SINGLE_LINKED_LIST)(this);
        return true;
    }
    GEN_SINGLE_LINKED_LIST_UNLOCK_USE(SINGLE_LINKED_LIST)(this);
    return false;
}
#undef GEN_SINGLE_LINKED_LIST_GET_FIRST_NAME
#undef MAKE_SINGLE_LINKED_LIST_GET_FIRST_NAME

#define MAKE_SINGLE_LINKED_LIST_GET_LAST_NAME(x) x ## _get_last(x this, SINGLE_LINKED_LIST_TYPE* outResult)
#define GEN_SINGLE_LINKED_LIST_GET_LAST_NAME(x) MAKE_SINGLE_LINKED_LIST_GET_LAST_NAME(x)
bool GEN_SINGLE_LINKED_LIST_GET_LAST_NAME(SINGLE_LINKED_LIST)
{
    if (this == NULL) return true;
    GEN_SINGLE_LINKED_LIST_LOCK_USE(SINGLE_LINKED_LIST)(this);
    if (this->tail != NULL)
        *outResult = this->tail->item;
    else
    {
        GEN_SINGLE_LINKED_LIST_UNLOCK_USE(SINGLE_LINKED_LIST)(this);
        return true;
    }
    GEN_SINGLE_LINKED_LIST_UNLOCK_USE(SINGLE_LINKED_LIST)(this);
    return false;
}
#undef GEN_SINGLE_LINKED_LIST_GET_FIRST_NAME
#undef MAKE_SINGLE_LINKED_LIST_GET_FIRST_NAME

#define MAKE_SINGLE_LINKED_LIST_GETRANGE_NAME(x) x ## _getrange(x* this, INDEX_TYPE index, INDEX_TYPE length, SINGLE_LINKED_LIST_TYPE* outResult, INDEX_TYPE* outLength)
#define GEN_SINGLE_LINKED_LIST_GETRANGE_NAME(x) MAKE_SINGLE_LINKED_LIST_GETRANGE_NAME(x)
bool GEN_SINGLE_LINKED_LIST_GETRANGE_NAME(SINGLE_LINKED_LIST)
{
    if (this == NULL || outResult == NULL) return true;
    GEN_SINGLE_LINKED_LIST_LOCK_USE(SINGLE_LINKED_LIST)(this);
    
    if (index < 0 || index >= this->size || length < 0)
    {
        GEN_SINGLE_LINKED_LIST_UNLOCK_USE(SINGLE_LINKED_LIST)(this);
        return true;
    }
    
    SINGLE_LINKED_LIST_ITEM* current = this->head;
    for (INDEX_TYPE i = 0; i < index && current != NULL; ++i)
    {
        current = (SINGLE_LINKED_LIST_ITEM*) current->next;
    }
    
    INDEX_TYPE remainder = this->size - index;
    INDEX_TYPE writtenLength = 0;
    for (INDEX_TYPE i = 0; i < remainder && current != NULL; ++i)
    {
        outResult[i] = current->item;
        ++writtenLength;
        current = (SINGLE_LINKED_LIST_ITEM*) current->next;
    }
    
    *outLength = writtenLength;
    GEN_SINGLE_LINKED_LIST_UNLOCK_USE(SINGLE_LINKED_LIST)(this);
    return false;
}
#undef GEN_SINGLE_LINKED_LIST_GETRANGE_NAME
#undef MAKE_SINGLE_LINKED_LIST_GETRANGE_NAME

#define MAKE_SINGLE_LINKED_LIST_ADD_NAME(x) x ## _add(x* this, SINGLE_LINKED_LIST_TYPE item)
#define GEN_SINGLE_LINKED_LIST_ADD_NAME(x) MAKE_SINGLE_LINKED_LIST_ADD_NAME(x)
bool GEN_SINGLE_LINKED_LIST_ADD_NAME(SINGLE_LINKED_LIST)
{
    if (this == NULL) return true;
    GEN_SINGLE_LINKED_LIST_LOCK_USE(SINGLE_LINKED_LIST)(this);
    
    SINGLE_LINKED_LIST_ITEM* newItem = (SINGLE_LINKED_LIST_ITEM*)malloc(sizeof(SINGLE_LINKED_LIST_ITEM));
    if (newItem == NULL)
    {
        GEN_SINGLE_LINKED_LIST_UNLOCK_USE(SINGLE_LINKED_LIST)(this);
        return true;
    }
    newItem->item = item;
    newItem->next = (intptr_t)this->head;
    this->head = newItem;
    
    GEN_SINGLE_LINKED_LIST_UNLOCK_USE(SINGLE_LINKED_LIST)(this);
    return false;
}
#undef GEN_SINGLE_LINKED_LIST_ADD_NAME
#undef MAKE_SINGLE_LINKED_LIST_ADD_NAME

#define MAKE_SINGLE_LINKED_LIST_ADDRANGE_NAME(x) x ## _addrange(x* this, SINGLE_LINKED_LIST_TYPE* items, INDEX_TYPE items_length)
#define GEN_SINGLE_LINKED_LIST_ADDRANGE_NAME(x) MAKE_SINGLE_LINKED_LIST_ADDRANGE_NAME(x)
bool GEN_SINGLE_LINKED_LIST_ADDRANGE_NAME(SINGLE_LINKED_LIST)
{
    if (this == NULL) return true;
    GEN_SINGLE_LINKED_LIST_LOCK_USE(SINGLE_LINKED_LIST)(this);
    SINGLE_LINKED_LIST_ITEM* current = this->head;
    for (INDEX_TYPE i = 0; i < items_length; ++i)
    {
        SINGLE_LINKED_LIST_ITEM* newItem = (SINGLE_LINKED_LIST_ITEM*)malloc(sizeof(SINGLE_LINKED_LIST_ITEM));
        if (newItem == NULL)
        {
            for (SINGLE_LINKED_LIST_ITEM* freePtr = current; freePtr != this->head; freePtr = (SINGLE_LINKED_LIST_ITEM*) freePtr->next)
            {
                free(freePtr);
            }
            GEN_SINGLE_LINKED_LIST_UNLOCK_USE(SINGLE_LINKED_LIST)(this);
            return true;
        }
        newItem->item = items[i];
        
        if (current != NULL)
            newItem->next = (intptr_t) current;
        else
            newItem->next = (intptr_t) NULL;
        
        current = newItem;
    }
    this->head = current;
    
    GEN_SINGLE_LINKED_LIST_UNLOCK_USE(SINGLE_LINKED_LIST)(this);
    return false;
}

#define MAKE_LIST_ADD_AT_NAME(x) x ## _add_at(x* this, LIST_TYPE item, INDEX_TYPE index)
#define GEN_LIST_ADD_AT_NAME(x) MAKE_LIST_ADD_AT_NAME(x)
// bool list_x_add(list_x* this, LIST_TYPE item, INDEX_TYPE index)
bool GEN_LIST_ADD_AT_NAME(LIST)
{
    if (this == NULL) return true;
    if (this->size >= this->capacity)
    {
        bool check = GEN_LIST_REALLOCATE_NAME_USE(LIST)(this, this->size + 1);
        if (check == false) { return true; }
    }
    INDEX_TYPE remainder = this->size - index;
    memmove(&this->items[index + 1], &this->items[index], (remainder + 1) * sizeof(LIST_TYPE));
    this->items[index] = item;
    this->size++;
    return false;
}

#define MAKE_LIST_ADDRANGE_AT_NAME(x) x ## _addrange_at(x* this, INDEX_TYPE index, LIST_TYPE* items, INDEX_TYPE items_length)
#define GEN_LIST_ADDRANGE_AT_NAME(x) MAKE_LIST_ADDRANGE_AT_NAME(x)
// bool list_x_addrange_at(list_x* this, INDEX_TYPE index, LIST_TYPE* items, INDEX_TYPE items_length)
bool GEN_LIST_ADDRANGE_AT_NAME(LIST)
{
    if (this == NULL) return true;
    if (items_length <= 0 || items == NULL || index < 0 || (this->size > 0 && index >= this->size))
        return true;
    INDEX_TYPE newLen = this->size + items_length;
    if (newLen >= this->capacity)
    {
        if (GEN_LIST_REALLOCATE_NAME_USE(LIST)(this, newLen))
            return true;
    }
    INDEX_TYPE remainder = this->size - index;
    if (remainder > 0)
        memmove(&this->items[index + items_length], &this->items[index], remainder * sizeof(LIST_TYPE));
    memcpy(&this->items[index], items, items_length * sizeof(LIST_TYPE));
    this->size = newLen;
    return false;
}

#define MAKE_LIST_REMOVE_NAME(x) x ## _remove(x* this, INDEX_TYPE index)
#define GEN_LIST_REMOVE_NAME(x) MAKE_LIST_REMOVE_NAME(x)
#define MAKE_LIST_REMOVE_NAME_USE(x) x ## _remove
#define GEN_LIST_REMOVE_NAME_USE(x) MAKE_LIST_REMOVE_NAME_USE(x)
// bool list_x_remove(list_x* this, INDEX_TYPE index)
bool GEN_LIST_REMOVE_NAME(LIST)
{
    if (this == NULL) return true;
    INDEX_TYPE remainder = this->size - index;
    if (remainder > 0)
    {
        memmove(&this->items[index], &this->items[index + 1], remainder * sizeof(LIST_TYPE));
        if (this->size > 0)
            this->size -= 1;
    }
    if (this->size < this->capacity / 4)
    {
        return GEN_LIST_REALLOCATE_NAME_USE(LIST)(this, this->capacity / 2);
    }
    return false;
}


#define MAKE_LIST_REMOVERANGE_NAME(x) x ## _removerange(x* this, INDEX_TYPE index, INDEX_TYPE length)
#define GEN_LIST_REMOVERANGE_NAME(x) MAKE_LIST_REMOVERANGE_NAME(x)
// bool list_x_removerange(list_x* this, INDEX_TYPE index, INDEX_TYPE length)
bool GEN_LIST_REMOVERANGE_NAME(LIST)
{
    if (this == NULL) return true;
    if (index >= this->size || index < 0)
        return true;
    INDEX_TYPE remainderSize = 0;
    if (this->size > index + length)
        remainderSize = this->size - index - length;
    if (remainderSize <= 0)
    {
        this->size = index;
        if (this->size < this->capacity / 4)
        {
            return GEN_LIST_REALLOCATE_NAME_USE(LIST)(this, this->capacity / 2);
        }
        return false;
    }

    if (length < 0)
        length = 0;
    memmove(&this->items[index], &this->items[index + length], remainderSize * sizeof(LIST_TYPE));
    this->size -= length;
    
    if (this->size < 0)
        this->size = 0;
    
    if (this->size < this->capacity / 4)
    {
        return GEN_LIST_REALLOCATE_NAME_USE(LIST)(this, this->capacity / 2);
    }
    return false;
}


#define MAKE_LIST_REMOVE_ITEM_NAME(x) x ## _remove_item(x* this, LIST_TYPE item)
#define GEN_LIST_REMOVE_ITEM_NAME(x) MAKE_LIST_REMOVE_ITEM_NAME(x)
// bool list_x_remove_item(list_x* this, INDEX_TYPE index)
bool GEN_LIST_REMOVE_ITEM_NAME(LIST)
{
    if (this == NULL) return true;
    INDEX_TYPE index = 0;
    if (GEN_LIST_INDEXOF_NAME_USE(LIST)(this, item, &index))
        return true;
    return GEN_LIST_REMOVE_NAME_USE(LIST)(this, index);
}

#define MAKE_LIST_CLEAR_NAME(x) x ## _clear(x* this)
#define GEN_LIST_CLEAR_NAME(x) MAKE_LIST_CLEAR_NAME(x)
// bool list_x_clear(list_x* this)
bool GEN_LIST_CLEAR_NAME(LIST)
{
    if (this == NULL) return true;
    if (this->items != NULL)
        free(this->items);
    this->items = calloc(sizeof(LIST_TYPE), MINIMUM_LIST_SIZE);
    this->capacity = MINIMUM_LIST_SIZE;
    this->size = 0;
    return false;
}

#define MAKE_LIST_COUNT_NAME(x) x ## _count(x* this, INDEX_TYPE* outCount)
#define GEN_LIST_COUNT_NAME(x) MAKE_LIST_COUNT_NAME(x)
// bool list_x_count(list_x* this, INDEX_TYPE* outCount)
bool GEN_LIST_COUNT_NAME(LIST)
{
    if (this == NULL) return true;
    *outCount = this->size;
    return false;
}

#undef GEN_SINGLE_LINKED_LIST_UNLOCK_USE
#undef MAKE_SINGLE_LINKED_LIST_UNLOCK_USE
#undef GEN_SINGLE_LINKED_LIST_LOCK_USE
#undef MAKE_SINGLE_LINKED_LIST_LOCK_USE

#undef MAKE_LIST_NAME
#undef LIST_TYPE_CMP_FUNCTION
#undef MINIMUM_LIST_SIZE
#undef LIST_NAME
#undef LIST
#undef LIST_TYPE
