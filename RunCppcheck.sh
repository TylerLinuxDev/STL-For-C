#!/bin/bash
# Licensed under MIT License, please refer to "LICENSE" file for more information.
# This script is written for Linux platform and require the following dependencies to run:
# bear - A tool that generates a compilation database for clang tooling.
# cppcheck - A static code analysis tool to assist in finding bugs such as double free.

if [[ -f "compile_commands.json" ]];
then
    rm "compile_commands.json"
fi

if [[ -d "build" ]]
then
    echo "build directory already exists"
else
    bear -- meson setup build
fi

bear -- meson test -C build

cppcheck --suppress=missingInclude --std=c11 --enable=warning,style,performance,portability,information -I include/ -I src/ --project=compile_commands.json 2> CppCheckResult.txt

if [ $( wc -c CppCheckResult.txt | cut -f 1 -d " " ) == 0 ];
then
    echo "No error reported!"
    rm "CppCheckResult.txt"
    #rm "compile_commands.json"
else
    echo "CppCheckResult.txt written, printing output:"
    cat "CppCheckResult.txt"
fi
