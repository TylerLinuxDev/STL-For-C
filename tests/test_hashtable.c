 
#include "stdint.h"
#include <stdint.h>
#define HASH_TABLE_VALUE_TYPE int32_t
#include "src/hash_table_impl.c"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int Test_SetAndGetItems()
{
    int status = 0;
    hash_table_int32_t table = hash_table_int32_t_new();
    if (table == NULL)
        status = 1;
    const char* realKey1;
    const char* realKey2;
    const char* realKey3;
    int32_t value1;
    int32_t value2;
    INDEX_TYPE count;
    if (!status && hash_table_int32_t_set(table, "ABC", 1, &realKey1))
        status = 2;
    if (!status && realKey1 == NULL)
        status = 3;
    if (!status && hash_table_int32_t_set(table, "CBA", 2, &realKey2))
        status = 4;
    if (!status && realKey2 == NULL)
        status = 5;
    if (!status && hash_table_int32_t_count(table, &count))
        status = 6;
    if (!status && count != 2)
        status = 7;
    if (!status && hash_table_int32_t_get(table, realKey1, &value1))
        status = 8;
    if (!status && value1 != 1)
        status = 9;
    if (!status && hash_table_int32_t_get(table, realKey2, &value2))
        status = 10;
    if (!status && value2 != 2)
        status = 11;
    if (!status && hash_table_int32_t_get(table, "ABC", &value1))
        status = 12;
    if (!status && value1 != 1)
        status = 13;
    if (!status && hash_table_int32_t_get(table, "CBA", &value2))
        status = 14;
    if (!status && value2 != 2)
        status = 15;
    if (!status && hash_table_int32_t_set(table, "CBA", 3, &realKey3))
        status = 16;
    if (!status && realKey2 != realKey3)
        status = 17;
    if (!status && hash_table_int32_t_get(table, "CBA", &value2))
        status = 18;
    if (!status && value2 != 3)
        status = 19;
    if (!status && hash_table_int32_t_destroy(table))
        status = 20;
    return status;
}

#pragma pack(1)
typedef struct {
    int32_t val;
    int32_t pad0;
} paddedIntegerStack;

int Test_IntegerAsKey()
{
    int status = 0;
    
    hash_table_int32_t table = hash_table_int32_t_new();
    if (table == NULL)
        status = 1;
    paddedIntegerStack originalKey = (paddedIntegerStack){.val = 16, .pad0 = 0};
    int32_t val;
    if (!status && hash_table_int32_t_set(table, (const char*) &originalKey.val, 321, NULL))
        status = 2;
    paddedIntegerStack realKey = (paddedIntegerStack){.val = 0, .pad0 = 0};
    for (int i = 0; i < 64; ++i)
    {
        realKey.val = i;
        if (!hash_table_int32_t_get(table, (const char*) &realKey.val, &val))
            break;
    }
    if (!status && val != 321)
        status = 3;
    
    if (!status && hash_table_int32_t_destroy(table))
        status = 4;
    
    return status;
}

int main(int argc, char* argv[])
{
    if (argc > 0)
    {
        for (int i = 0; i < argc; ++i)
        {
            if (strcmp(argv[i], "test_setandgetitems") == 0)
            {
                return Test_SetAndGetItems();
            }
            if (strcmp(argv[i], "test_integeraskey") == 0)
            {
                return Test_IntegerAsKey();
            }
        }
    }
    else
    {
        printf("Please specify which test to run.\n");
    }
    return 0;
}
