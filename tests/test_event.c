#include "stdint.h"
#define INDEX_TYPE int16_t
typedef struct {
    int32_t A;
    int32_t B;
} DemoCallback;

#define EVENT_ARG_TYPE DemoCallback
#include "src/event_impl.c"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdatomic.h>

static _Atomic int32_t val = 0;

void Callback(DemoCallback arg)
{
    int32_t operand = arg.A - arg.B;
    atomic_fetch_add(&val, operand);
}

int Test_Basic_Event()
{
    int status = 0;
    event_DemoCallback event = event_new_DemoCallback();
    if (event == NULL)
        status = 1;
    if (!status && event_add_DemoCallback(event, Callback))
        status = 2;
    if (!status && event_add_DemoCallback(event, Callback))
        status = 3;
    INDEX_TYPE count;
    if (!status && event_count_DemoCallback(event, &count))
        status = 4;
    if (!status && count != 2)
        status = 5;
    if (!status && event_invoke_DemoCallback(event, (DemoCallback){.A = 4, .B = 2}))
        status = 6;
    if (!status && val != 4)
        status = 7;
    if (!status && event_destroy_DemoCallback(event))
        status = 8;
    return status;
}

int Test_Excessive_Subscribed_Event()
{
    int status = 0;
    event_DemoCallback event = event_new_DemoCallback();
    if (event == NULL)
        status = 1;
    for (int32_t i = 0; i < 128; ++i)
    {
        if (!status && event_add_DemoCallback(event, Callback))
            status = 2;
    }
    INDEX_TYPE count = 0;
    if (!status && event_count_DemoCallback(event, &count))
        status = 3;
    if (!status && count != 128)
        status = 4;
    if (!status && event_invoke_DemoCallback(event, (DemoCallback){.A = 4, .B = 2}))
        status = 5;
    if (!status && val != 256)
        status = 6;
    if (!status && event_destroy_DemoCallback(event))
        status = 7;
    return status;
}

int main(int argc, const char* argv[])
{
    if (argc > 0)
    {
        for (int i = 0; i < argc; ++i)
        {
            if (strcmp(argv[i], "test_basic_event") == 0)
            {
                return Test_Basic_Event();
            }
            if (strcmp(argv[i], "test_excessive_subscribed_event") == 0)
            {
                return Test_Excessive_Subscribed_Event();
            }
        }
    }
    else
    {
        printf("Please specify which test to run.\n");
    }
    return 0;
}
