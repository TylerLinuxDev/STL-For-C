#include "stdint.h"
#define CONCURRENT_STACK_TYPE int64_t
#include "src/concurrent_stack_impl.c"
#include "STL_CThread/source/tinycthread.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int Test_Push_And_Pop()
{
    concurrent_stack_int64_t* stack = concurrent_stack_int64_t_new();
    int64_t result = 0;

    for (int i = 0; i < 1024; ++i)
    {
        if (concurrent_stack_int64_t_push(stack, i)) return 1;
    }

    for (int i = 1023; i > -1; --i)
    {
        if (concurrent_stack_int64_t_trypop(stack, &result)) return 2;
        if (result != i) return 3;
    }

    concurrent_stack_int64_t_destroy(stack);
    return 0;
}

int Test_PushRange_And_PopRange()
{
    concurrent_stack_int64_t* stack = concurrent_stack_int64_t_new();
    int64_t input[1024];
    for (int i = 0; i < 1024; ++i)
        input[i] = i + 1;
    int64_t output[1024];
    int64_t resultLength = 0;
    concurrent_stack_int64_t_pushrange(stack, input, 1024);
    for (int i = 1024; i > 0; --i)
    {
        int64_t item = 0;
        concurrent_stack_int64_t_trypop(stack, &item);
        if (item != i) return 1;
    }
    concurrent_stack_int64_t_pushrange(stack, input, 1024);
    concurrent_stack_int64_t_trypoprange(stack, output, &resultLength, 1024);

    for (int i = 0; i < 1024; ++i)
    {
        if (output[i] != 1024 - i) return 2;
    }

    concurrent_stack_int64_t_destroy(stack);
    return 0;
}


static concurrent_stack_int64_t *global_stack;
static volatile _Atomic int64_t completedCount = 0;

int push_to_global_stack(void *arg)
{
    int64_t index = ((int64_t *)arg)[0];
    int64_t length = ((int64_t *)arg)[1];
    index *= length;
    int64_t endIndex = index + length;
    for (int64_t i = index; i < endIndex;)
    {
        if (i % 3 == 0 || i + 1 >= endIndex)
        {
            concurrent_stack_int64_t_push(global_stack, i);
            ++i;
        }
        else
        {
            int64_t items[2];
            items[0] = i++;
            items[1] = i++;
            concurrent_stack_int64_t_pushrange(global_stack, items, 2);
        }
    }
    ++completedCount;
    return 0;
}

int Test_Multithread_Push()
{
    global_stack = concurrent_stack_int64_t_new();
    thrd_t *threads = (thrd_t *)malloc(sizeof(thrd_t) * 8);
    int64_t args[16];
    // Dispatch 8 Threads
    for (int64_t threadIndex = 0; threadIndex < 8; ++threadIndex)
    {
        args[threadIndex * 2] = threadIndex;
        args[threadIndex * 2 + 1] = 1048576;
        thrd_create(&threads[threadIndex], push_to_global_stack, &args[threadIndex * 2]);
    }

    while (completedCount != 8)
    {
        thrd_yield();
    }

    if (concurrent_stack_int64_t_count(global_stack) != 8388608)
    {
        return 1;
    }

    int64_t *validator = (int64_t *)calloc(8388608, sizeof(int64_t));
    while (!concurrent_stack_int64_t_isempty(global_stack))
    {
        int64_t res = 0;
        if (concurrent_stack_int64_t_trypop(global_stack, &res))
        {
            return 2;
        }
        validator[res] = 1;
    }

    for (int64_t i = 8388607; i > -1; --i)
    {
        if (validator[i] == 0)
        {
            return 3;
        }
    }
    for (int threadId = 0; threadId < 8; ++threadId)
    {
        thrd_detach(threads[threadId]);
    }
    free(validator);
    completedCount = 0;
    free(threads);
    return 0;
}

static volatile _Atomic int64_t* popResult;

int pop_from_global_stack(void *arg)
{
    int64_t length = ((int64_t *)arg)[1];
    int64_t i = 0;
    for (; i < length; ++i)
    {
        if (i % 3 == 0 || i + 1 >= length)
        {
            int64_t res = 0;
            if (concurrent_stack_int64_t_trypop(global_stack, &res))
            {
                return 1;
            }
            popResult[res] = 1;
        }
        else
        {
            int64_t items[2];
            int64_t resCount = 0;
            if (concurrent_stack_int64_t_trypoprange(global_stack, items, &resCount, 2))
            {
                return 1;
            }
            if (resCount != 2)
            {
                return 1;
            }
            ++i;
            popResult[items[0]] = 1;
            popResult[items[1]] = 1;
        }
        
    }
    atomic_fetch_add(&completedCount, 1);
    return 0;
}

int Test_Multithread_Pop()
{
    global_stack = concurrent_stack_int64_t_new();
    thrd_t *threads = (thrd_t *)malloc(sizeof(thrd_t) * 8);
    popResult = (_Atomic(int64_t) *)calloc(8388608, sizeof(int64_t));
    for (int64_t i = 0; i < 8388608; ++i)
    {
        if (concurrent_stack_int64_t_push(global_stack, i))
        {
            return 1;
        }
    }
    int64_t* args = (int64_t*)malloc(sizeof(int64_t));
    *args = 1048576;
    // Dispatch 8 Threads
    for (int64_t threadIndex = 0; threadIndex < 8; ++threadIndex)
    {
        thrd_create(&threads[threadIndex], pop_from_global_stack, args);
    }

    while (completedCount != 8)
    {
        thrd_yield();
    }
    free(args);
    
    for (int64_t i = 8388607; i > -1; --i)
    {
        if (popResult[i] == 0)
        {
            return 2;
        }
    }
    for (int threadId = 0; threadId < 8; ++threadId)
    {
        thrd_detach(threads[threadId]);
    }
    free((void*)popResult);
    completedCount = 0;
    free(threads);
    return 0;
}

int push_to_global_stack2(void *arg)
{
    int64_t index = *((int64_t *)arg);
    index *= 1048576;
    int64_t endIndex = index + 1048576;
    for (int64_t i = index; i < endIndex;)
    {
        if (i % 3 == 0 || i + 1 >= endIndex)
        {
            concurrent_stack_int64_t_push(global_stack, i);
            ++i;
        }
        else
        {
            int64_t items[2];
            items[0] = i++;
            items[1] = i++;
            concurrent_stack_int64_t_pushrange(global_stack, items, (int64_t) 2);
        }
    }
    ++completedCount;
    return 0;
}

int pop_from_global_stack2(void *arg)
{
    int64_t i = 0;
    int64_t* items = (int64_t*) calloc(sizeof(int64_t), 2);
    int64_t resCount = 0;
    for (; i < 1048576;)
    {
        if (i % 3 == 0 || i + 1 >= 1048576)
        {
            int64_t res = 0;
            while (concurrent_stack_int64_t_trypop(global_stack, &res))
            {
                atomic_signal_fence(__ATOMIC_ACQ_REL);
            }
            popResult[res] = 1;
            ++i;
        }
        else
        {
            while (concurrent_stack_int64_t_trypoprange(global_stack, items, &resCount, (int64_t)2))
            {
                atomic_thread_fence(__ATOMIC_ACQ_REL);
            }
            i += resCount;
            if (resCount > 0)
                popResult[items[0]] = 1;
            if (resCount > 1)
                popResult[items[1]] = 1;
        }
    }
    ++completedCount;
    return 0;
}

int Test_Maximum_Contention()
{
    int64_t threadCount = 8;
    global_stack = concurrent_stack_int64_t_new();
    thrd_t *threads = (thrd_t *)malloc(sizeof(thrd_t) * threadCount);
    popResult = (_Atomic(int64_t) *)calloc(1048576 * threadCount, sizeof(int64_t));
    int64_t* args = (int64_t*)calloc(sizeof(int64_t), threadCount);

    // Dispatch 8 Threads
    for (int64_t threadIndex = 0; threadIndex < threadCount; ++threadIndex)
    {
        args[threadIndex] = threadIndex;
        if (threadIndex % 2 == 0)
            thrd_create(&threads[threadIndex], pop_from_global_stack2, &args[threadIndex]);
        else
            thrd_create(&threads[threadIndex], push_to_global_stack2, &args[threadIndex]);
    }

    while (completedCount != threadCount)
    {
        thrd_yield();
    }
    free(args);
    int64_t count = concurrent_stack_int64_t_count(global_stack);
    if (count != 0)
    {
        fprintf(stderr, "%li\n", count);
        return 2;
    }
    for (int threadId = 0; threadId < 8; ++threadId)
    {
        thrd_detach(threads[threadId]);
    }
    free((void*)popResult);
    completedCount = 0;
    free(threads);
    return 0;
}

int main(int argc, char* argv[])
{
    if (argc > 0)
    {
        for (int i = 0; i < argc; ++i)
        {
            if (strcmp(argv[i], "test_maximum_contention") == 0)
            {
                return Test_Maximum_Contention();
            }
        }
    }
    else
    {
        printf("Please specify which test to run.\n");
    }
    return 0;
}