#include <stdint.h>
#define QUEUE_TYPE int32_t
#define ALLOW_SHRINKING_QUEUE 1
#include "src/queue_impl.c"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int Test_EnqueueAndDequeue() {
  int status = 0;
  queue_int32_t queue = queue_int32_t_new();
  int32_t dequeueItem;
  uint64_t outEnqueueCount = 0;
  uint64_t outCount = 0;

  // This section tests the followings:
  // 1. Ensure Push Item works
  // 2. Ensure Pop Item works
  // 3. Verify that ciruclar buffer works
  for (int32_t i = 0; i < 31; ++i) {
    outEnqueueCount = 0;
    if (!status && queue_int32_t_enqueue(queue, i, &outEnqueueCount))
      status = 1;
    if (!status && outEnqueueCount != 1)
      status = 2;
  }
  outCount = 0;
  if (!status && queue_int32_t_getcount(queue, &outCount))
    status = 3;
  if (!status && outCount != 31)
    status = 4;

  for (int32_t i = 0; i < 16; ++i) {
    dequeueItem = 0;
    if (!status && queue_int32_t_dequeue(queue, &dequeueItem, NULL))
      status = 5;
    if (!status && dequeueItem != i)
      status = 6;
  }
  outCount = 0;
  if (!status && queue_int32_t_getcount(queue, &outCount))
    status = 7;
  if (!status && outCount != 15)
    status = 8;

  for (int32_t i = 0; i < 16; ++i) {
    outEnqueueCount = 0;
    if (!status && queue_int32_t_enqueue(queue, i, &outEnqueueCount))
      status = 9;
    if (!status && outEnqueueCount != 1)
      status = 10;
  }
  outCount = 0;
  if (!status && queue_int32_t_getcount(queue, &outCount))
    status = 11;
  if (!status && outCount != 31)
    status = 12;

  // This section tests the followings:
  // 1. Ensure Reallocation works
  // 2. Ensure that items sorting through reallocation works
  outEnqueueCount = 0;
  if (!status && queue_int32_t_enqueue(queue, 32, &outEnqueueCount))
    status = 13;
  if (!status && outEnqueueCount != 1)
    status = 14;
  outCount = 0;
  if (!status && queue_int32_t_getcount(queue, &outCount))
    status = 15;
  if (!status && outCount != 32)
    status = 16;

  outEnqueueCount = 0;
  if (!status && queue_int32_t_enqueue(queue, 33, &outEnqueueCount))
    status = 17;
  if (!status && outEnqueueCount != 1)
    status = 18;
  outCount = 0;
  if (!status && queue_int32_t_getcount(queue, &outCount))
    status = 19;
  if (!status && outCount != 33)
    status = 20;

  for (int32_t i = 16; i < 31; ++i) {
    dequeueItem = 0;
    if (!status && queue_int32_t_dequeue(queue, &dequeueItem, NULL))
      status = 21;
    if (!status && dequeueItem != i) {
      status = 22;
    }
  }

  for (int32_t i = 0; i < 16; ++i) {
    dequeueItem = 0;
    if (!status && queue_int32_t_dequeue(queue, &dequeueItem, NULL))
      status = 23;
    if (!status && dequeueItem != i)
      status = 24;
  }

  for (int32_t i = 32; i < 34; ++i) {
    dequeueItem = 0;
    if (!status && queue_int32_t_dequeue(queue, &dequeueItem, NULL))
      status = 26;
    if (!status && dequeueItem != i)
      status = 27;
  }
  outCount = 0;
  if (!status && queue_int32_t_getcount(queue, &outCount))
    status = 28;
  if (!status && outCount != 0)
    status = 29;

  // This section tests the followings:
  // 1. Validate the second portion of Reallocation Function
  
  for (int32_t i = 0; i < 30; ++i) {
    outEnqueueCount = 0;
    if (!status && queue_int32_t_enqueue(queue, i, &outEnqueueCount))
      status = 30;
    if (!status && outEnqueueCount != 1)
      status = 31;
  }

  for (int32_t i = 0; i < 15; ++i) {
    dequeueItem = 0;
    if (!status && queue_int32_t_dequeue(queue, &dequeueItem, NULL))
      status = 32;
    if (!status && dequeueItem != i)
      status = 33;
  }

  if (!status && queue_int32_t_reallocate(queue, 32))
    status = 34;

  for (int32_t i = 15; i < 30; ++i) {
    dequeueItem = 0;
    if (!status && queue_int32_t_dequeue(queue, &dequeueItem, NULL))
      status = 35;
    if (!status && dequeueItem != i)
      status = 36;
  }

  queue_int32_t_destroy(queue);
  return status;
}

int Test_EnqueueAndDequeueRange() {
  int status = 0;
  queue_int32_t queue = queue_int32_t_new();
  if (queue == NULL)
    return 1;
  int32_t *rawData = (int32_t *)malloc(sizeof(int32_t) * 30);
  if (rawData == NULL)
    status = 2;
  if (!status)
    for (int32_t i = 0; i < 30; ++i)
      rawData[i] = i;
  if (!status && queue_int32_t_enqueue_items(queue, rawData, 0, 30, NULL))
    status = 3;

  int32_t *dequeueData = (int32_t *)calloc(sizeof(int32_t), 30);
  if (dequeueData == NULL)
    status = 4;
  if (!status && queue_int32_t_dequeue_items(queue, 30, dequeueData, NULL))
    status = 5;
  if (!status)
    for (int32_t i = 0; i < 30; ++i) {
      if (dequeueData[i] != i)
        status = 6;
    }
  free(dequeueData);
  queue_int32_t_destroy(queue);
  return status;
}

int Test_CircularBuffer() {
  int status = 0;
  uint64_t enqueueCount = 0;
  uint64_t popCount = 0;
  queue_int32_t queue = queue_int32_t_new();
  if (queue == NULL)
    return 1;
  int32_t *rawData = (int32_t *)malloc(sizeof(int32_t) * 1024);
  if (rawData == NULL)
    status = 2;
  int32_t *popData = (int32_t *)malloc(sizeof(int32_t) * 1024);
  if (popData == NULL)
    status = 3;

  // Simulate real-world use of queue
  for (uint64_t retry = 0; retry < 1000; ++retry) {
    int32_t length = ((int32_t)rand() % 1023) + 1;

    for (int32_t dataIndex = 0; dataIndex < length; dataIndex++)
      rawData[dataIndex] = (int32_t)rand();

    if (length == 1 || length % 2 == 0) {
        for (int32_t i = 0; i < length; ++i)
        {
            if (!status && queue_int32_t_enqueue(queue, rawData[i], &enqueueCount))
                status = 4;
            if (!status && enqueueCount != 1)
                status = 5;
        }
    }
    else
    {
        if (!status && queue_int32_t_enqueue_items(queue, rawData, 0, length, &enqueueCount))
            status = 6;
        if (enqueueCount != length)
            status = 7;
    }
    
    if (length == 1 || length % 2 == 1) {
        for (int32_t i = 0; i < length; ++i)
        {
            if (!status && queue_int32_t_dequeue(queue, popData, NULL))
                status = 8;
            if (!status && popData[0] != rawData[i])
                status = 9;
        }
    }
    else
    {
        if (!status && queue_int32_t_dequeue_items(queue, length, popData, &popCount))
            status = 10;
        if (!status && popCount != length)
            status = 11;
        
        for (int32_t i = 0; i < length; ++i)
            if (!status && popData[i] != rawData[i])
                status = 12;
    }
  }

  free(popData);
  free(rawData);
  queue_int32_t_destroy(queue);
  return status;
}

int Test_LoopQueue()
{
  queue_int32_t queue = queue_int32_t_new();
  size_t count = 0;
  for (size_t i = 0; i < 1024*1024; ++i)
  {
    int action = rand() % 4;
    if (action > 0)
    {
      queue_int32_t_enqueue(queue, i, &count);
    }
    else
    {
      int32_t out;
      queue_int32_t_getcount(queue, &count);
      if (count > 0)
      {
        queue_int32_t_dequeue(queue, &out, &count);
      }
    }
  }
  queue_int32_t_destroy(queue);
  return 0;
}

int main(int argc, char *argv[]) {
  if (argc > 0) {
    for (int i = 0; i < argc; ++i) {
      if (strcmp(argv[i], "test_enqueueanddequeue") == 0) {
        return Test_EnqueueAndDequeue();
      }
      if (strcmp(argv[i], "test_enqueueanddequeuerange") == 0) {
        return Test_EnqueueAndDequeueRange();
      }
      if (strcmp(argv[i], "test_circularbuffer") == 0) {
        return Test_CircularBuffer();
      }
      if (strcmp(argv[i], "test_loopqueue") == 0) {
        return Test_LoopQueue();
      }
    }
  } else {
    printf("Please specify which test to run.\n");
  }
  return 0;
}
