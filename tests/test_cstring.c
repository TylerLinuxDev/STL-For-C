#include "cstring.h"
#include "src/cstring_impl.c"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>


int Test_CString_Allocation()
{
    cstring test = cstring_new();
    if (cstring_validate(test))
        return 1;
    bool result = cstring_destroy(test);
    if (result == true)
        return 2;
    test = cstring_new2(0);
    if (cstring_validate(test))
        return 3;
    test = cstring_new2(1);
    if (cstring_validate(test))
        return 4;
    result = cstring_destroy(test);
    if (result == true)
        return 5;
    return 0;
}


int Test_CString_IndexOf()
{
    cstring test = cstring_new3("Hello world!");
    uint64_t index = 0;
    bool result = cstring_indexof(test, "o", 0, 13, &index);
    if (result == true)
        return 1;
    if (index != 4)
    {
        return 2;
    }
    char charCheck = 0;
    if (cstring_getcharacter(test, index, &charCheck))
        return 3;
    if (charCheck != 'o')
        return 4;
    result = cstring_indexof(test, " ", 0, 13, &index);
    if (result == true)
        return 5;
    if (index != 5)
        return 6;
    result = cstring_indexof(test, "\0", 0, 13, &index);
    if (result != true)
        return 7;
    result = cstring_indexof(test, "!", 0, 13, &index);
    if (result == true)
        return 8;
    if (index != 11)
        return 9;
    if (cstring_destroy(test) !=  false)
        return 10;
    return 0;
}

int main(int argc, const char* argv[])
{
    if (argc > 0)
    {
        for (int i = 0; i < argc; ++i)
        {
            if (strcmp(argv[i], "test_cstring_allocation") == 0)
            {
                return Test_CString_Allocation();
            }
            if (strcmp(argv[i], "test_cstring_indexof") == 0)
            {
                return Test_CString_IndexOf();
            }
        }
    }
    else
    {
        printf("Please specify which test to run.\n");
    }
    return 0;
}
