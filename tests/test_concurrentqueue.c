#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdatomic.h>
#include <sys/time.h>
#include <threads.h>
#include<unistd.h>

#define INDEX_TYPE uint32_t
#define CONCURRENT_QUEUE_TYPE int32_t
#define NO_UNDEF 1
#define ALLOW_SHRINKING_QUEUE 0
#include "src/concurrent_queue_impl.c"

int AllocateAndFree()
{
    concurrent_queue_int32_t queue = concurrent_queue_int32_t_new();
    
    if (queue == NULL)
        return 1;
    
    if (concurrent_queue_int32_t_destroy(queue))
        return 2;
    
    return 0;
}

concurrent_queue_int32_t globalQueue;
_Atomic uint32_t completedCount = 0;
_Atomic uint32_t start = 0;
_Atomic uint64_t count;
int EnqueueThread(void *arg)
{
    while (start == 0) {}
    uint64_t val = *(uint64_t*)arg;
    INDEX_TYPE outCount;
    uint32_t totalCount = 0;
    for (int32_t i = 0; i < val; ++i)
    {
        outCount = 0;
        concurrent_queue_int32_t_enqueue(globalQueue, i, &outCount);
        if (outCount < 1)
        {
            i--;
            continue;
        }
        assert(outCount < 2);
        ++totalCount;
    }
    assert(totalCount == val);
    atomic_fetch_add(&completedCount, 1);
    return 0;
}

int timeval_subtract(struct timeval *result, struct timeval *x, struct timeval *y)
{  
  // preserve *y
  struct timeval yy = *y;
  y = &yy;

  /* Perform the carry for the later subtraction by updating y. */  
  if (x->tv_usec < y->tv_usec) {  
    int nsec = (y->tv_usec - x->tv_usec) / 1000000 + 1;  
    y->tv_usec -= 1000000 * nsec;  
    y->tv_sec += nsec;  
  }  
  if (x->tv_usec - y->tv_usec > 1000000) {  
    int nsec = (y->tv_usec - x->tv_usec) / 1000000;  
    y->tv_usec += 1000000 * nsec;  
    y->tv_sec -= nsec;  
  }  

  /* Compute the time remaining to wait.
     tv_usec is certainly positive. */  
  result->tv_sec = x->tv_sec - y->tv_sec;  
  result->tv_usec = x->tv_usec - y->tv_usec;  

  /* Return 1 if result is negative. */  
  return x->tv_sec < y->tv_sec;  
}

int EnqueueOnly()
{
    globalQueue = concurrent_queue_int32_t_new();
    completedCount = 0;
    if (globalQueue == NULL)
        return 1;
    
    int32_t threadCount = 8;
    count = 128;
    thrd_t threads[threadCount];
    // Dispatch 8 Threads
    for (int32_t threadIndex = 0; threadIndex < threadCount; ++threadIndex)
    {
        thrd_create(&threads[threadIndex], EnqueueThread, &count);
    }
    start = 1;
    while (completedCount != threadCount)
    {
    }
    for (int32_t i = 0; i < threadCount; ++i)
    {
        thrd_join(threads[i], NULL);
        thrd_detach(threads[i]);
    }
    if (concurrent_queue_int32_t_destroy(globalQueue))
        return 3;
    
    return 0;
}

int DequeueThread(void* arg)
{
    uint64_t val = *(uint64_t*)arg;
    uint32_t totalCount = 0;
    for (int32_t i = 0; i < val; ++i)
    {
        INDEX_TYPE count = 0;
        int32_t item = 0;
        concurrent_queue_int32_t_dequeue(globalQueue, &item, &count);
        if (count != 1)
        {
            assert(count < 1);
            --i;
            continue;
        }
        assert(count < 2);
        ++totalCount;
    }
    assert(totalCount == val);
    atomic_fetch_add(&completedCount, 1);
    return 0;
}

int MaximumContention()
{
    globalQueue = concurrent_queue_int32_t_new();
    completedCount = 0;
    if (globalQueue == NULL)
        return 1;
    
    int32_t threadCount = 32;
    thrd_t threads[threadCount];
    count = 1024;
    // Dispatch 8 Threads
    for (int32_t threadIndex = 0; threadIndex < threadCount; ++threadIndex)
    {
        if (threadIndex % 2 == 0)
            thrd_create(&threads[threadIndex], EnqueueThread, &count);
        else
            thrd_create(&threads[threadIndex], DequeueThread, &count);
    }
    start = 1;
    while (completedCount != threadCount)
    {
    }
    for (int32_t i = 0; i < threadCount; ++i)
    {
        thrd_detach(threads[i]);
    }

    if (concurrent_queue_int32_t_destroy(globalQueue))
        return 3;
    
    return 0;
}

int main(int argc, char* argv[])
{
    if (argc > 0)
    {
        for (int i = 0; i < argc; ++i)
        {
            if (strcmp(argv[i], "test_allocate_and_free") == 0)
            {
                return AllocateAndFree();
            }
            if (strcmp(argv[i], "test_enqueue_only") == 0)
            {
                return EnqueueOnly();
            }
            if (strcmp(argv[i], "test_maximum_contention") == 0)
            {
                return MaximumContention();
            }
        }
    }
    else
    {
        printf("Please specify which test to run.\n");
    }
    return 0;
}
