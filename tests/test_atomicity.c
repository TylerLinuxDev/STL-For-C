#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdatomic.h>
#include "assert.h"
#include <threads.h>
static volatile _Atomic int64_t globalValue = 0;
static volatile _Atomic int64_t completedCount = 0;

int ApplyOperationThread(void *arg)
{
    int64_t addorsub = ((int64_t*)arg)[0];
    int64_t count = ((int64_t*)arg)[1];
    int64_t val = globalValue;
    for (int64_t i = 0; i < count; ++i)
    {
        if (addorsub)
            while (!atomic_compare_exchange_strong(&globalValue, &val, val + 1)) {atomic_signal_fence(__ATOMIC_ACQ_REL);}
        else
            while (!atomic_compare_exchange_strong(&globalValue, &val, val - 1)) {atomic_signal_fence(__ATOMIC_ACQ_REL);}
    }
    ++completedCount;
    return 0;
}

int Test_Maximum_Contentions()
{
    int32_t threadCount = 32;
    thrd_t threads[threadCount];
    int64_t args[threadCount * 2];
    // Dispatch 8 Threads
    for (int32_t threadIndex = 0; threadIndex < threadCount; ++threadIndex)
    {
        args[threadIndex * 2] = (int64_t)(threadIndex % 2);
        args[threadIndex * 2 + 1] = (int64_t) 32768;
        thrd_create(&threads[threadIndex], ApplyOperationThread, &args[threadIndex*2]);
    }

    while (completedCount != threadCount)
    {
    }
    int32_t val = globalValue;
    if (val != 0)
    {
        fprintf(stderr, "Incorrect value: %i\n", val);
        return 1;
    }
    for (int32_t i = 0; i < threadCount; ++i)
    {
        thrd_detach(threads[i]);
    }
    completedCount = 0;
    return 0;
}

_Atomic intptr_t* data;

int DesequenceThread(void *arg)
{
    for (int64_t i = 0; i < 32768; ++i)
    {
        _Atomic intptr_t* current = data;
        while (!atomic_compare_exchange_strong(data, current, current[1]))
        {
            atomic_thread_fence(__ATOMIC_ACQ_REL);
        }
       
    }
    atomic_fetch_add(&completedCount, 1);
    return 0;
}

int Test_Sequence_Contentions()
{
    int32_t threadCount = 8;
    thrd_t* threads = (thrd_t*) calloc(threadCount, sizeof(thrd_t));
    data = (_Atomic intptr_t*)calloc(sizeof(_Atomic(intptr_t)), threadCount * 32768 + 2);
    for (int i = 0; i < threadCount * 32768 - 1; ++i)
    {
        data[i] = (intptr_t) &data[i + 1];
    }
    data[threadCount * 32768 - 1] = (intptr_t) NULL;
    _Atomic intptr_t* start = data;
    for (int32_t threadIndex = 0; threadIndex < threadCount; ++threadIndex)
    {
        thrd_create(&threads[threadIndex], DesequenceThread, start);
    }

    while (completedCount != threadCount)
    {
    }

    completedCount = 0;
    free((void*)start);
    free(threads);
    return 0;
}

void* Test = NULL;
int32_t flag = 0;

int ExchangeOp(void* arg)
{
    for (int32_t i = 0; i < 32*1024; ++i)
    {
        void* val = atomic_exchange((_Atomic(void*)*)&Test, (void*)1);
        if (val == (void*)1)
        {
            i--;
            atomic_signal_fence(memory_order_seq_cst);
            continue;
        }
        assert(val == NULL);
        assert(atomic_fetch_add((_Atomic(int32_t)*)&flag, 1) == 0);
        assert(atomic_fetch_sub((_Atomic(int32_t)*)&flag, 1) == 1);
        atomic_store((_Atomic(void*)*)&Test, NULL);
    }
    ++completedCount;
    return 0;
}

int Test_Atomic_Exchange_And_Load()
{
    completedCount = 0;
    
    int32_t threadCount = 8;
    thrd_t threads[threadCount];
    // Dispatch 8 Threads
    for (int32_t threadIndex = 0; threadIndex < threadCount; ++threadIndex)
    {
        thrd_create(&threads[threadIndex], ExchangeOp, NULL);
    }
    while (completedCount != threadCount)
    {
        atomic_signal_fence(memory_order_seq_cst);
    }
    for (int32_t i = 0; i < threadCount; ++i)
    {
        thrd_detach(threads[i]);
    }
    
    return 0;
}

int32_t threadCount = 8;
typedef struct teststruct {
    int32_t data;
    void* ptr;
} *teststruct;
int LeaseAndRelease(void* arg)
{
    int32_t* records = (int32_t*)arg;
    for (size_t loop = 0; loop < 1024*128; ++loop)
    {
        for (int i = 0; i < threadCount; ++i)
        {
            int32_t val = atomic_exchange((_Atomic int32_t*)&records[i], 0);
            if (val == 0) continue;
            val = atomic_exchange((_Atomic int32_t*)&records[i], val);
            if (val != 0)
                exit(1);
            break;
        }
    }
    atomic_fetch_add(&completedCount, 1);
    return 0;
}

int Test_Atomic_Leasing_Releasing()
{
    completedCount = 0;

    _Atomic int32_t* records = (_Atomic int32_t*)malloc(sizeof(int32_t) * threadCount);
    for (int32_t i = 0; i < threadCount; ++i)
        records[i] = 1;
    thrd_t threads[threadCount];
    // Dispatch 8 Threads
    for (int32_t threadIndex = 0; threadIndex < threadCount; ++threadIndex)
    {
        thrd_create(&threads[threadIndex], LeaseAndRelease, (void*)records);
    }
    while (completedCount != threadCount)
    {
        thrd_sleep(&(struct timespec){.tv_nsec = 1000000}, NULL);
    }
    for (int32_t i = 0; i < threadCount; ++i)
    {
        thrd_join(threads[i], NULL);
        thrd_detach(threads[i]);
    }
    return 0;
}

int main(int argc, char* argv[])
{
    if (argc > 0)
    {
        for (int i = 0; i < argc; ++i)
        {
            if (strcmp(argv[i], "test_maximum_contentions") == 0)
            {
                return Test_Maximum_Contentions();
            }
            if (strcmp(argv[i], "test_sequence_contentions") == 0)
            {
                return Test_Sequence_Contentions();
            }
            if (strcmp(argv[i], "test_atomic_exchange_and_load") == 0)
            {
                return Test_Atomic_Exchange_And_Load();
            }
            if (strcmp(argv[i], "test_atomic_leasing_releasing") == 0)
            {
                return Test_Atomic_Leasing_Releasing();
            }
        }
    }
    else
    {
        printf("Please specify which test to run.\n");
    }
    return 0;
}
